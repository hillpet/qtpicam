/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          CImageViewer.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Image viewer class
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       18 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QtWidgets>
#include "imageviewer.h"

//
//  Function:  CImageViewer
//  Purpose:   Constructor
//
//  Parms:     
//  Returns:
//
CImageViewer::CImageViewer()
{
   fHaveImage = false;
   //
   pclImg = new QLabel;
   pclImg->setBackgroundRole(QPalette::Base);
   pclImg->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
   pclImg->setFrameStyle(QFrame::Box);
   pclImg->setScaledContents(true);

   clImgSize = pclImg->size();
   setMinimumSize(clImgSize);

   pclScrArea = new QScrollArea;
   pclScrArea->setBackgroundRole(QPalette::Dark);
   pclScrArea->setWidget(pclImg);
}
//
//  Function:  CImageViewer
//  Purpose:   Constructor, using an existing QLabel widget
//
//  Parms:     Widget
//  Returns:
//
CImageViewer::CImageViewer(QLabel *pclImageLabel)
{
   pclImg = pclImageLabel;
   pclImg->setBackgroundRole(QPalette::Base);
   pclImg->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
   pclImg->setFrameStyle(QFrame::Box);
   pclImg->setScaledContents(true);

   clImgSize = pclImg->size();
   setMinimumSize(clImgSize);

   pclScrArea = new QScrollArea;
   pclScrArea->setBackgroundRole(QPalette::Dark);
}


//
//  Function:  DisplayImageFromData
//  Purpose:   Display the picture from a byte-array buffer
//
//  Parms:     QByteArray baImage
//  Returns:   true if OKee
//
bool CImageViewer::DisplayImageFromData(QByteArray baImage)
{
   QImage clImage = QImage::fromData(baImage);

   clImgSize = clImage.size();
   //
   if (clImage.isNull()) 
   {
      fHaveImage = false;
      pclImg->setPixmap(QPixmap());
      pclImg->adjustSize();
   }
   else
   {
      fHaveImage = true;
      pclImg->setPixmap(QPixmap::fromImage(clImage));
      //
      // Scroll Area does notwork on QLabel *Fot_Image
      // This implemetation has its own widget:
      //
      //pclScrArea->setWidget(pclImg);
      //pclScrArea->viewport()->setBackgroundRole(QPalette::Dark);
      //pclScrArea->viewport()->setAutoFillBackground(true);
      //pclScrArea->setWindowTitle(QObject::tr("Picture"));
      //pclScrArea->show();
   }
   return(fHaveImage);
}

//
//  Function:  DisplayImageFromFile
//  Purpose:   Display the picture
//
//  Parms:     QByteArray baImage
//  Returns:   true if OKee
//
bool CImageViewer::DisplayImageFromFile(const QString clFile)
{
   QImage clImage(clFile);

   clImgSize = clImage.size();
   //
   if (clImage.isNull()) 
   {
      fHaveImage = false;
      pclImg->setPixmap(QPixmap());
      pclImg->adjustSize();
   }
   else
   {
      fHaveImage = true;
      pclImg->setPixmap(QPixmap::fromImage(clImage));
      //
      // Scroll Area does notwork on QLabel *Fot_Image
      // This implemetation has its own widget:
      //
      //pclScrArea->setWidget(pclImg);
      //pclScrArea->viewport()->setBackgroundRole(QPalette::Dark);
      //pclScrArea->viewport()->setAutoFillBackground(true);
      //pclScrArea->setWindowTitle(QObject::tr("Picture"));
      //pclScrArea->show();
   }
   return(fHaveImage);
}

//
//  Function:  zoomIn
//  Purpose:   Zoom in on the picture
//
//  Parms:     Destination
//  Returns:
//
bool CImageViewer::GetImageInfo(QString *pclStr)
{
   if(fHaveImage)
   {
      pclStr->sprintf("Image OK: w=%d, h=%d", clImgSize.width(), clImgSize.height());
   }
   else
   {
      pclStr->sprintf("No valid Image");
   }
   return(fHaveImage);
}

//
//  Function:  zoomIn
//  Purpose:   Zoom in on the picture
//
//  Parms:     
//  Returns:
//
void CImageViewer::zoomIn()
{
    scaleImage(1.25);
}

//
//  Function:  zoomOut
//  Purpose:   Zoom out on the picture
//
//  Parms:     
//  Returns:
//
void CImageViewer::zoomOut()
{
    scaleImage(0.8);
}

//
//  Function:  normalSize
//  Purpose:   Display image at normal size
//
//  Parms:     
//  Returns:
//
void CImageViewer::normalSize()
{
    pclImg->adjustSize();
    dScaleFactor = 1.0;
}

//
//  Function:  scaleImage
//  Purpose:   Scale the image
//
//  Parms:     Factor
//  Returns:
//
void CImageViewer::scaleImage(double dFactor)
{
    Q_ASSERT(pclImg->pixmap());
    dScaleFactor *= dFactor;
    pclImg->resize(dScaleFactor * pclImg->pixmap()->size());

    adjustScrollBar(pclScrArea->horizontalScrollBar(), dFactor);
    adjustScrollBar(pclScrArea->verticalScrollBar(), dFactor);
}

//
//  Function:  adjustScrollBar
//  Purpose:   Adjust the image scrollbar
//
//  Parms:     Scrollbar, scale
//  Returns:
//
void CImageViewer::adjustScrollBar(QScrollBar *pclScrBar, double dFactor)
{
    pclScrBar->setValue(int(dFactor * pclScrBar->value() + ((dFactor - 1) * pclScrBar->pageStep()/2)));
}

