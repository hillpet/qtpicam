/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          CImageViewer.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Image viewer class headerfile
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       18 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>

class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;

class CImageViewer : public QMainWindow
{
    Q_OBJECT

public:
    CImageViewer              ();
    CImageViewer              (QLabel *);
    //
   bool DisplayImageFromData  (QByteArray);
   bool DisplayImageFromFile  (const QString);
   bool GetImageInfo          (QString *);

private slots:

private:
   void scaleImage         (double);
   void adjustScrollBar    (QScrollBar *, double);
   void zoomIn             ();
   void zoomOut            ();
   void normalSize         ();
   void fitToWindow        ();

   bool                    fHaveImage;
   QLabel                 *pclImg;
   QScrollArea            *pclScrArea;
   QSize                   clImgSize;
   double                  dScaleFactor;
};

#endif
