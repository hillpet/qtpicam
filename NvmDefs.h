/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          NvmDefs.h
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            Define the NVM storage members
**
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28 Jun 2013
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_NVMDEFS_H_)
#define _NVMDEFS_H_

enum
{
   NUM_ELEMENTS = 0,             // Store NUM_NVMS = number of elements in storage
   NVM_VERSION,                  // Version string
   NVM_INITIALIZED,              // mark if NVM was bad !
   //
   NVM_JSON_CONFIG_FILE,         // JSON parameter file
   //
   NVM_GEN_VIEWER_DIR,           // JSON Viewer directory
   NVM_PICAM_EXECMODE,
   //
   NVM_PIX_FILTER,               // Fotos filename server side filter
   NVM_PIX_WILDCARD,             // Fotos filename client side filter
   NVM_PIX_SERVER_DIR,           // Fotos directory server side
   NVM_PIX_CLIENT_DIR,           // Fotos directory client side
   //
   NVM_VID_FILTER,               // Video filename server side filter
   NVM_VID_WILDCARD,             // Video filename client side filter
   NVM_VID_SERVER_DIR,           // Video directory server side
   NVM_VID_CLIENT_DIR,           // Video directory client side
   //
   NVM_CHECKBOX_LOG_APPEND,      // LOGfile checkbox: Append new logs
   NVM_CHECKBOX_LOG_ERRORS,      //                   Log errors
   NVM_CHECKBOX_LOG_DEBUG,       //                   Log debug
   NVM_CHECKBOX_LOG_PROGRESS,    //                   Log progress traces
   NVM_CHECKBOX_LOG_JSON,        //                   Log json results
   //     
   NVM_PATH_LOG_ERRORS,          // LOG filenames : error (default)
   NVM_PATH_LOG_DEBUG,           //                 Debug (default)
   NVM_PATH_LOG_PROGRESS,        //                 Progress traces
   NVM_PATH_LOG_JSON,            //                 json     traces
   // 
   NVM_DB_VERSION,               // NVM database version: discard settings if changed
   NUM_NVMS                      // Actual number of elements in storage
};
#endif // !defined(_NVMDEFS_H_)
