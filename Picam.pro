#-------------------------------------------------
#
# Project created by QtCreator 2014-01-27T19:44:18
#
#-------------------------------------------------

QT += core gui network widgets multimedia multimediawidgets

TARGET = Picam
TEMPLATE = app


SOURCES += main.cpp\
    dialog.cpp\
    RpiNet.cpp \
    TabGeneric.cpp \
    TabMotion.cpp \
    TabTimers.cpp \
    TabSnapshot.cpp \
    TabRecord.cpp \
    TabStreaming.cpp \
    TabFotos.cpp \
    TabVideos.cpp \
    ImageViewer.cpp

HEADERS  += dialog.h \
    NvmDefs.h \
    NvmStorage.h \
    libpatrn.h \
    PatrnFiles.h \
    RpiNet.h \
    TabGeneric.h \
    TabMotion.h \
    TabTimers.h \
    TabSnapshot.h \
    TabRecord.h \
    TabStreaming.h \
    TabFotos.h \
    TabVideos.h \
    ImageViewer.h \
    http_connections.h

FORMS    += dialog.ui

LIBS     += -L../Libs
LIBS     += -lpatrn


INCLUDEPATH += ../Include
DEPENDPATH += ../Include

DISTFILES += \
    picam.json


