/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          RpiNet.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera network comms
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       02 Jun 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <libpatrn.h>
#include <NvmStorage.h>
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Static members
//
int      CPinet::iInstance;
STCNX    CPinet::tConnection;
//
// HTTP connection strings
//
#define  EXTRACT_CNX(a,b,c)   c,
const char *pcHttpConnections[]  =
{
   #include "http_connections.h"
};
#undef   EXTRACT_CNX
//
#define  EXTRACT_CNX(a,b,c)   b,
const bool bHttpConnections[]  =
{
   #include "http_connections.h"
};
#undef   EXTRACT_CNX
//
//
const char *pcHttpGetRequest1    = "GET /";
const char *pcHttpGetRequest2    = " HTTP/1.0\r\n"
                                     "Host: www.patrn.nl\r\n"
                                     "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)\r\n"
                                     "Accept: image/png,image/*;q=0.8,*/*;q=0.5\r\n"
                                     "Accept-Encoding: gzip,deflate\r\n"
                                     "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n"
                                     "Keep-Alive: 115\r\n"
                                     "Connection: keep-alive\r\n"
                                     "\r\n";

/* ======   Local   Functions separator ===========================================
void _____CPinet_Class_____(){}
==============================================================================*/

//
//  Function:  CPinet constructor
//  Purpose:   
//
//  Parms:     
//  Returns:   
//
CPinet::CPinet()
{
   if(iInstance++ == 0) tConnection = STCNX_DISCONNECTED;
   //
   llRead      = 0;
   llToWrite   = 0;
   llWritten   = 0;
   tHttpCnx    = HCNX_IDLE;
   //
   // Connect signals
   //
   //       Signals                                              Slots
// connect( this,          SIGNAL(stateChanged(SocketState)),    this,      SLOT(ReadHttpStateChanged(SocketState))  );
   connect( this,          SIGNAL(connected()),                  this,      SLOT(WriteHttpCmd())                     );
   connect( this,          SIGNAL(bytesWritten(qint64)),         this,      SLOT(WriteHttpCmdMore(qint64))           );
   connect( this,          SIGNAL(readyRead()),                  this,      SLOT(ReadHttpReply())                    );
   connect( this,          SIGNAL(readChannelFinished()),        this,      SLOT(ReadHttpReplyReady())               );
   connect( this,          SIGNAL(disconnected()),               this,      SLOT(ReadHttpDisconnect())               );
}

//
//  Function:  CPinet constructor
//  Purpose:   
//
//  Parms:     IP address, port
//  Returns:   
//
CPinet::CPinet(QString clIp, int iPort)
{
   SetIpAddress(clIp, iPort);
   CPinet();
}

//
//  Function:  CPinet destructor
//  Purpose:   
//
//  Parms:     
//  Returns:   
//
CPinet::~CPinet()
{
}

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  GetHttpCnx
//  Purpose:   Return the connection type
//
//  Parms:     
//  Returns:   Connection type
//
HCNX CPinet::GetHttpCnx()
{
   return(tHttpCnx);
}

//
//  Function:  GetHttpRead
//  Purpose:   Return the number of bytes read
//
//  Parms:     
//  Returns:   Number of bytes read
//
qint64 CPinet::GetHttpRead()
{
   return(llRead);
}

//
//  Function:  IsConnected
//  Purpose:   Return if we have a valid connection to the server
//
//  Parms:     
//  Returns:   Connection status
//
STCNX CPinet::IsConnected()
{
   return(tConnection);
}

//
//  Function:  SetIpAddress
//  Purpose:   
//
//  Parms:     IP address, port
//  Returns:   
//
void CPinet::SetIpAddress(QString clIp, int iPort)
{
   clServerIp  = clIp;
   iServerPort = iPort;
}

//
//  Function    : SendHttpRequest
//  Description : Build and send a generic http request
//
//  Parameters  : Connection type, options
//  Returns     : TRUE if OKee
//
bool CPinet::SendHttpRequest(HCNX tCnx, ...)
{
   va_list     tParms;
   bool        fParsing=true;
   bool        fCc=true;
   bool        fBinary;
   int         i=0, iVal;
   long        lVal;
   char       *pcData, cChar;
   const char *pcPage;
   QString     clStr, clTmp;
   
   va_start(tParms, tCnx);
   //
   // Start with the HTTP GET header
   //
   if(tCnx < NUM_HCNX)
   {
      pcPage   = pcHttpConnections[tCnx];
      if(pcPage)
      {
         clStr   = pcHttpGetRequest1;
         fBinary = bHttpConnections[tCnx];
         //
         while(fParsing)
         {
            cChar = pcPage[i];
            switch(cChar)
            {
               case 0:
                  fParsing = 0;
                  break;
            
               case '%':
                  i++;
                  cChar = pcPage[i];
                  switch(cChar)
                  {
                     case '%':
                        clStr += cChar;
                        break;
               
                     case 'd':
                        iVal = va_arg(tParms, int);
                        clTmp.sprintf("%d", iVal);
                        clStr += clTmp;
                        break;
               
                     case 'x':
                        iVal = va_arg(tParms, int);
                        clTmp.sprintf("%x", iVal);
                        clStr += clTmp;
                        break;
               
                     case 'l':
                        lVal = va_arg(tParms, long);
                        clTmp.sprintf("%ld", lVal);
                        clStr += clTmp;
                        break;
               
                     case 's':
                        pcData = va_arg(tParms, char *);
                        clTmp.sprintf("%s", pcData);
                        clStr += clTmp;
                        break;
               
                     case 'S':
                        pcData = va_arg(tParms, char *);
                        clTmp.sprintf("%s", pcData);
                        clStr += clTmp;
                        break;
               
                     case 'c':
                        pcData = va_arg(tParms, char *);
                        clStr += pcData;
                        break;
               
                     case 'C':
                        pcData = va_arg(tParms, char *);
                        clStr += pcData;
                        break;
               
                     default:
                        fCc = false;
                        break;
                  }
                  break;
            
               default:
                  clStr += cChar;
                  break;
            }
            i++;
         }
         //
         // End with the rest of the HTTP header
         //
         clStr += pcHttpGetRequest2;
         fCc    = CamCommand(clStr, fBinary);
         if(fCc)
         {
            tHttpCnx = tCnx;
         }
      }
      else
      {
         tHttpCnx = tCnx;
      }
   }
   va_end(tParms);
   return(fCc);
}

//
//  Function:  TerminateHttpRequest
//  Purpose:   Terminate the connection request
//
//  Parms:     
//  Returns:   Problem
//
QString CPinet::TerminateHttpRequest()
{
   QString clStr;

   abort();
   tConnection = STCNX_DISCONNECTED;
   clStr       = errorString();
   //
   return(clStr);
}

/* ======   Local   Functions separator ===========================================
void _____Slot_Methods_____(){}
==============================================================================*/

//
//  Function:  ReadHttpStateChanged
//  Purpose:   Signal: The http state has changed
//
//  Parms:     
//  Returns:   
//
void CPinet::ReadHttpStateChanged(SocketState tState)
{
   tState = tState;
}

//
//  Function:  ReadHttpReply
//  Purpose:   Signal: Read HTTP reply from the server
//
//  Parms:     
//  Returns:   
//
void CPinet::ReadHttpReply()
{
   qint64      llAvail, llBytes;
   QByteArray  baTemp;

   llAvail = bytesAvailable();
   if(llAvail)
   {
      baTemp      = readAll();
      baHttpResp += baTemp;
      llBytes     = baTemp.length();
      llRead     += llBytes;
      emit ReadHttpProgress(llBytes);
   }
}

//
//  Function:  ReadHttpReplyReady
//  Purpose:   Signal: All JSON data received
//
//  Parms:     
//  Returns:   
//
void CPinet::ReadHttpReplyReady()
{
   //
   // See if there is some more final data
   //
   ReadHttpReply();
   //
   // Strip off the HTTP header
   //
   StripHttpResponse();
   //
   // Signal the owner we have data
   //
   emit ReadHttpReady(llRead);
}

void CPinet::ReadHttpDisconnect()
{
   tConnection = STCNX_CONNECTED;
   //
   disconnectFromHost();
   emit ReadHttpDisconnected(bBinaryData);
}


//
//  Function:  WriteHttpCmd
//  Purpose:   Signal: we are connected to the RpiCam server :
//             Send requested HTTP command to RpiCam server
//
//  Parms:     
//  Returns:   
//
void CPinet::WriteHttpCmd()
{
   qint64   llNr;

   baHttpResp.clear();
   llRead     = 0;
   bConnected = true;
   //
   // Need to check if all data has been written !!!
   //
   llNr       = write((char *)baHttpCmd.data(), llToWrite);
   llWritten += llNr;
   llToWrite -= llNr;
}

//
//  Function:  WriteHttpCmdMore
//  Purpose:   Signal: the write buffer has room for more data
//             Send more of the HTTP command to RpiCam server
//
//  Parms:     Number written last time
//  Returns:   
//
void CPinet::WriteHttpCmdMore(qint64 llWr)
{
   qint64   llNr;
   char    *pcData = (char *)baHttpCmd.data();

   llWr = llWr;

   //
   // Need to check if all data has been written !!!
   //
   if(llToWrite)
   {
      // Not all data has been written: 
      pcData    += llWritten;
      llNr       = write(pcData, llToWrite);
      llWritten += llNr;
      llToWrite -= llNr;
   }
}

/* ======   Functions separator ===========================================
void _____private_Methods_____(){}
==============================================================================*/

//
//  Function:  CamCommand
//  Purpose:   Connect to RpiCam and send out command
//
//  Parms:     HTTP request, reply is binary
//  Returns:   TRUE if OKee
//
bool CPinet::CamCommand(QString clCmd, bool bReplyBinary)
{
   baHttpCmd = clCmd.toLocal8Bit();
   return( CamCommand(bReplyBinary) );
}

//
//  Function:  CamCommand
//  Purpose:   Connect to RpiCam and send out command
//
//  Parms:     HTTP request, reply is binary
//  Returns:   TRUE if OKee
//
bool CPinet::CamCommand(QByteArray *pbaHttpCmd, bool bReplyBinary)
{
   baHttpCmd   = *pbaHttpCmd;
   return( CamCommand(bReplyBinary) );
}

//
//  Function:  CamCommand
//  Purpose:   Connect to RpiCam and send out command
//
//  Parms:     HTTP request, reply is binary
//  Returns:   TRUE if OKee
//
bool CPinet::CamCommand(char *pcHttpCmd, bool bReplyBinary)
{
   bool fCc=true;

   switch(tConnection)
   {
      case STCNX_CONNECTED:
      case STCNX_DISCONNECTED:
      case STCNX_ERROR:
         baHttpCmd   = pcHttpCmd;
         bBinaryData = bReplyBinary;
         bConnected  = false;
         llToWrite   = baHttpCmd.length();
         llWritten   = 0;
         //
         tConnection = STCNX_CONNECTING;
         connectToHost(clServerIp, iServerPort);
         break;

      case STCNX_CONNECTING:
      default:
         //
         // Connecting is still ongoing: wait till finished
         //
         fCc = false;
         break;
   }
   return(fCc);
}

//
//  Function:  CamCommand
//  Purpose:   Connect to RpiCam and send out command
//
//  Parms:     reply is binary
//  Returns:   TRUE if OKee
//
bool CPinet::CamCommand(bool bReplyBinary)
{
   bool fCc = true;

   switch(tConnection)
   {
      case STCNX_CONNECTED:
      case STCNX_DISCONNECTED:
      case STCNX_ERROR:
         bBinaryData = bReplyBinary;
         bConnected  = false;
         llToWrite   = baHttpCmd.length();
         llWritten   = 0;
         //
         tConnection = STCNX_CONNECTING;
         connectToHost(clServerIp, iServerPort);
         break;

      case STCNX_CONNECTING:
      default:
         //
         // Connecting is still ongoing: wait till finished
         //
         fCc = false;
         break;
   }
   return(fCc);
}

//
//  Function:  StripHttpResponse
//  Purpose:   Strip off the HTTP response header from the JSON reply
//                "HTTP/1.1 200 OK\r\n"
//                "...."
//                \r\n\r\n
//                http-payload
//  Parms:     
//  Returns:   TRUE if OKee
//
bool CPinet::StripHttpResponse()
{
   bool  fCc=false;
   int   iSize, iIdx;

   if( baHttpResp.contains("200 OK") )
   {
      //
      // Reply contains correct HTTP OK header: strip until end-of-header (Cr,Lf,Cr,Lf)
      //
      iSize = baHttpResp.size();
      iIdx  = baHttpResp.indexOf("\r\n\r\n", 0);
      if(iIdx > 0)
      {
         baHttpResp = baHttpResp.right(iSize-iIdx-4);
         fCc = true;
      }
   }
   return(fCc);
}
