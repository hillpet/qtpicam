/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          RpiNet.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera network comms headerfile
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       02 Jun 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef RPINET_H
#define RPINET_H

#include <QDialog>
#include <QByteArray>
#include <QTcpSocket>
//
// HTTP connection strings
//
#define  EXTRACT_CNX(a,b,c) a,
typedef enum _HCNX_
{
   #include "http_connections.h"
   NUM_HCNX
}  HCNX;
#undef   EXTRACT_CNX

typedef enum _STCNX_
{
   STCNX_CONNECTING = 0,
   STCNX_CONNECTED,
   STCNX_ERROR,
   STCNX_DISCONNECTED
}  STCNX;

//
// Set generic timeout for HTTP requests
//
#define  HTTP_CNX_TIMEOUT     1000
#define  HTTP_GEN_TIMEOUT     5000

class CPinet : public QTcpSocket
{
    Q_OBJECT

public:
               CPinet               ();
               CPinet               (QString clIp, int iPort);
              ~CPinet               ();
   //
   HCNX        GetHttpCnx           ();
   qint64      GetHttpRead          ();
   STCNX       IsConnected          ();
   bool        SendHttpRequest      (HCNX, ...);
   void        SetIpAddress         (QString clIp, int iPort);
   QString     TerminateHttpRequest ();
   //
   QByteArray  baHttpResp;
   //
private slots:
   void        WriteHttpCmd         ();
   void        WriteHttpCmdMore     (qint64);
   void        ReadHttpReply        ();
   void        ReadHttpReplyReady   ();
   void        ReadHttpDisconnect   ();
   void        ReadHttpStateChanged (SocketState);

signals:
   void        ReadHttpProgress     (qint64);
   void        ReadHttpReady        (qint64);
   void        ReadHttpDisconnected (bool);

private:
   bool        CamCommand           (bool);
   bool        CamCommand           (QString, bool);
   bool        CamCommand           (QByteArray *pbaHttpCmd, bool bReplyBinary);
   bool        CamCommand           (char       *pbaHttpCmd, bool bReplyBinary);
   bool        StripHttpResponse    ();

private:
   static int   iInstance;
   static STCNX tConnection;
   //
   HCNX        tHttpCnx;
   bool        bConnected;
   bool        bBinaryData;
   QByteArray  baHttpCmd;
   QString     clServerIp;
   int         iServerPort;
   qint64      llRead;
   qint64      llToWrite;
   qint64      llWritten;
};

#endif // RPINET_H
