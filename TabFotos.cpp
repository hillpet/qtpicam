/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabFotos.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Fotos tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QMessageBox>
#include <QFileDialog>
#include <QTimer>
#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
extern const char *pcJsonParseError;
extern const char *pcConnecting;
extern const char *pcConnectionError;
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsPathname;
extern const char *pcKeyIsFilter;
extern const char *pcKeyIsNumFiles;
extern const char *pcKeyIsFotos;
extern const char *pcKeyIsImageName;
extern const char *pcKeyIsImageSize;
extern const char *pcKeyIsValue;
//
extern const char *pcMsgParseError;

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabFotos::CTabFotos(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;

   pclUi          = pclParent;
   //
   iFotTotal      = 0;
   iFotIndex      = 0;
   pclLib         = new CPatrn; 
   pclTimer       = new QTimer(this);
   pclParJson     = new CPiJson;
   pclValJson     = new CPiJson;
   pclRpiNet      = new CPinet();
   pclNvm         = new CNvmStorage;
   pclView        = new CImageViewer(pclUi->Fot_Image);
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //                                                                                                                                      
   //       Signals                                                                    Slots
   connect( pclUi->Fot_CheckListFiles,    SIGNAL(currentRowChanged(int)),              this,             SLOT(on_Fot_CheckList_clicked(int))       );
   connect( pclUi->Fot_CheckListFiles,    SIGNAL(itemDoubleClicked(QListWidgetItem *)),this,             SLOT(on_Fot_ButtonShow_clicked())         );
   connect( pclUi->Fot_Filter,            SIGNAL(editingFinished()),                   this,             SLOT(on_Fot_FilterChanged())              );
   connect( pclUi->Fot_Wildcard,          SIGNAL(editingFinished()),                   this,             SLOT(on_Fot_WildcardChanged())            );
   connect( pclUi->Fot_ButtonShow,        SIGNAL(clicked()),                           this,             SLOT(on_Fot_ButtonShow_clicked())         );
   connect( pclUi->Fot_ButtonDelete,      SIGNAL(clicked()),                           this,             SLOT(on_Fot_ButtonDelete_clicked())       );
   connect( pclUi->Fot_ButtonDownload,    SIGNAL(clicked()),                           this,             SLOT(on_Fot_ButtonDownload_clicked())     );
   connect( pclUi->Fot_ButtonBrowse,      SIGNAL(clicked()),                           this,             SLOT(on_Fot_ButtonBrowse_clicked())       );
   connect( pclUi->Fot_ButtonAll,         SIGNAL(clicked()),                           this,             SLOT(on_Fot_ButtonAll_clicked())          );
   connect( pclUi->Fot_ButtonNone,        SIGNAL(clicked()),                           this,             SLOT(on_Fot_ButtonNone_clicked())         );
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Fot_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Fot_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Fot_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Fot_HttpDisconnected(bool))       );
   //
   fot_HandleProgressbar(true, 0);
   pclUi->Fot_Filter->setText(pclParJson->GetObjectString(pcKeyIsFilter, pcKeyIsValue));
   pclUi->Fot_Wildcard->setText("*");
   //
   //  Dialog window done
   //
   pclLib->MSG_AddText(pclUi->Fot_Messages, "TAB:Foto init OK", clStr);
}

//
//  Function:  CTabFotos destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabFotos::~CTabFotos()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
   delete pclNvm;
   delete pclView;
}

/* ======   Local   Functions separator ===========================================
void __Signal_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Fot_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabFotos::on_Fot_HttpProgress(qint64 llReceived)
{
   //test QString clStr;
   //test 
   //test clStr.sprintf("on_Fot_HttpProgress(): %lld bytes received", llReceived);
   //test pclLib->MSG_AddText(pclUi->Fot_Messages, clStr);
   //test

   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   fot_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Fot_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabFotos::on_Fot_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Fot_Messages, clStr);
}

//
//  Function:  on_Fot_HttpDisconnected
//  Purpose:   ***** HTTP disconnected from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Data is Binary
//  Returns:
//
void CTabFotos::on_Fot_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   pclTimer->stop();
   //
   if(fBinary)
   {
      fot_HandleHttpCallback(pclValJson);
   }
   else
   {
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         fot_UpdateCnxStatus(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data, result is 
         //    pclValJson->clStrList : all root key names
         //    pclValJson->clRootObj : the root object
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)
         {
            fot_HandleHttpCallback(pclValJson);
         }
         else
         {
            fot_UpdateCnxStatus(STCNX_ERROR);
         }
      }
   }              
}

//
//  Function:  on_Fot_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabFotos::on_Fot_Timeout()
{
   pclTimer->stop();
   fot_TerminateConnection(STCNX_DISCONNECTED);
}

//
//  Function:  on_Fot_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//  Note:      To get the tab info:
//             clStr = pclUi->TabWidget->tabText(iIdx);
//
void CTabFotos::on_Fot_TabChanged(int iIdx)
{
   iIdx = iIdx;

   QString clStr = pclUi->Fot_Filter->text();
   fot_ConnectToServer(HCNX_GET_FOTOS, clStr);
}

//
//  Function:  on_Fot_FilterChanged
//  Purpose:   Filter field was edited, and maybe changed
//
//  Parms:     
//  Returns:
//  Note:      
//
void CTabFotos::on_Fot_FilterChanged()
{
   QString clStr = pclUi->Fot_Filter->text();
   fot_ConnectToServer(HCNX_GET_FOTOS, clStr);
}

//
//  Function:  on_Fot_WildcardChanged
//  Purpose:   Wildcard field was edited, and maybe changed
//
//  Parms:     
//  Returns:
//  Note:      
//
void CTabFotos::on_Fot_WildcardChanged()
{
   fot_ImagesHide();
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Fot_ButtonAll_clicked
//  Purpose:   Select ALL picture
//
//  Parms:     
//  Returns:
//
void CTabFotos::on_Fot_ButtonAll_clicked()
{
   QListWidgetItem  *pclLwi;

   //
   // Set all checklistboxes of the list
   //
   for(int iIdx=0; iIdx<iFotTotal; iIdx++)
   {
      pclLwi = pclUi->Fot_CheckListFiles->item(iIdx);
      if(pclLwi->isHidden() == false)
      {
         pclLwi->setCheckState(Qt::Checked);
      }
   }
}

//
//  Function:  on_Fot_ButtonBrowse_clicked
//  Purpose:   Browse for download pathname
//
//  Parms:     
//  Returns:
//
void CTabFotos::on_Fot_ButtonBrowse_clicked()
{
   QString clDir = QFileDialog::getExistingDirectory(this, tr("Select download directory"), "/", 
                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

   pclUi->Fot_Pathname->setText(clDir);
}

//
//  Function:  on_Fot_ButtonDelete_clicked
//  Purpose:   Delete picture
//
//  Parms:     
//  Returns:
//  Note:      To display images:
//             pclView->DisplayImageFromFile("E:\\Users\\Peter\\ProjQt\\Picam\\test.jpg");
//             pclView->DisplayImageFromData(baTest);
//
void CTabFotos::on_Fot_ButtonDelete_clicked()
{
   int               iNumChecked=0;
   qint64            llTotal=0;
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   //
   // Poll all checklistboxes of the list to find out what files to delete
   //
   for(int iIdx=0; iIdx<iFotTotal; iIdx++)
   {
      pclLwi = pclUi->Fot_CheckListFiles->item(iIdx);
      switch( pclLwi->checkState() )
      {
         case Qt::Checked:
            if(pclLwi->isHidden() == false)
            {
               iNumChecked++;
               clVar    = pclLwi->data(Qt::UserRole);
               llTotal += clVar.toInt();
            }
            break;

         default:
            break;
      }
   }
   if(llTotal)
   {
      //
      // Setup the progress bar
      //
      fot_HandleProgressbar(true, llTotal); 
      //
      // Start deleting
      //
      fot_CallbackDeleteNextImage();
   }
   clStr.sprintf("Delete %d Images: %lld bytes", iNumChecked, llTotal);
   pclLib->MSG_AddText(pclUi->Fot_Messages, clStr);
}

//
//  Function:  on_Fot_ButtonDownload_clicked
//  Purpose:   Delete picture
//
//  Parms:     
//  Returns:
//  Note:      To display images:
//             pclView->DisplayImageFromFile("E:\\Users\\Peter\\ProjQt\\Picam\\test.jpg");
//             pclView->DisplayImageFromData(baTest);
//
void CTabFotos::on_Fot_ButtonDownload_clicked()
{
   int               iNumChecked=0;
   qint64            llTotal=0;
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   //
   // Poll all checklistboxes of the list to find out what files to download
   //
   for(int iIdx=0; iIdx<iFotTotal; iIdx++)
   {
      pclLwi = pclUi->Fot_CheckListFiles->item(iIdx);
      switch( pclLwi->checkState() )
      {
         case Qt::Checked:
            if(pclLwi->isHidden() == false)
            {
               iNumChecked++;
               clVar    = pclLwi->data(Qt::UserRole);
               llTotal += clVar.toInt();
            }
            break;

         default:
            break;
      }
   }
   if(llTotal)
   {
      //
      // Setup the progress bar
      //
      fot_HandleProgressbar(true, llTotal); 
      //
      // Start downloading
      //
      fot_CallbackDownloadNextImage();
   }
   clStr.sprintf("Download %d Images: %lld bytes", iNumChecked, llTotal);
   pclLib->MSG_AddText(pclUi->Fot_Messages, clStr);
}

//
//  Function:  on_Fot_ButtonNone_clicked
//  Purpose:   Select NO picture
//
//  Parms:     
//  Returns:
//
void CTabFotos::on_Fot_ButtonNone_clicked()
{
   QListWidgetItem  *pclLwi;

   pclLib->MSG_AddText(pclUi->Fot_Messages);  //clear widget text
   //
   // Set all checklistboxes of the list
   //
   for(int iIdx=0; iIdx<iFotTotal; iIdx++)
   {
      pclLwi = pclUi->Fot_CheckListFiles->item(iIdx);
      if(pclLwi->isHidden() == false)
      {
         pclLwi->setCheckState(Qt::Unchecked);
      }
   }
}

//
//  Function:  on_Fot_ButtonShow_clicked
//  Purpose:   Show picture
//
//  Parms:     
//  Returns:
//  Note:      To display images:
//             pclView->DisplayImageFromFile("E:\\Users\\Peter\\ProjQt\\Picam\\test.jpg");
//             pclView->DisplayImageFromData(baTest);
//
void CTabFotos::on_Fot_ButtonShow_clicked()
{
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   //
   // Get the current selection of the listbox
   //
   pclLwi = pclUi->Fot_CheckListFiles->currentItem();
   if(pclLwi)
   {
      //
      // Get image size for progress bar
      //
      clVar = pclLwi->data(Qt::UserRole);
      fot_HandleProgressbar(true, clVar.toInt()); 
      //
      clStr  = clFotPath;
      clStr += pclLwi->text();
      fot_ConnectToServer(HCNX_SHW_FOTO, clStr);
   }
}

//
//  Function:  on_Fot_CheckList_clicked
//  Purpose:   One of the items in the pixture list has been selected (mouse or keys)
//             Only called if a different item in the pixture list has been selected
//
//  Parms:     Index
//  Returns:
//  Note:      Can be called on tab changes with iIdx = -1 !
//
void CTabFotos::on_Fot_CheckList_clicked(int iIdx)
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   if(iIdx >= 0)
   {
      //
      // Get the current row index of the listbox
      //
      iFotIndex = iIdx;
      pclLwi    = pclUi->Fot_CheckListFiles->currentItem();
      clStr     = clFotPath;
      clStr    += pclLwi->text();
   }
}

/* ======   Private Functions separator ===========================================
void __Private_Methods________(){}
==============================================================================*/

//
//  Function:  fot_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabFotos::fot_ConnectToServer(HCNX tReq)
{
   fot_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  fot_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabFotos::fot_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   fot_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  fot_GetNextImage
//  Purpose:   Retrieve the next checked file from the list widget
//
//  Parms:     
//  Returns:   ListWidgetItem *
//
QListWidgetItem *CTabFotos::fot_GetNextImage()
{
   QListWidgetItem  *pclLwi;

   //
   // Check all checklistboxes of the list to find the next image
   //
   for(int iIdx=0; iIdx<iFotTotal; iIdx++)
   {
      pclLwi = pclUi->Fot_CheckListFiles->item(iIdx);
      if( pclLwi && (pclLwi->isHidden() == false) )
      {
         if(pclLwi->checkState() == Qt::Checked)
         {
            return(pclLwi);
         }
      }
   }
   return(NULL);
}

//
//  Function:  fot_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabFotos::fot_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Fot_ProgBar->setMaximum(1000);
      pclUi->Fot_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Fot_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Fot_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  fot_ImagesBuildList
//  Purpose:   Fill the checklist widget with all the pixtures
//
//  Parms:     
//  Returns:
//
void CTabFotos::fot_ImagesBuildList(CPiJson *pclJson)
{
   int               iFotSize;
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   clFotPath   = pclJson->GetObjectString(pcKeyIsPathname);
   iFotTotal   = pclJson->GetObjectInteger(pcKeyIsNumFiles);
   //
   // Delete List, if any
   // Disable sorting
   //
   if(pclUi->Fot_CheckListFiles) 
   {
      pclUi->Fot_CheckListFiles->clear();
      pclUi->Fot_CheckListFiles->setSortingEnabled(false);
      //
      for(int i=0; i<iFotTotal; i++)
      {
         clStr    = pclJson->GetObjectString(pcKeyIsFotos, pcKeyIsImageName, i);
         iFotSize = pclJson->GetObjectInteger(pcKeyIsFotos, pcKeyIsImageSize, i);
         //
         // Store this pixture filename into the QListWidget
         //
         pclLwi = new QListWidgetItem(clStr, pclUi->Fot_CheckListFiles);
         //
         // Reset checkbox value
         //
         pclLwi->setCheckState(Qt::Unchecked);
         pclLwi->setSelected(false);
         //
         clVar = iFotSize;
         pclLwi->setData(Qt::UserRole, clVar);
      }
      fot_ImagesHide();
      iFotIndex = 0;
      pclUi->Fot_CheckListFiles->setCurrentRow(iFotIndex);
   }
}

//
//  Function:  fot_ImagesHide
//  Purpose:   Hide all images in the list that do not match the current wildcard
//
//  Parms:     
//  Returns:   Not hidden number in the list
//
int CTabFotos::fot_ImagesHide()
{
   QString           clStr, clWld;
   QListWidgetItem  *pclLwi;

   clWld = pclUi->Fot_Wildcard->text();
   //
   for(int iIdx=0; iIdx<iFotTotal; iIdx++)
   {
      pclLwi = pclUi->Fot_CheckListFiles->item(iIdx);
      clStr = pclLwi->text();
      pclLwi->setHidden((fot_WildcardMatch(clWld, clStr, true)==false));
   }
   return(pclUi->Fot_CheckListFiles->count());
}

//
//  Function:  fot_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabFotos::fot_TerminateConnection(STCNX tStatus)
{
   fot_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  fot_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabFotos::fot_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Fot_Status->setText(pcConnecting);
         pclUi->Fot_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Fot_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Fot_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Fot_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Fot_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Fot_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Fot_Status->setText(pcConnectionError);
         break;
   }
}

//
//  Function:  fot_SaveImage
//  Purpose:   Save the image we just received from the Raspberry
//
//  Parms:     Filename
//  Returns:   true if OKee
//
bool CTabFotos::fot_SaveImage(QString clName)
{
   bool    fCc=false;
   QString clPath=pclUi->Fot_Pathname->text();

   if( clPath.length() > 0)
   {
      clPath += "\\";
      clPath += clName;
      QFile clFile(clPath);

      if( clFile.open(QIODevice::WriteOnly) )
      {
         clFile.write(pclRpiNet->baHttpResp);
         clFile.close();
         fCc = true;
      }
   }
   //
   // Verify if all went OKee
   //
   if(fCc == false)
   {
      QMessageBox::information(this, QGuiApplication::applicationDisplayName(), tr("Error saving image: check download path !"));
   }
   return(fCc);
}

//
//  Function:  fot_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabFotos::fot_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   fot_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Fot_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  fot_WildcardMatch
//  Purpose:   Check if the string matches the wildcard search
//
//  Parms:     Wildcard string, Destination string, NoCase YESNO
//  Returns:   
//
bool CTabFotos::fot_WildcardMatch(QString sWld, QString sSrc, bool fIgnoreCase)
{
   QString  clWld, clSrc;
   int      iS=0, iW=0, iWcopy=-1;

   clWld = sWld;
   clSrc  = sSrc;
   //
   if(fIgnoreCase)
   {
      // Ignore case
      clWld.toUpper();
      clSrc.toUpper();
   }
   while( (clWld[iW] != 0) && (clSrc[iS] != 0) )
   {
      if(clWld[iW] == '*')
      {
         iWcopy = iW;
         if(clSrc[iS++] == clWld[iW+1])
         {
            iW += 2;
         }
      }
      else if(clWld[iW] == '?') 
      {
         iS++;
         iW++;
      }
      else if(clWld[iW++] != clSrc[iS++])
      {
         if(iWcopy != -1)
         {
            // We have an ongoing * match
            iW = iWcopy;
         }
         else
         {
            return(false);
         }
      }
   }
   //
   while(clWld[iW] == '*') 
   {
      iW++;
   }
   return(clWld[iW] == 0);
}

/* ======   Private Functions separator ===========================================
void __Callback_Methods________(){}
==============================================================================*/

//
//  Function:  fot_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabFotos::fot_HandleHttpCallback(CPiJson *pclJson)
{
   QListWidgetItem  *pclLwi;

   switch( pclRpiNet->GetHttpCnx() )
   {
      default:
         fot_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_IDLE:
         break;

      case HCNX_DLD_FILE:
         //
         // Request went out to download (an) image(s)
         //    o Store this image
         //    o download more
         //
         pclLwi = fot_GetNextImage();
         if(pclLwi)
         {
            fot_SaveImage(pclLwi->text());
            //
            // Image has been handled: forget it
            //
            pclLwi->setCheckState(Qt::Unchecked);
            fot_CallbackDownloadNextImage();
         }
         else fot_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_SHW_FOTO:
         //
         // Request went out to display an image
         //
         fot_TerminateConnection(STCNX_CONNECTED);
         fot_CallbackShowPixture();
         break;

      case HCNX_GET_FOTOS:
         //
         // Request went out to download the relevant data of all fotos matching the
         // filtersettings;
         //
         fot_TerminateConnection(STCNX_CONNECTED);
         fot_ImagesBuildList(pclJson);
         break;

      case HCNX_DEL_FOTOS:
         //
         // Request went out to delete (an) image(s)
         //    o update deleted bytes
         //    o delete more
         //
         pclLwi = fot_GetNextImage();
         if(pclLwi)
         {
            QVariant clVar = pclLwi->data(Qt::UserRole);
            fot_HandleProgressbar(false, clVar.toInt());
            //
            // Image has been handled: forget it
            //
            pclLwi->setCheckState(Qt::Unchecked);
            fot_CallbackDeleteNextImage();
         }
         else fot_TerminateConnection(STCNX_CONNECTED);
         break;
   }
}

//
//  Function:  fot_CallbackDeleteNextImage
//  Purpose:   Handle the deletion of the next checked item
//
//  Parms:     
//  Returns:
//
void CTabFotos::fot_CallbackDeleteNextImage()
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   //
   // Check all checklistboxes of the list to find the next image
   //
   pclLwi = fot_GetNextImage();
   if(pclLwi)
   {
      //
      // Delete this image
      //
      clStr = pclLwi->text();
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Delete:", clStr);
      fot_ConnectToServer(HCNX_DEL_FOTOS, clStr);
   }
   else
   {
      //
      // All images deleted: rebuild file list
      //
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Deleted Images, total size=", fot_HandleProgressbar(true, 0));
      on_Fot_FilterChanged();
   }
}

//
//  Function:  fot_CallbackShowPixture
//  Purpose:   Http server has responded: display the returned image
//
//  Parms:     
//  Returns:
//
void CTabFotos::fot_CallbackShowPixture()
{
   QString clStr;

   pclView->DisplayImageFromData(pclRpiNet->baHttpResp);
   pclView->GetImageInfo(&clStr);
   pclLib->MSG_AddText(pclUi->Fot_Messages, clStr);
}

//
//  Function:  fot_CallbackDownloadNextImage
//  Purpose:   Handle the download of the next checked item
//
//  Parms:     
//  Returns:
//
void CTabFotos::fot_CallbackDownloadNextImage()
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   //
   // Check all checklistboxes of the list to find the next image
   //
   pclLwi = fot_GetNextImage();
   if(pclLwi)
   {
      //
      // Download this image
      //
      clStr  = clFotPath;
      clStr += pclLwi->text();
      pclLib->MSG_AddText(pclUi->Fot_Messages, "Download: ", clStr);
      fot_ConnectToServer(HCNX_DLD_FILE, clStr);
   }
   else
   {
      //
      // All images downloaded: rebuild file list
      //
      on_Fot_FilterChanged();
      clStr.sprintf("Downloaded all Images: %lld bytes", fot_HandleProgressbar(true, 0));
      pclLib->MSG_AddText(pclUi->Fot_Messages, clStr);
   }
}

