/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabFotos.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Fotos tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABFOTOS_H
#define TABFOTOS_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include "ui_dialog.h"
#include <RpiJson.h>
#include "RpiNet.h"
#include "ImageViewer.h"


class CTabFotos : public QDialog
{
    Q_OBJECT

public:
   CTabFotos(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabFotos();
  //

signals:

private slots:
   void              on_Fot_ButtonAll_clicked      ();
   void              on_Fot_ButtonDelete_clicked   ();
   void              on_Fot_ButtonDownload_clicked ();
   void              on_Fot_ButtonNone_clicked     ();
   void              on_Fot_ButtonShow_clicked     ();
   void              on_Fot_ButtonBrowse_clicked   ();
   void              on_Fot_CheckList_clicked      (int);
   void              on_Fot_HttpProgress           (qint64);
   void              on_Fot_HttpDisconnected       (bool);
   void              on_Fot_HttpReady              (qint64);
   void              on_Fot_TabChanged             (int);
   void              on_Fot_Timeout                ();
   void              on_Fot_FilterChanged          ();
   void              on_Fot_WildcardChanged        ();
   //
private:
   void              fot_ConnectToServer           (HCNX);
   void              fot_ConnectToServer           (HCNX, QString);
   QListWidgetItem  *fot_GetNextImage              ();
   qint64            fot_HandleProgressbar         (bool, qint64);
   void              fot_ImagesBuildList           (CPiJson *);
   int               fot_ImagesHide                ();
   bool              fot_SaveImage                 (QString);
   void              fot_SetupConnection           (HCNX);
   void              fot_TerminateConnection       (STCNX);
   void              fot_UpdateCnxStatus           (STCNX);
   bool              fot_WildcardMatch             (QString, QString, bool);
   //
   void              fot_HandleHttpCallback        (CPiJson *);
   void              fot_CallbackDeleteNextImage   ();
   void              fot_CallbackDownloadNextImage ();
   void              fot_CallbackShowPixture       ();
   //
   Ui::Dialog       *pclUi;
   //
   int               iFotIndex;
   int               iFotTotal;
   qint64            llToDownload;
   qint64            llDownloaded;
   QString           clFotPath;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclParJson;
   CPiJson          *pclValJson;
   CNvmStorage      *pclNvm;
   CImageViewer     *pclView;
};

#endif // TABFOTOS_H
