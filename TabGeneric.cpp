/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabGeneric.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Generic tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28 Jul 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QTimer>
#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"


//
// Cam JSON parameter file: picam.json
//
static const char *pcIsTextSwVersion   = "v";
//
extern const char *pcJsonParseError;
extern const char *pcConnecting;
extern const char *pcConnectionError;
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsSwVersion;
extern const char *pcKeyIsDescription;
extern const char *pcKeyIsSelected;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsObject;
extern const char *pcKeyIsName;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsNumparms;
extern const char *pcKeyIsType;
extern const char *pcKeyIsValue;
//
extern const char *pcMsgParseError;

/* ======   Local   Functions separator ===========================================
void __Public_Methods______________(){}
==================================X==============================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window, Global JSON Doc
//  Returns:
//  Note:      This generic TAB is selected by the ServerAutoConnect signal emitted by the
//             dialog class !
//
CTabGeneric::CTabGeneric(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString        clStr;

   iCnxTimeout    = 0;
   iNumParameters = 0;
   iCurParameter  = 0;
   //
   pclUi      = pclParent;
   pclLib     = new CPatrn; 
   pclTimer   = new QTimer(this);
   pclParJson = new CPiJson;
   pclValJson = new CPiJson;
   pclNvm     = new CNvmStorage;
   //
   // Retrieve all JSON root objects from the config file and build the parameter checklistbox
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      iNumParameters = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNumParameters);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   gen_BuildParameterList(pclParJson);
   //
   // Build dropdown list with any possible value widgets
   //
   gen_BuildValueFields(pclParJson);
   gen_UpdateCnxSettings();
   //
   // Retrieve Raspberry Pi HTTP server connection details
   // Connect to Raspberry Pi: retrieve cam.json parms
   //
   pclRpiNet = new CPinet();
   //
   //    Define signals and slots
   //
   //       Signals                                                                 Slots
   connect( pclUi->Gen_ButtonConnect,     SIGNAL(clicked()),                        this,          SLOT(on_Gen_ButtonConnect_clicked()) );
   connect( pclUi->Gen_ButtonJsonSave,    SIGNAL(clicked()),                        this,          SLOT(on_Gen_ButtonJsonSave_clicked()) );
   connect( pclUi->Gen_ButtonJsonView,    SIGNAL(clicked()),                        this,          SLOT(on_Gen_ButtonJsonView_clicked()) );
   connect( pclUi->Gen_ButtonJsonBrowse,  SIGNAL(clicked()),                        this,          SLOT(on_Gen_ButtonJsonBrowse_clicked()) );
   //
   connect( pclUi->Gen_ComboParm,         SIGNAL(activated(int)),                   this,          SLOT(on_Gen_ComboParm_activated(int)) );
   //
   connect( pclUi->Gen_EditParm1,         SIGNAL(editingFinished()),                this,          SLOT(on_Gen_ValueFieldsFinished()) );
   connect( pclUi->Gen_EditParm2,         SIGNAL(editingFinished()),                this,          SLOT(on_Gen_ValueFieldsFinished()) );
   connect( pclUi->Gen_EditParm3,         SIGNAL(editingFinished()),                this,          SLOT(on_Gen_ValueFieldsFinished()) );
   connect( pclUi->Gen_EditParm4,         SIGNAL(editingFinished()),                this,          SLOT(on_Gen_ValueFieldsFinished()) );
   //
   connect( pclUi->Gen_CheckListParms,    SIGNAL(itemPressed(QListWidgetItem *)),   this,          SLOT(on_Gen_CheckList_checked(QListWidgetItem *)) );
   connect( pclUi->Gen_CheckListParms,    SIGNAL(currentRowChanged(int)),           this,          SLOT(on_Gen_CheckList_clicked(int)) );
   connect( pclUi->Gen_CheckListParms,    SIGNAL(itemChanged(QListWidgetItem *)),   this,          SLOT(on_Gen_CheckList_changed(QListWidgetItem *)) );
   //
   connect( pclUi->Gen_ButtonOkCancel,    SIGNAL(clicked(QAbstractButton *)),       this,          SLOT(on_Gen_ButtonOkCancel_clicked(QAbstractButton *)) );
   //
   connect( pclTimer,                     SIGNAL(timeout()),                        this,          SLOT(on_Gen_Timeout()) );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),         this,          SLOT(on_Gen_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),            this,          SLOT(on_Gen_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),       this,          SLOT(on_Gen_HttpDisconnected(bool))       );
   //
   //  Dialog window done
   //
   pclLib->MSG_AddText(pclUi->Gen_Messages, "TAB:Generic init OK", clStr);
}

//
//  Function:  CTabGeneric destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabGeneric::~CTabGeneric()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
   delete pclNvm;
}

//
//  Function:  on_Gen_ServerAutoConnect
//  Purpose:   Auto-connect to Raspberry Pi server
//
//  Parms:     
//  Returns:
//
void CTabGeneric::on_Gen_ServerAutoConnect()
{
   on_Gen_ButtonConnect_clicked();
}

/* ======   Local   Functions separator ===========================================
void __Signal_Handlers___________(){}
==============================================================================*/

//
//  Function:  on_Gen_HttpProgress
//  Purpose:   ***** HTTP progress report from the Raspberry Pi has been received ****
//
//  Parms:     Bytes received so far
//  Returns:
//
void CTabGeneric::on_Gen_HttpProgress(qint64 llWritten)
{
   llWritten = llWritten;
   pclTimer->start(HTTP_CNX_TIMEOUT); //Restart time-out
}

//
//  Function:  on_Gen_HttpReady
//  Purpose:   ***** HTTP ready from the Raspberry Pi has been received ****
//
//  Parms:     Bytes received so far
//  Returns:
//
void CTabGeneric::on_Gen_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Gen_Messages, clStr);
}

//
//  Function:  on_Gen_HttpDisconnected
//  Purpose:   ***** HTTP reply from the Raspberry Pi has been received ****
//             Parse the JSON document received from the server
//
//  Parms:     Data is binary (or JSON text)
//  Returns:
//
void CTabGeneric::on_Gen_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   pclTimer->stop();
   //
   if(fBinary)
   {
      gen_HandleHttpCallback(pclValJson);
   }
   else
   {
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         gen_TerminateConnection(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)
         {
            clStr  = pcIsTextSwVersion;
            clStr += pclValJson->GetObjectString(pcKeyIsSwVersion);
            pclUi->Gen_SwVersion->setText(clStr);
            //
            gen_HandleHttpCallback(pclValJson);
         }
         else
         {
            gen_TerminateConnection(STCNX_ERROR);
         }
      }
   }
}

//
//  Function:  on_Gen_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabGeneric::on_Gen_Timeout()
{
   pclTimer->stop();
   //
   switch( pclRpiNet->GetHttpCnx() )
   {
      default:
         break;

      case HCNX_CAM_PARMS:
         gen_TerminateConnection(STCNX_DISCONNECTED);
         pclLib->MSG_AddText(pclUi->Gen_Messages, "Connection Timeout !");
         break;
   
      case HCNX_IDLE:
         break;
   }
}

//
//  Function:  on_Gen_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//
void CTabGeneric::on_Gen_TabChanged(int iIdx)
{
   QString clStr = pclUi->TabWidget->tabText(iIdx);
   pclLib->MSG_AddText(pclUi->Gen_Messages, clStr);
   //
   // Reconnect
   //
   on_Gen_ButtonConnect_clicked();
}

/* ======   Local   Functions separator ===========================================
void __Button_Handlers_____________(){}
==================================X==============================================*/

//
//  Function:  on_Gen_ButtonConnect_clicked
//  Purpose:   Connect to new IP settings
//
//  Parms:     
//  Returns:
//
void CTabGeneric::on_Gen_ButtonConnect_clicked()
{
   gen_ConnectToServer(HCNX_CAM_PARMS);
}

//
//  Function:  on_Gen_ButtonJsonSave_clicked
//  Purpose:   Save JSON parameter to file
//
//  Parms:     
//  Returns:
//
void CTabGeneric::on_Gen_ButtonJsonSave_clicked()
{
   QString  clStr;

   pclNvm->NvmGet(NVM_JSON_CONFIG_FILE, &clStr);
   if(pclParJson->WriteDocumentFile(clStr) )
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "Json parameters saved");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "Error saving JSON parameters!");
   }
}

//
//  Function:  on_Gen_ButtonJsonView_clicked
//  Purpose:   View JSON parameters
//
//  Parms:     
//  Returns:
//
void CTabGeneric::on_Gen_ButtonJsonView_clicked()
{
   pclLib->MSG_AddText(pclUi->Gen_Messages, "Json View: needs implementation");
}

//
//  Function:  on_Gen_ButtonJsonBrowse_clicked
//  Purpose:   Browse for JSON viewer exec
//
//  Parms:     
//  Returns:
//
void CTabGeneric::on_Gen_ButtonJsonBrowse_clicked()
{
   pclUi->Gen_Messages->clear();
   pclLib->MSG_AddText(pclUi->Gen_Messages, "Json Click: needs implementation");
}

//
//  Function:  on_Gen_ComboParm_activated
//  Purpose:   A different item in the ComboBox drop-down list has been selected
//             The ComboParm list contains all URL's.
//  Parms:     New combobox drop-down index
//  Returns:
//  Note:      The new IP/Port needs to be propagated to the other tab as well.
//
void CTabGeneric::on_Gen_ComboParm_activated(int iComboIdx)
{
   int      iVal;
   QString  clStr;

   iVal  = pclParJson->GetObjectInteger(iCurParameter, pcKeyIsValue, 0);
   if(iVal != iComboIdx)
   {
      //
      // Value has changed: update "value" array
      //
      pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 0, iComboIdx);
      //
      // Update the widgets on-screen to reflect the correct URL
      //
      gen_UpdateCnxSettings();
      //
      // Make new data available to all tabs
      //
      iNumParameters = pclParJson->ParseGlobalParameters();
   }
}

//  Function:  on_Gen_CheckList_changed
//  Purpose:   One of the checkboxes in the parameter list has been toggled
//
//  Parms:     Checklist Item *
//  Returns:
//
void CTabGeneric::on_Gen_CheckList_changed(QListWidgetItem *pclLwi)
{

   //== Show state and JSOON storage here
   // QString clTmp = pclLwi->text(); //test
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_changed():", clTmp);
   // clTmp = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_changed():Parameter=", clTmp);
   //== Show state and JSOON storage here

   pclLwi->setSelected(true);
   //
   // We need to get the checkstate ourselves here:
   //
   switch( pclLwi->checkState() )
   {
      case Qt::Checked:
         //
         // Store new value into parameters "selected" field
         //
         iCurParameter = pclUi->Gen_CheckListParms->row(pclLwi);
         pclParJson->PutRootKey(iCurParameter, pcKeyIsSelected, true);
         gen_BuildValueFields(pclParJson);
         break;

      case Qt::Unchecked:
         //
         // Store new value into parameters "selected" field
         //
         iCurParameter = pclUi->Gen_CheckListParms->row(pclLwi);
         pclParJson->PutRootKey(iCurParameter, pcKeyIsSelected, false);
         gen_BuildValueFields(pclParJson);
         break;

      default:
         break;
   }
   //== Show state and JSOON storage here
   // clTmp = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_changed():Parameter (on exit)=", clTmp);
   //== Show state and JSOON storage here
}

//
//  Function:  on_Gen_CheckList_checked
//  Purpose:   One of the items in the parameter list has been selected (mouse or keys)
//
//  Parms:     Checklist Item *
//  Returns:
//
void CTabGeneric::on_Gen_CheckList_checked(QListWidgetItem *pclLwi)
{
   pclLwi = pclLwi;

   //== Show state and JSON storage here
   // QString clTmp = pclLwi->text(); //test
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_checked():", clTmp);
   // clTmp = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_checked():Parameter=", clTmp);
   //== Show state and JSOON storage here
}

//
//  Function:  on_Gen_CheckList_clicked
//  Purpose:   One of the items in the parameter list has been selected (mouse or keys)
//             Only called if a different item in the parameter list has been selected
//
//  Parms:     Index
//  Returns:
//
void CTabGeneric::on_Gen_CheckList_clicked(int iIdx)
{
   QListWidgetItem  *pclLwi;

   //
   // Get the current row index of the listbox
   //
   iCurParameter = iIdx;
   pclLwi = pclUi->Gen_CheckListParms->currentItem();

   //== Show state and JSOON storage here
   // QString clTmp = pclLwi->text();
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_clicked():", clTmp);
   // clTmp = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_clicked():Parameter=", clTmp);
   //== Show state and JSOON storage here

   switch( pclLwi->checkState() )
   {
      case Qt::Checked:
         //pclParJson->PutRootKey(iCurParameter, pcKeyIsSelected, true);
         gen_BuildValueFields(pclParJson);
         break;

      case Qt::Unchecked:
         //pclParJson->PutRootKey(iCurParameter, pcKeyIsSelected, false);
         gen_BuildValueFields(pclParJson);
         break;

      default:
         break;
   }
   //== Show state and JSOON storage here
   // clTmp = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_CheckList_clicked():Parameter (on exit)=", clTmp);
   //== Show state and JSOON storage here
}

//
//  Function:  on_Gen_ButtonOkCancel_clicked
//  Purpose:   OK/Cancel button pressed
//
//  Parms:     QAbstractButton *
//  Returns:
//
void CTabGeneric::on_Gen_ButtonOkCancel_clicked(QAbstractButton *pclButton)
{
   switch( pclUi->Gen_ButtonOkCancel->buttonRole(pclButton))
   {
      case QDialogButtonBox::AcceptRole:
         //
         // OK button pressed
         //
         break;

      default:
      case QDialogButtonBox::RejectRole:
         //
         // CANCEL button pressed
         //
         break;
   }
}

//
//  Function:  on_Gen_ValueFieldsFinished
//  Purpose:   One of the parameter "value" fields have been changed
//             The fields may hold different types of data, depending the definition in the
//             JSON parameter config file: root."type"=
//                bool    : "value" is true/false
//                integer : "value" is a number
//                string  : "value" is a text string
//                enum    : "value" is an index in the enum list
//                object  : "value" is an index in a list of objects
//
//  Parms:     
//  Returns:   
//
void CTabGeneric::on_Gen_ValueFieldsFinished()
{
   PTYPE    tType;
   QString  clStr;

   //== Show state and JSOON storage here
   // clStr = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_ValueFieldsFinished():Parameter", clStr);
   //== Show state and JSOON storage here
   
   tType = pclLib->LIB_GetRootType(pclParJson, iCurParameter);
   switch(tType)
   {
      case PTYPE_STRING:
         clStr = pclUi->Gen_EditParm1->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 0, clStr);
         //
         clStr = pclUi->Gen_EditParm2->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 1, clStr);
         //
         clStr = pclUi->Gen_EditParm3->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 2, clStr);
         //
         clStr = pclUi->Gen_EditParm4->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 3, clStr);
         break;

      case PTYPE_BOOL:
         clStr = pclUi->Gen_EditParm1->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 0, (clStr.toInt()?true:false) );
         //
         clStr = pclUi->Gen_EditParm2->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 1, (clStr.toInt()?true:false) );
         //
         clStr = pclUi->Gen_EditParm3->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 2, (clStr.toInt()?true:false) );
         //
         clStr = pclUi->Gen_EditParm4->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 3, (clStr.toInt()?true:false) );
         break;

      case PTYPE_INTEGER:
         clStr = pclUi->Gen_EditParm1->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 0, clStr.toInt());
         //
         clStr = pclUi->Gen_EditParm2->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 1, clStr.toInt());
         //
         clStr = pclUi->Gen_EditParm3->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 2, clStr.toInt());
         //
         clStr = pclUi->Gen_EditParm4->text();
         pclParJson->PutRootKeyItem(iCurParameter, pcKeyIsValue, 3, clStr.toInt());
         break;

      default:
      case PTYPE_ENUM:
      case PTYPE_OBJECT:
         break;
   }

   //== Show state and JSOON storage here
   // clStr = pclParJson->GetObjectName(iCurParameter);
   // pclLib->MSG_AddText(pclUi->Gen_Messages, "on_Gen_ValueFieldsFinished():Parameter (on exit)", clStr);
   //== Show state and JSOON storage here
   
   pclLib->MSG_AddText(pclUi->Gen_Messages, "Value Field changed");
}

/* ======   Local   Functions separator ===========================================
void __Private_methods_____________(){}
==================================X==============================================*/

//
//  Function:  gen_BuildCompoundComboList
//  Purpose:   Build the ComboBox containing the current parameter value options
//
//  Parms:     Json ptr
//  Returns:   
//
void CTabGeneric::gen_BuildCompoundComboList(CPiJson *pclJson)
{
   int         iIdx, iNumElements, iVal;
   JTYPE       tType;
   QString     clStr;
   
   //
   // "Address"                           key-pair name
   //    "type":  "object"                key-pair type
   //    "enum":  [ {.., ...}, {...}]
   //    "value": [..]
   //    "...":   ...
   // "AutoBw"
   //    "type":  "enum"
   //    "enum":[.., ...]
   //    "value":[..]
   //
   // Get number of elements in the enum-array
   //
   tType        = pclJson->GetObjectType(iCurParameter, pcKeyIsEnum, pcKeyIsName);
   iNumElements = pclJson->GetArrayNumElements();
   //
   // Retrieve the object.members
   //
   pclUi->Gen_ComboParm->clear();
   for(iIdx=0; iIdx<iNumElements; iIdx++)
   {
      tType = pclJson->GetObjectType(iCurParameter, pcKeyIsEnum, pcKeyIsName, iIdx);
      switch(tType)
      {
         default:
            break;

         case JTYPE_INTEGER:
            iVal = pclJson->GetValueInteger();
            clStr.sprintf("%d", iVal);
            pclUi->Gen_ComboParm->addItem(clStr);
            break;

         case JTYPE_BOOL:
            if(pclJson->GetValueBool() == true) pclUi->Gen_ComboParm->addItem("true");
            else                                pclUi->Gen_ComboParm->addItem("false");
            break;

         case JTYPE_STRING:
            clStr = pclJson->GetValueString();
            pclUi->Gen_ComboParm->addItem(clStr);
            break;
      }
   }
   iIdx  = pclJson->GetObjectInteger(iCurParameter, pcKeyIsValue, 0);
   pclUi->Gen_ComboParm->setCurrentIndex(iIdx);
}

//
//  Function:  gen_BuildFlatComboList
//  Purpose:   Build the ComboBox containing the current parameter value options
//
//  Parms:     Json ptr
//  Returns:   
//
void CTabGeneric::gen_BuildFlatComboList(CPiJson *pclJson)
{
   int         iIdx, iNumElements, iVal;
   JTYPE       tType;
   QString     clStr;
   
   tType        = pclJson->GetObjectType(iCurParameter, pcKeyIsEnum, NULL);
   iNumElements = pclJson->GetArrayNumElements();
   //
   // Retrieve the object.members
   //
   pclUi->Gen_ComboParm->clear();
   for(iIdx=0; iIdx<iNumElements; iIdx++)
   {
      tType = pclJson->GetObjectType(iCurParameter, pcKeyIsEnum, NULL, iIdx);
      switch(tType)
      {
         default:
            break;

         case JTYPE_INTEGER:
            iVal = pclJson->GetValueInteger();
            clStr.sprintf("%d", iVal);
            pclUi->Gen_ComboParm->addItem(clStr);
            break;

         case JTYPE_BOOL:
            if(pclJson->GetValueBool() == true) pclUi->Gen_ComboParm->addItem("true");
            else                                pclUi->Gen_ComboParm->addItem("false");
            break;

         case JTYPE_STRING:
            clStr = pclJson->GetValueString();
            pclUi->Gen_ComboParm->addItem(clStr);
            break;
      }
   }
   iIdx  = pclJson->GetObjectInteger(iCurParameter, pcKeyIsValue, 0);
   pclUi->Gen_ComboParm->setCurrentIndex(iIdx);
}

//
//  Function:  gen_BuildParameterList
//  Purpose:   Read all parameters from the JSON document
//
//  Parms:     Json ptr
//  Returns:
//
void CTabGeneric::gen_BuildParameterList(CPiJson *pclJson)
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   if(iNumParameters > 0)
   {
      //
      // Delete List, if any
      //`Disable sorting
      //
      if(pclUi->Gen_CheckListParms) 
      {
         pclUi->Gen_CheckListParms->clear();
         pclUi->Gen_CheckListParms->setSortingEnabled(false);
      }
      for(int i=0; i<iNumParameters; i++)
      {
         clStr = pclJson->GetObjectName(i);
         if( clStr.length() )
         {
            //
            // Store this parameter into the QListWidget
            //
            pclLwi = new QListWidgetItem(clStr, pclUi->Gen_CheckListParms);
            //
            // Retrieve default checkbox value
            //
            if( pclJson->GetObjectInteger(i, pcKeyIsSelected) )
            {
               pclLwi->setCheckState(Qt::Checked);
            }
            else
            {
               pclLwi->setCheckState(Qt::Unchecked);
            }
            pclLwi->setSelected(false);
         }
      }
      pclUi->Gen_CheckListParms->setCurrentRow(iCurParameter);
   }
}

//
//  Function:  gen_BuildValueFields
//  Purpose:   Build the correct fields: 
//                QComboBox
//                QLineEdit1
//                   ...
//                QLineEdit4
//
//  Parms:     Json ptr
//  Returns:   
//
void CTabGeneric::gen_BuildValueFields(CPiJson *pclJson)
{
   int      iNumParms;
   PTYPE    tType;

   //
   // Get number of value-fields of this parameter
   //
   iNumParms = pclJson->GetObjectInteger(iCurParameter, pcKeyIsNumparms);
   if(iNumParms)
   {
      //
      // Get the type of value-fields, enable/disable the appropriate fields
      //
      tType = pclLib->LIB_GetRootType(pclJson, iCurParameter);
      switch(tType)
      {
         default:
            gen_EnableValueFields(iNumParms);
            pclLib->MSG_AddText(pclUi->Gen_Messages, "gen_BuildValueFields():Unknown parameter type");
            break;

         case PTYPE_STRING:
         case PTYPE_INTEGER:
            gen_FillValueFields(pclJson, iNumParms);
            gen_EnableValueFields(iNumParms);
            pclUi->Gen_DescribeParm->setPlainText(( pclJson->GetObjectString(iCurParameter, pcKeyIsDescription) ));
            break;

         case PTYPE_BOOL:
         case PTYPE_ENUM:
            gen_BuildFlatComboList(pclJson);
            gen_EnableValueFields(0);
            pclUi->Gen_DescribeParm->setPlainText(( pclJson->GetObjectString(iCurParameter, pcKeyIsDescription) ));
            break;

         case PTYPE_OBJECT:
            gen_BuildCompoundComboList(pclJson);
            gen_EnableValueFields(0);
            pclUi->Gen_DescribeParm->setPlainText(( pclJson->GetObjectString(iCurParameter, pcKeyIsDescription) ));
            break;
      }
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "gen_BuildValueFields():ZERO parameters");
   }
}

//
//  Function:  gen_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabGeneric::gen_ConnectToServer(HCNX tReq)
{
   gen_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  gen_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabGeneric::gen_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   gen_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Gen_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  gen_FillValueFields
//  Purpose:   Fill the value fields
//
//  Parms:     Json ptr, Number of value fields
//  Returns:   
//
void CTabGeneric::gen_FillValueFields(CPiJson *pclJson, int iNum)
{
   int         iIdx, iVal;
   JTYPE       tType;
   QString     clVal;

   //
   // Retrieve the value.members
   //
   for(iIdx=0; iIdx<iNum; iIdx++)
   {
      tType = pclJson->GetObjectType(iCurParameter, pcKeyIsValue, (const char *)NULL, iIdx);
      switch(tType)
      {
         default:
            break;

         case JTYPE_INTEGER:
            iVal = pclJson->GetValueInteger();
            clVal.sprintf("%d", iVal);
            break;

         case JTYPE_BOOL:
            if(pclJson->GetValueBool() == true) clVal = "true";
            else                                clVal = "false";
            break;

         case JTYPE_STRING:
            clVal = pclJson->GetValueString();
            break;
      }
      //
      // Store this value at the right place
      //
      switch(iIdx)
      {
         case 0:
            pclUi->Gen_EditParm1->setText(clVal);
            break;

         case 1:
            pclUi->Gen_EditParm2->setText(clVal);
            break;

         case 2:
            pclUi->Gen_EditParm3->setText(clVal);
            break;

         case 3:
            pclUi->Gen_EditParm4->setText(clVal);
            break;

         default:
            break;
      }
   }
}

//
//  Function:  gen_EnableValueFields
//  Purpose:   Show the correct fields
//
//  Parms:     Number of value fields
//  Returns:   
//
void CTabGeneric::gen_EnableValueFields(int iNum)
{
   switch(iNum)
   {
      default:
      case -1:
         pclUi->Gen_ComboParm->setVisible(false);
         pclUi->Gen_EditParm1->setVisible(false);
         pclUi->Gen_EditParm2->setVisible(false);
         pclUi->Gen_EditParm3->setVisible(false);
         pclUi->Gen_EditParm4->setVisible(false);
         break;

      case 0:
         pclUi->Gen_ComboParm->setVisible(true);
         pclUi->Gen_EditParm1->setVisible(false);
         pclUi->Gen_EditParm2->setVisible(false);
         pclUi->Gen_EditParm3->setVisible(false);
         pclUi->Gen_EditParm4->setVisible(false);
         break;

      case 1:
         pclUi->Gen_ComboParm->setVisible(false);
         pclUi->Gen_EditParm1->setVisible(true);
         pclUi->Gen_EditParm2->setVisible(false);
         pclUi->Gen_EditParm3->setVisible(false);
         pclUi->Gen_EditParm4->setVisible(false);
         break;

      case 2:
         pclUi->Gen_ComboParm->setVisible(false);
         pclUi->Gen_EditParm1->setVisible(true);
         pclUi->Gen_EditParm2->setVisible(true);
         pclUi->Gen_EditParm3->setVisible(false);
         pclUi->Gen_EditParm4->setVisible(false);
         break;

      case 3:
         pclUi->Gen_ComboParm->setVisible(false);
         pclUi->Gen_EditParm1->setVisible(true);
         pclUi->Gen_EditParm2->setVisible(true);
         pclUi->Gen_EditParm3->setVisible(true);
         pclUi->Gen_EditParm4->setVisible(false);
         break;

      case 4:
         pclUi->Gen_ComboParm->setVisible(false);
         pclUi->Gen_EditParm1->setVisible(true);
         pclUi->Gen_EditParm2->setVisible(true);
         pclUi->Gen_EditParm3->setVisible(true);
         pclUi->Gen_EditParm4->setVisible(true);
         break;
   }
}

//
//  Function:  gen_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabGeneric::gen_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq= tReq;

   iCnxTimeout = 0;
   pclTimer->start(HTTP_GEN_TIMEOUT);
   gen_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
   pclLib->MSG_AddText(pclUi->Gen_Messages, "gen_SetupConnection():Ip=", clIp);
}

//
//  Function:  gen_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     
//  Returns:
//
void CTabGeneric::gen_TerminateConnection(STCNX tStatus)
{
   QString clStr;

   switch(tStatus)
   {
      case STCNX_DISCONNECTED:
         clStr = pclRpiNet->TerminateHttpRequest();
         pclLib->MSG_AddText(pclUi->Gen_Messages, "gen_TerminateConnection():Disconnected=", clStr);
         break;

      default:
         break;
   }
   gen_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  gen_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabGeneric::gen_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Gen_Status->setStyleSheet("color:gray; background-color: white");
         pclUi->Gen_Status->setText(pcConnecting);
         break;

      case STCNX_CONNECTED:
         pclUi->Gen_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Gen_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Gen_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Gen_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Gen_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Gen_Status->setText(pcConnectionError);
         break;
   }
}

//
//  Function:  gen_UpdateCnxSettings
//  Purpose:   Update the connection settings on-screen
//
//  Parms:     
//  Returns:
//
void CTabGeneric::gen_UpdateCnxSettings()
{
   int iIdx = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   //
   // Fill dialog fields
   //
   pclUi->Gen_IpAddress->setText(pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx));
   pclUi->Gen_IpPort->setText(pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx));
}


/* ======   Private Functions separator ===========================================
void __Callback_Methods________(){}
==============================================================================*/

//
//  Function:  gen_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabGeneric::gen_HandleHttpCallback(CPiJson *pclJson)
{
   pclJson = pclJson;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      default:
      case HCNX_CAM_PARMS:
         gen_TerminateConnection(STCNX_CONNECTED);
         break;
   }
}


/* ======   Local   Functions separator ===========================================
void __COMMENTED_OUT_______________(){}
==================================X==============================================*/
#ifdef COMMENT

//
//  Function:  gen_DisplayKeypairs
//  Purpose:   Show all keypairs
//
//  Parms:     Json ptr
//  Returns:
//
void CTabGeneric::gen_DisplayKeypairs(CPiJson *pclJson)
{
   QString  clStr;
   JTYPE    tJsonType;
   int      iNr;

   iNr = pclJson->ParseLocalParameters();
   if(iNr > 0)
   {
      for(int i=0; i<iNr; i++)
      {
         clStr = pclJson->GetObjectName(i);
         if(clStr.length())   pclLib->MSG_AddText(pclUi->Gen_Messages, clStr);
         else                 pclLib->MSG_AddText(pclUi->Gen_Messages, "Empty key");
         //
         tJsonType = pclJson->GetObjectType(i, NULL, NULL);
         switch(tJsonType)
         {
            case JTYPE_ARRAY:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":Array\n");
                  break;

            case JTYPE_BOOL:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":Bool\n");
                  break;

            case JTYPE_INTEGER:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":Double\n");
                  break;

            case JTYPE_NULL:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":Null\n");
                  break;

            case JTYPE_OBJECT:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":Object\n");
                  break;

            case JTYPE_STRING:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":String\n");
                  break;

            default:
            case JTYPE_UNDEFINED:
                  pclLib->MSG_AddText(pclUi->Gen_Messages, ":Undefined\n");
                  break;
         }
      }
   }
}

#endif //COMMENT
