/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabGeneric.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Generic tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28 Jul 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABGENERIC_H
#define TABGENERIC_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"

class CTabGeneric : public QDialog
{
    Q_OBJECT

public:
   CTabGeneric(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabGeneric();

signals:
   void           NfyParmChanges();

private slots:
   void           on_Gen_HttpProgress(qint64);
   void           on_Gen_HttpReady(qint64);
   void           on_Gen_HttpDisconnected(bool);
   void           on_Gen_Timeout();
   void           on_Gen_TabChanged(int);
   //
   void           on_Gen_ButtonConnect_clicked();
   void           on_Gen_ButtonJsonSave_clicked();
   void           on_Gen_ButtonJsonView_clicked();
   void           on_Gen_ButtonJsonBrowse_clicked();
   void           on_Gen_ComboParm_activated(int);
   void           on_Gen_CheckList_changed(QListWidgetItem *);
   void           on_Gen_CheckList_checked(QListWidgetItem *);
   void           on_Gen_CheckList_clicked(int);
   void           on_Gen_ButtonOkCancel_clicked(QAbstractButton *);
   void           on_Gen_ValueFieldsFinished();
   void           on_Gen_ServerAutoConnect();

public:

private:
   void           gen_BuildFlatComboList        (CPiJson *);
   void           gen_BuildCompoundComboList    (CPiJson *);
   void           gen_BuildParameterList        (CPiJson *);
   void           gen_BuildValueFields          (CPiJson *);
   void           gen_ConnectToServer           (HCNX);
   void           gen_ConnectToServer           (HCNX, QString);
   void           gen_EnableValueFields         (int);
   void           gen_FillValueFields           (CPiJson *, int);
   void           gen_HandleHttpCallback        (CPiJson *);
   void           gen_SetupConnection           (HCNX);
   void           gen_TerminateConnection       (STCNX);
   void           gen_UpdateCnxStatus           (STCNX);
   void           gen_UpdateCnxSettings         ();
   //
   // Private variables
   //
   int            iCnxTimeout;
   int            iNumParameters;
   int            iCurParameter;
   //
   Ui::Dialog    *pclUi;
   //
   CPatrn        *pclLib;
   QTimer        *pclTimer;
   CPinet        *pclRpiNet;
   CPiJson       *pclParJson;
   CPiJson       *pclValJson;
   CNvmStorage   *pclNvm;
   //
   // void        gen_DisplayKeypairs           ();
};

#endif // TABGENERIC_H
