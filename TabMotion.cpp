/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabMotion.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Motion tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QTimer>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsValue;
extern const char *pcKeyIsLastFile;
extern const char *pcKeyIsLastFileSize;
//
extern const char *pcJsonParseError;
extern const char *pcConnectionError;
extern const char *pcConnecting;
extern const char *pcMsgParseError;

const char *pcKeyIsMotionSeq  =     "MotionSeq";
const char *pcKeyIsMotionPix  =     "MotionPix";
const char *pcKeyIsMotionLev  =     "MotionLev";
const char *pcKeyIsMotionRed  =     "MotionRed";
const char *pcKeyIsMotionGrn  =     "MotionGrn";
const char *pcKeyIsMotionBlu  =     "MotionBlu";
const char *pcKeyIsMotionX    =     "MotionX";
const char *pcKeyIsMotionY    =     "MotionY";
const char *pcKeyIsMotionW    =     "MotionW";
const char *pcKeyIsMotionH    =     "MotionH";
const char *pcKeyIsMotionAvg1 =     "MotionAvg1";
const char *pcKeyIsMotionAvg2 =     "MotionAvg2";

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabMotion::CTabMotion(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;
   QByteArray  baCamParms;

   pclUi      = pclParent;
   pclLib     = new CPatrn; 
   pclTimer   = new QTimer(this);
   pclParJson = new CPiJson;
   pclValJson = new CPiJson;
   pclRpiNet  = new CPinet();
   pclNvm     = new CNvmStorage;
   pclView1   = new CImageViewer(pclUi->Mot_Image1);
   pclView2   = new CImageViewer(pclUi->Mot_Image2);
   //
   iMotionSequence = 0;
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //
   //       Signals                                                                    Slots
   connect( pclUi->Mot_ButtonPix1,        SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonSnapshot1_clicked())    );
   connect( pclUi->Mot_ButtonZoom1,       SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonZoom1_clicked())        );
   connect( pclUi->Mot_ButtonNorm1,       SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonNorm1_clicked())        );
   connect( pclUi->Mot_ButtonPix2,        SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonSnapshot2_clicked())    );
   connect( pclUi->Mot_ButtonZoom2,       SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonZoom2_clicked())        );
   connect( pclUi->Mot_ButtonNorm2,       SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonNorm2_clicked())        );
   connect( pclUi->Mot_ButtonMark,        SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonMark_clicked())         );
   connect( pclUi->Mot_ButtonDelta,       SIGNAL(clicked()),                           this,             SLOT(on_Mot_ButtonDelta_clicked())        );
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Mot_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Mot_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Mot_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Mot_HttpDisconnected(bool))       );
   //
   mot_HandleProgressbar(true, 0);
   //
   //  Dialog window done
   //
   pclLib->MSG_AddText(pclUi->Mot_Messages, "TAB:Motion Detect init OK", clStr);
}

//
//  Function:  CTabMotion destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabMotion::~CTabMotion()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
   delete pclNvm;
   delete pclView1;
   delete pclView2;
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Mot_ButtonSnapshot1_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//  Note:      RPi HTTP sqeuence:
//             cam.json?msnap&....&MotionSeq=0
//
void CTabMotion::on_Mot_ButtonSnapshot1_clicked()
{
   QString clStr;

   iMotionSequence = 0;
   //
   // Make snapshot, include all selected parameters
   //
   clStr  = pclLib->LIB_BuildOptionsList(pclParJson);
   clStr += "&MotionSeq=0&Ext=bmp";
   mot_ConnectToServer(HCNX_SNAP_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonNorm1_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonNorm1_clicked()
{
   QString clStr;

   iMotionSequence = 0;
   //
   // Make snapshot, include all selected parameters
   //
   clStr  = pclLib->LIB_BuildOptionsList(pclParJson);
   clStr += "&MotionSeq=0&Ext=bmp";
   mot_ConnectToServer(HCNX_NORM_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonZoom1_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonZoom1_clicked()
{
   QString clStr;

   iMotionSequence = 0;
   //
   // Make snapshot, include all selected parameters
   //
   clStr = pclLib->LIB_BuildOptionsList(pclParJson);
   clStr += "&MotionSeq=0";
   //
   // Retrieve current coords settings
   //
   clStr += "&MotionX=";
   clStr +=  pclUi->Mot_ZoomX1->text();
   clStr += "&MotionY=";
   clStr +=  pclUi->Mot_ZoomY1->text();
   clStr += "&MotionW=";
   clStr +=  pclUi->Mot_ZoomW1->text();
   clStr += "&MotionH=";
   clStr +=  pclUi->Mot_ZoomH1->text();
   //
   mot_ConnectToServer(HCNX_ZOOM_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonSnapshot2_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonSnapshot2_clicked()
{
   QString clStr;

   iMotionSequence = 1;
   //
   // Make snapshot, include all selected parameters
   //
   clStr  = pclLib->LIB_BuildOptionsList(pclParJson);
   clStr += "&MotionSeq=1&Ext=bmp";
   mot_ConnectToServer(HCNX_SNAP_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonNorm2_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonNorm2_clicked()
{
   QString clStr;

   iMotionSequence = 1;
   //
   // Make snapshot, include all selected parameters
   //
   clStr  = pclLib->LIB_BuildOptionsList(pclParJson);
   clStr += "&MotionSeq=1&Ext=bmp";
   mot_ConnectToServer(HCNX_NORM_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonZoom2_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonZoom2_clicked()
{
   QString clStr;

   iMotionSequence = 1;
   //
   // Make snapshot, include all selected parameters
   //
   clStr = pclLib->LIB_BuildOptionsList(pclParJson);
   clStr += "&MotionSeq=1";
   //
   // Retrieve current coords settings
   //
   clStr += "&MotionX=";
   clStr +=  pclUi->Mot_ZoomX2->text();
   clStr += "&MotionY=";
   clStr +=  pclUi->Mot_ZoomY2->text();
   clStr += "&MotionW=";
   clStr +=  pclUi->Mot_ZoomW2->text();
   clStr += "&MotionH=";
   clStr +=  pclUi->Mot_ZoomH2->text();
   //
   mot_ConnectToServer(HCNX_ZOOM_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonDelta_clicked
//  Purpose:   Calculate delta between pictues 1 and 2
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonDelta_clicked()
{
   QString clStr;

   //
   // Make snapshot, include all selected parameters
   //
   clStr = pclLib->LIB_BuildOptionsList(pclParJson);
   mot_ConnectToServer(HCNX_MARK_MOTION, clStr);
}

//
//  Function:  on_Mot_ButtonMark_clicked
//  Purpose:   Mark delta's picture 1 and 2
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_ButtonMark_clicked()
{
   QString clStr;

   //
   // Make snapshot, include all selected parameters
   //
   clStr = pclLib->LIB_BuildOptionsList(pclParJson);
   mot_ConnectToServer(HCNX_MARK_MOTION, clStr);
}

/* ======   Local   Functions separator ===========================================
void __Signal_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Mot_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabMotion::on_Mot_HttpProgress(qint64 llReceived)
{
   // QString clStr;
   // 
   // clStr.sprintf("on_Mot_HttpProgress(): %lld bytes received", llReceived);
   // pclLib->MSG_AddText(pclUi->Mot_Messages, clStr);
   //
   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   mot_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Mot_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabMotion::on_Mot_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Mot_Messages, clStr);
}

//
//  Function:  on_Mot_HttpDisconnected
//  Purpose:   ***** HTTP disconnected from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Data is Binary
//  Returns:
//
void CTabMotion::on_Mot_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   pclTimer->stop();
   //
   if(fBinary)
   {
      mot_HandleHttpCallback(pclValJson);
   }
   else
   {
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         mot_UpdateCnxStatus(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data, result is 
         //    pclValJson->clStrList : all root key names
         //    pclValJson->clRootObj : the root object
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)
         {
            mot_HandleHttpCallback(pclValJson);
         }
         else
         {
            mot_UpdateCnxStatus(STCNX_ERROR);
         }
      }
   }
}

//
//  Function:  on_Mot_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabMotion::on_Mot_Timeout()
{
   pclTimer->stop();
   mot_TerminateConnection(STCNX_DISCONNECTED);
}

//
//  Function:  on_Mot_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//
void CTabMotion::on_Mot_TabChanged(int iIdx)
{
   QString  clStr;

   clStr = pclUi->TabWidget->tabText(iIdx);
   clStr += "&MotionSeq=0";
   mot_ConnectToServer(HCNX_PARM_MOTION);
   pclLib->MSG_AddText(pclUi->Mot_Messages, clStr);
}

/* ======   Private Functions separator ===========================================
void __Private_Methods________(){}
==============================================================================*/

//
//  Function:  mot_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabMotion::mot_ConnectToServer(HCNX tReq)
{
   mot_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Mot_Messages, "Connect to Server...");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Mot_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  mot_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabMotion::mot_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   mot_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Mot_Messages, "Connect to Server: ", pcOpt);
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Mot_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  mot_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabMotion::mot_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Mot_ProgBar->setMaximum(1000);
      pclUi->Mot_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Mot_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Mot_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  mot_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabMotion::mot_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   mot_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Mot_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  mot_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabMotion::mot_TerminateConnection(STCNX tStatus)
{
   mot_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  mot_UpdateMotionParms
//  Purpose:   Update the connection status
//
//  Parms:     Sequence 0 or 1
//  Returns:
//  Note:      Parameters :
//
//             PiCam --> RPi:
//             ================================================================
//             MotionSeq   :  Picture 1st or 2nd (0..1)
//             MotionLev   :  Pixel threshold level for on/off (0..255)
//             MotionPix   :  Number of pixels for average calculation
//             MotionX1    :  Zoom X
//             MotionY1    :       Y
//             MotionW1    :       W
//             MotionH1    :       H
//             MotionX2    :  Zoom X
//             MotionY2    :       Y
//             MotionW2    :       W
//             MotionH2    :       H
//
//             RPi --> PiCam:
//             ================================================================
//             MotionSeq   :  Picture 1st or 2nd (0..1)
//             MotionAvg1  :  Average pixel value after normalization
//             MotionAvg2  :  Average pixel value after normalization
//
void CTabMotion::mot_UpdateMotionParms(int iSeq)
{
   //
   // Update all widget fields with info from the JSON reply
   //
   switch(iSeq)
   {
      case 0:
         pclUi->Mot_ZoomX1->setText(pclValJson->GetObjectString(pcKeyIsMotionX));
         pclUi->Mot_ZoomY1->setText(pclValJson->GetObjectString(pcKeyIsMotionY));
         pclUi->Mot_ZoomH1->setText(pclValJson->GetObjectString(pcKeyIsMotionH));
         pclUi->Mot_ZoomW1->setText(pclValJson->GetObjectString(pcKeyIsMotionW));
         //
         pclUi->Mot_Average1->setText(pclValJson->GetObjectString(pcKeyIsMotionAvg1));
         pclUi->Mot_Threshold1->setText(pclValJson->GetObjectString(pcKeyIsMotionLev));
         //
         pclUi->Mot_Red->setText(pclValJson->GetObjectString(pcKeyIsMotionRed));
         pclUi->Mot_Grn->setText(pclValJson->GetObjectString(pcKeyIsMotionGrn));
         pclUi->Mot_Blu->setText(pclValJson->GetObjectString(pcKeyIsMotionBlu));
         break;

      case 1:
         pclUi->Mot_ZoomX2->setText(pclValJson->GetObjectString(pcKeyIsMotionX));
         pclUi->Mot_ZoomY2->setText(pclValJson->GetObjectString(pcKeyIsMotionY));
         pclUi->Mot_ZoomH2->setText(pclValJson->GetObjectString(pcKeyIsMotionH));
         pclUi->Mot_ZoomW2->setText(pclValJson->GetObjectString(pcKeyIsMotionW));
         //
         pclUi->Mot_Average2->setText(pclValJson->GetObjectString(pcKeyIsMotionAvg2));
         pclUi->Mot_Threshold2->setText(pclValJson->GetObjectString(pcKeyIsMotionLev));
         //
         pclUi->Mot_Red->setText(pclValJson->GetObjectString(pcKeyIsMotionRed));
         pclUi->Mot_Grn->setText(pclValJson->GetObjectString(pcKeyIsMotionGrn));
         pclUi->Mot_Blu->setText(pclValJson->GetObjectString(pcKeyIsMotionBlu));
         break;

      default:
         break;
   }
}

//
//  Function:  mot_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabMotion::mot_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Mot_Status->setText(pcConnecting);
         pclUi->Mot_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Mot_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Mot_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Mot_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Mot_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Mot_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Mot_Status->setText(pcConnectionError);
         break;
   }
}

/* ======   Private Functions separator ===========================================
void __Callback_Methods________(){}
==============================================================================*/

//
//  Function:  mot_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabMotion::mot_HandleHttpCallback(CPiJson *pclJson)
{
   pclJson = pclJson;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      default:
         mot_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_CAM_PARMS:
      case HCNX_PARM_MOTION:
         iMotionSequence = pclValJson->GetObjectInteger(pcKeyIsMotionSeq);
         mot_UpdateMotionParms(iMotionSequence);
         mot_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_SHOW_MOTION:
         //
         // Last file downloaded
         //
         mot_TerminateConnection(STCNX_CONNECTED);
         mot_CallbackDownloadPixture();
         break;

      case HCNX_ZOOM_MOTION:
      case HCNX_NORM_MOTION:
      case HCNX_SNAP_MOTION:
         iMotionSequence = pclValJson->GetObjectInteger(pcKeyIsMotionSeq);
         //
         // Last action completed
         //
         mot_UpdateMotionParms(iMotionSequence);
         mot_TerminateConnection(STCNX_CONNECTED);
         mot_CallbackSnapshotPixture();
         break;
   }
}

//
//  Function:  mot_CallbackSnapshotPixture1
//  Purpose:   Http server has responded: download the last snapshot image
//
//  Parms:     
//  Returns:
//
void CTabMotion::mot_CallbackSnapshotPixture()
{
   int      iSize=0;
   QString  clStr;

   clStr = pclValJson->GetObjectString(pcKeyIsLastFile);
   iSize = pclValJson->GetObjectInteger(pcKeyIsLastFileSize);

   if(iSize)
   {
      mot_HandleProgressbar(true, (qint64)iSize); 
      mot_ConnectToServer(HCNX_SHOW_MOTION, clStr);
   }
}

//
//  Function:  mot_CallbackDownloadPixture
//  Purpose:   Http server has responded: display the returned image
//
//  Parms:     
//  Returns:
//
void CTabMotion::mot_CallbackDownloadPixture()
{
   QString clStr;

   switch(iMotionSequence)
   {
      case 0:
      default:
         pclView1->DisplayImageFromData(pclRpiNet->baHttpResp);
         pclView1->GetImageInfo(&clStr);
         pclLib->MSG_AddText(pclUi->Mot_Messages, "Snapshot image 1: ", clStr);
         break;

      case 1:
         pclView2->DisplayImageFromData(pclRpiNet->baHttpResp);
         pclView2->GetImageInfo(&clStr);
         pclLib->MSG_AddText(pclUi->Mot_Messages, "Snapshot image 2: ", clStr);
         break;
   }
}

