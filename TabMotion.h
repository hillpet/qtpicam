/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabMotion.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Motion Detect tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABMOTION_H
#define TABMOTION_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"
#include "ImageViewer.h"


class CTabMotion : public QDialog
{
    Q_OBJECT

public:
   CTabMotion(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabMotion();

signals:

private slots:
   void              on_Mot_HttpReady              (qint64);
   void              on_Mot_HttpProgress           (qint64);
   void              on_Mot_HttpDisconnected       (bool);
   void              on_Mot_Timeout                ();
   void              on_Mot_TabChanged             (int);
   void              on_Mot_ButtonSnapshot1_clicked();
   void              on_Mot_ButtonZoom1_clicked    ();
   void              on_Mot_ButtonNorm1_clicked    ();
   void              on_Mot_ButtonSnapshot2_clicked();
   void              on_Mot_ButtonZoom2_clicked    ();
   void              on_Mot_ButtonNorm2_clicked    ();
   void              on_Mot_ButtonMark_clicked     ();
   void              on_Mot_ButtonDelta_clicked    ();
   //
public:
   CPiJson          *pclParJson;

private:
   void              mot_ConnectToServer           (HCNX);
   void              mot_ConnectToServer           (HCNX, QString);
   void              mot_HandleHttpCallback        (CPiJson *);
   qint64            mot_HandleProgressbar         (bool, qint64);
   void              mot_SetupConnection           (HCNX);
   void              mot_TerminateConnection       (STCNX);
   void              mot_UpdateCnxStatus           (STCNX);
   void              mot_UpdateMotionParms         (int);
   //
   void              mot_CallbackSnapshotPixture   ();
   void              mot_CallbackDownloadPixture   ();
   //
   Ui::Dialog       *pclUi;
   //
   int               iMotionSequence;
   qint64            llToDownload;
   qint64            llDownloaded;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclValJson;
   CNvmStorage      *pclNvm;
   CImageViewer     *pclView1;
   CImageViewer     *pclView2;
};

#endif // TABMOTION_H
