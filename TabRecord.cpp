/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabRecord.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Record tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *             CR008:   Handle the duration correctly
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QTimer>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
extern const char *pcJsonParseError;
extern const char *pcConnecting;
extern const char *pcConnectionError;
//
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsValue;
//
extern const char *pcKeyIsRecording;

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabRecord::CTabRecord(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;
   QByteArray  baCamParms;

   pclUi      = pclParent;
   pclLib     = new CPatrn; 
   pclTimer   = new QTimer(this);
   pclParJson = new CPiJson;
   pclValJson = new CPiJson;
   pclRpiNet  = new CPinet();
   pclNvm     = new CNvmStorage;
   pclView    = new CImageViewer(pclUi->Rec_Image);
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //
   //       Signals                                                                    Slots
   connect( pclUi->Rec_ButtonRecord,      SIGNAL(clicked()),                           this,             SLOT(on_Rec_ButtonRecord_clicked())       );
   //
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Rec_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Rec_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Rec_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Rec_HttpDisconnected(bool))       );
   //
   //  Dialog window done
   //
   iDuration  = 0;
   iRecording = 0;
   pclLib->MSG_AddText(pclUi->Rec_Messages, "TAB:Recording init OK", clStr);
}

//
//  Function:  CTabRecord destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabRecord::~CTabRecord()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Rec_ButtonRecord_clicked
//  Purpose:   Start/Stop recording
//
//  Parms:     
//  Returns:
//
void CTabRecord::on_Rec_ButtonRecord_clicked()
{
   //
   // Start/Stop recording, include all selected parameters
   //
   if(iRecording > 0)
   {
      //
      // Recording is ongoing: stop it
      //
      //pclLib->MSG_AddText(pclUi->Rec_Messages, "BTN:Stop");
      rec_ConnectToServer(HCNX_STOP);
   }
   else if(iRecording == 0)
   {
      //
      // No recording is ongoing: start one
      //
      QString clOpt = pclLib->LIB_BuildOptionsList(pclParJson);
      //
      clOpt += "&Duration=";
      clOpt += rec_GetDuration();
      //pclLib->MSG_AddText(pclUi->Rec_Messages, "BTN:Start", clOpt);
      rec_ConnectToServer(HCNX_RECORD, clOpt);
   }
   else
   {
      //
      // Recording has errors: retry to connect
      //
      pclLib->MSG_AddText(pclUi->Rec_Messages, "BTN:Error");
      rec_ConnectToServer(HCNX_REC_PARMS);
   }
}

//
//  Function:  on_Rec_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabRecord::on_Rec_HttpProgress(qint64 llReceived)
{
   // QString clStr;
   // 
   // clStr.sprintf("on_Rec_HttpProgress(): %lld bytes received", llReceived);
   // pclLib->MSG_AddText(pclUi->Rec_Messages, clStr);
   //
   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   rec_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Rec_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabRecord::on_Rec_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Rec_Messages, clStr);
}

//
//  Function:  on_Rec_HttpDisconnected
//  Purpose:   ***** HTTP reply from the Raspberry Pi has been received ****
//             Parse the JSON document received from the server
//
//  Parms:     
//  Returns:
//
void CTabRecord::on_Rec_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   if(fBinary)
   {
      rec_HandleHttpCallback(pclValJson);
   }
   else
   {
      //
      // These commands return all settings, so we need to reparse
      // the new JSON vars
      //
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         rec_UpdateCnxStatus(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data, result is 
         //    pclValJson->clStrList : all root key names
         //    pclValJson->clRootObj : the root object
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)   
         {
            switch( rec_CheckRecordingStatus() )
            {
               case 0:
                  // Not recording
                  pclUi->Rec_ButtonRecord->setText("START");
                  pclUi->Rec_ButtonRecord->setStyleSheet("color:white; background-color: green");
                  break;

               case -1:
                  // Error recording
                  pclUi->Rec_ButtonRecord->setText("ERROR");
                  pclUi->Rec_ButtonRecord->setStyleSheet("color:red; background-color: yellow");
                  break;

               default:
                  // Recording
                  pclUi->Rec_ButtonRecord->setText("STOP");
                  pclUi->Rec_ButtonRecord->setStyleSheet("color:white; background-color: red");
                  break;
            }
            rec_HandleHttpCallback(pclValJson);
         }
         else
         {
            iRecording = -1;
            rec_UpdateCnxStatus(STCNX_ERROR);
         }
      }
   }
}

//
//  Function:  on_Rec_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabRecord::on_Rec_Timeout()
{
   if(iRecording >= 0)
   {
      //
      // The RPi seems to be doing OKee
      //
      iRecording++;
      rec_UpdateRecordingStatus();
   }
   else
   {
      //
      // Some kind of comm error
      //
      pclTimer->stop();
      rec_TerminateConnection(STCNX_DISCONNECTED);
      pclLib->MSG_AddText(pclUi->Rec_Messages, "TO:Stopped");
   }
}

//
//  Function:  on_Rec_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//
void CTabRecord::on_Rec_TabChanged(int iIdx)
{
   QString  clStr;

   clStr = pclUi->TabWidget->tabText(iIdx);
   pclLib->MSG_AddText(pclUi->Rec_Messages, clStr);
   rec_ConnectToServer(HCNX_REC_PARMS);
}

/* ======   Private Functions separator ===========================================
void _____Private_Methods_____(){}
==============================================================================*/

//
//  Function:  rec_CheckRecordingStatus
//  Purpose:   Check if we have an ongoing recording
//
//  Parms:     
//  Returns:   iRecording
//
//  Note:      Updates local iRecording!
//
int CTabRecord::rec_CheckRecordingStatus()
{
   int     iTime;
   QString clStr = pclValJson->GetObjectString(pcKeyIsRecording);

   if( !rec_ConvertHms(&clStr, &iTime) )
   {
      // We are NOT recording
      iRecording = 0;
   }
   else
   {
      // We ARE recording
      iRecording = iTime;
   }
   return(iRecording);
}

//
//  Function:  rec_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabRecord::rec_ConnectToServer(HCNX tReq)
{
   rec_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Rec_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Rec_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  ec_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabRecord::rec_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   rec_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Rec_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Rec_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  rec_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabRecord::rec_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Rec_ProgBar->setMaximum(1000);
      pclUi->Rec_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Rec_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Rec_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  rec_ConvertHms
//  Purpose:   Convert time format to int
//
//  Parms:     Time ptr, result ptr
//  Returns:   true if OKee
//
//  Note:      Time format hh:mm:ss
//             result =
//               -1 Illegal time
//                0 Time = --:--:--
//             else Time in Secs
//
bool CTabRecord::rec_ConvertHms(QString *pclStr, int *piTime)
{
   bool  fCc=true;
   int   iTime=0;

   if(pclStr->contains("--:--:--"))
   {
   fCc = false;
   }
   else
   {
      QVariant clVar(*pclStr);
      if( clVar.canConvert(QVariant::Time) )
      {
         QTime clTime = clVar.toTime();
         if(clTime.isValid())
         {
            iTime = (clTime.hour() * 3600) + (clTime.minute() * 60) + clTime.second() + 1;
         }
         else
         {
            iTime = pclStr->toInt(&fCc) + 1;
            if(!fCc) iTime = -1;
         }
      }
      else
      {
         iTime = -1;
      }
   }
   *piTime = iTime;
   return(fCc);
}

//
//  Function:  rec_GetDuration
//  Purpose:   Get the specified recording duration (or 0 if not specified)
//
//  Parms:     
//  Returns:   Duration in mSecs
//
QString CTabRecord::rec_GetDuration()
{
   int      iTime=0;
   QString  clStr=pclUi->Rec_Time->text();

   if( rec_ConvertHms(&clStr, &iTime) )
   {
      clStr.sprintf("%d", iTime * 1000);
   }
   else clStr = "0";
   //
   iDuration = iTime;
   return(clStr);
}

//
//  Function:  rec_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabRecord::rec_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   rec_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Rec_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  rec_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabRecord::rec_TerminateConnection(STCNX tStatus)
{
   rec_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  rec_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabRecord::rec_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Rec_Status->setText(pcConnecting);
         pclUi->Rec_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Rec_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Rec_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Rec_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Rec_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Rec_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Rec_Status->setText(pcConnectionError);
         break;
   }
}

//
//  Function:  rec_UpdateRecordingStatus
//  Purpose:   Update the recording status
//
//  Parms:     
//  Returns:
//
void CTabRecord::rec_UpdateRecordingStatus()
{

   //
   // Show recording time
   //
   if(iRecording < 0)
   {
      //
      // Error in retrieving recording time: erase
      //
      pclUi->Rec_Duration->setText(" Error !");
   }
   else if (iRecording == 0)
   {
      //
      // "Recording" JSON element shows RPi is NOT recording
      //
      pclUi->Rec_Duration->setText("--:--:--");
      pclUi->Rec_Time->clear();
   }
   else
   {
      //
      // "Recording" JSON element shows RPi recording 
      //
      int   iTime=0;
      QTime clRecTime(0, 0, 0);
      QTime clDurTime(0, 0, 0);
      //
      clRecTime = clRecTime.addSecs(iRecording);
      pclUi->Rec_Duration->setText( clRecTime.toString(Qt::TextDate) );
      //
      // Update duration
      //
      if(iDuration)
      {
         if(--iDuration > 0) 
         {
            // Still running 
            iTime = iDuration;
         }
         else if(iDuration == 0)
         {
            // Recording has stopped at the server side by now: wait till this process is signalled to us 
            iDuration--;
         }
         else if(iDuration < -3)
         {
            //
            // Server should be completed now: retrieve settings
            //
            iDuration = 0;
            rec_ConnectToServer(HCNX_REC_PARMS);
         }
         //
         // Display recording duration
         //
         clDurTime = clDurTime.addSecs(iTime);
         pclUi->Rec_Time->setText( clDurTime.toString(Qt::TextDate) );
      }
      else
      {
         pclUi->Rec_Time->clear();
      }
   }
}
/* ======   Private Functions separator ===========================================
void _____Callback_Methods_____(){}
==============================================================================*/

//
//  Function:  rec_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabRecord::rec_HandleHttpCallback(CPiJson *pclJson)
{
   pclJson = pclJson;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      default:
      case HCNX_REC_PARMS:
         if(iRecording > 0) pclTimer->start(1000);
         else               pclTimer->stop();
         //
         rec_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_RECORD:
         //
         // Recording started: show seconds increase
         //
         pclTimer->start(1000);
         rec_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_STOP:
         //
         // Recording stopped
         //
         pclTimer->stop();
         rec_TerminateConnection(STCNX_CONNECTED);
         break;
   }
   rec_UpdateRecordingStatus();
}

