/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabRecord.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Record tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABRECORD_H
#define TABRECORD_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"
#include "ImageViewer.h"


class CTabRecord : public QDialog
{
    Q_OBJECT

public:
   CTabRecord(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabRecord();

signals:

private slots:
   void              on_Rec_ButtonRecord_clicked   ();
   void              on_Rec_HttpReady              (qint64);
   void              on_Rec_HttpProgress           (qint64);
   void              on_Rec_HttpDisconnected       (bool);
   void              on_Rec_Timeout                ();
   void              on_Rec_TabChanged             (int);
   //
public:
   CPiJson       *pclParJson;

private:
   int               rec_CheckRecordingStatus      ();
   void              rec_ConnectToServer           (HCNX);
   void              rec_ConnectToServer           (HCNX, QString);
   bool              rec_ConvertHms                (QString *, int *);
   QString           rec_GetDuration               ();
   qint64            rec_HandleProgressbar         (bool, qint64);
   void              rec_SetupConnection           (HCNX);
   void              rec_TerminateConnection       (STCNX);
   void              rec_HandleHttpCallback        (CPiJson *);
   void              rec_UpdateCnxStatus           (STCNX);
   void              rec_UpdateRecordingStatus     ();
   //
   Ui::Dialog       *pclUi;
   //
   int               iDuration;
   int               iRecording;
   qint64            llToDownload;
   qint64            llDownloaded;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclValJson;
   //
   CNvmStorage      *pclNvm;
   CImageViewer     *pclView;
};

#endif // TABRECORD_H
