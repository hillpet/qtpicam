/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabSnapshot.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Snapshot tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QTimer>
#include <PatrnFiles.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiJson.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsValue;
extern const char *pcKeyIsSwVersion;
extern const char *pcKeyIsLastFile;
extern const char *pcKeyIsLastFileSize;
//
extern const char *pcJsonParseError;
extern const char *pcConnectionError;
extern const char *pcConnecting;
extern const char *pcMsgParseError;

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabSnapshot::CTabSnapshot(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;
   QByteArray  baCamParms;

   pclUi      = pclParent;
   pclLib     = new CPatrn; 
   pclTimer   = new QTimer(this);
   pclParJson = new CPiJson;
   pclValJson = new CPiJson;
   pclRpiNet  = new CPinet();
   pclNvm     = new CNvmStorage;
   pclView    = new CImageViewer(pclUi->Snp_Image);
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //
   //       Signals                                                                    Slots
   connect( pclUi->Snp_ButtonSnapshot,    SIGNAL(clicked()),                           this,             SLOT(on_Snp_ButtonSnapshot_clicked())     );
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Snp_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Snp_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Snp_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Snp_HttpDisconnected(bool))       );
   //
   snp_HandleProgressbar(true, 0);
   //
   //  Dialog window done
   //
   pclLib->MSG_AddText(pclUi->Snp_Messages, "TAB:Snapshot init OK", clStr);
}

//
//  Function:  CTabSnapshot destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabSnapshot::~CTabSnapshot()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
   delete pclNvm;
   delete pclView;
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Snp_ButtonSnapshot_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabSnapshot::on_Snp_ButtonSnapshot_clicked()
{
   QString clStr;

   //
   // Make snapshot, include all selected parameters
   //
   clStr = pclLib->LIB_BuildOptionsList(pclParJson);
   snp_ConnectToServer(HCNX_SNAPSHOT, clStr);
}

/* ======   Local   Functions separator ===========================================
void __Signal_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Snp_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabSnapshot::on_Snp_HttpProgress(qint64 llReceived)
{
   // QString clStr;
   // 
   // clStr.sprintf("on_Snp_HttpProgress(): %lld bytes received", llReceived);
   // pclLib->MSG_AddText(pclUi->Snp_Messages, clStr);
   //
   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   snp_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Snp_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabSnapshot::on_Snp_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Snp_Messages, clStr);
}

//
//  Function:  on_Snp_HttpDisconnected
//  Purpose:   ***** HTTP disconnected from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Data is Binary
//  Returns:
//
void CTabSnapshot::on_Snp_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   pclTimer->stop();
   //
   if(fBinary)
   {
      snp_HandleHttpCallback(pclValJson);
   }
   else
   {
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         snp_UpdateCnxStatus(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data, result is 
         //    pclValJson->clStrList : all root key names
         //    pclValJson->clRootObj : the root object
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)
         {
            snp_HandleHttpCallback(pclValJson);
         }
         else
         {
            snp_UpdateCnxStatus(STCNX_ERROR);
         }
      }
   }
}

//
//  Function:  on_Snp_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabSnapshot::on_Snp_Timeout()
{
   pclTimer->stop();
   snp_TerminateConnection(STCNX_DISCONNECTED);
}

//
//  Function:  on_Snp_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//
void CTabSnapshot::on_Snp_TabChanged(int iIdx)
{
   QString  clStr;

   clStr = pclUi->TabWidget->tabText(iIdx);
   pclLib->MSG_AddText(pclUi->Snp_Messages, clStr);
   snp_ConnectToServer(HCNX_CAM_PARMS);
}

/* ======   Private Functions separator ===========================================
void __Private_Methods________(){}
==============================================================================*/

//
//  Function:  snp_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabSnapshot::snp_ConnectToServer(HCNX tReq)
{
   snp_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Snp_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Snp_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  snp_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabSnapshot::snp_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   snp_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Snp_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Snp_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  snp_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabSnapshot::snp_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Snp_ProgBar->setMaximum(1000);
      pclUi->Snp_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Snp_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Snp_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  snp_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabSnapshot::snp_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   snp_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Snp_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  snp_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabSnapshot::snp_TerminateConnection(STCNX tStatus)
{
   snp_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  snp_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabSnapshot::snp_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Snp_Status->setText(pcConnecting);
         pclUi->Snp_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Snp_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Snp_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Snp_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Snp_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Snp_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Snp_Status->setText(pcConnectionError);
         break;
   }
}

/* ======   Private Functions separator ===========================================
void __Callback_Methods________(){}
==============================================================================*/

//
//  Function:  snp_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabSnapshot::snp_HandleHttpCallback(CPiJson *pclJson)
{
   pclJson = pclJson;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      default:
         snp_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_CAM_PARMS:
         snp_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_DLD_FILE:
         //
         // Snapshot has been taken and downloaded
         //
         snp_TerminateConnection(STCNX_CONNECTED);
         snp_CallbackDownloadPixture();
         break;

      case HCNX_SNAPSHOT:
         //
         // Snapshot was taken
         //
         snp_TerminateConnection(STCNX_CONNECTED);
         snp_CallbackSnapshotPixture();
         break;
   }
}

//
//  Function:  snp_CallbackSnapshotPixture
//  Purpose:   Http server has responded: download the snapshot image
//
//  Parms:     
//  Returns:
//
void CTabSnapshot::snp_CallbackSnapshotPixture()
{
   int      iSize=0;
   QString  clStr;

   clStr = pclValJson->GetObjectString(pcKeyIsLastFile);
   iSize = pclValJson->GetObjectInteger(pcKeyIsLastFileSize);

   if(iSize)
   {
      snp_HandleProgressbar(true, (qint64)iSize); 
      snp_ConnectToServer(HCNX_DLD_FILE, clStr);
   }
}

//
//  Function:  snp_CallbackDownloadPixture
//  Purpose:   Http server has responded: display the returned image
//
//  Parms:     
//  Returns:
//
void CTabSnapshot::snp_CallbackDownloadPixture()
{
   QString clStr;

   pclView->DisplayImageFromData(pclRpiNet->baHttpResp);
   pclView->GetImageInfo(&clStr);
   pclLib->MSG_AddText(pclUi->Snp_Messages, "Snapshot image: ", clStr);
}

