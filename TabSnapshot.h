/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabSnapshot.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Snapshot tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABSNAPSHOT_H
#define TABSNAPSHOT_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"
#include "ImageViewer.h"


class CTabSnapshot : public QDialog
{
    Q_OBJECT

public:
   CTabSnapshot(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabSnapshot();

signals:

private slots:
   void              on_Snp_HttpReady              (qint64);
   void              on_Snp_HttpProgress           (qint64);
   void              on_Snp_HttpDisconnected       (bool);
   void              on_Snp_Timeout                ();
   void              on_Snp_TabChanged             (int);
   void              on_Snp_ButtonSnapshot_clicked ();
   //
public:
   CPiJson          *pclParJson;

private:
   void              snp_ConnectToServer           (HCNX);
   void              snp_ConnectToServer           (HCNX, QString);
   qint64            snp_HandleProgressbar         (bool, qint64);
   void              snp_SetupConnection           (HCNX);
   void              snp_TerminateConnection       (STCNX);
   void              snp_UpdateCnxStatus           (STCNX);
   void              snp_HandleHttpCallback        (CPiJson *);
   void              snp_CallbackDownloadPixture   ();
   void              snp_CallbackSnapshotPixture   ();
   //
   Ui::Dialog       *pclUi;
   //
   qint64            llToDownload;
   qint64            llDownloaded;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclValJson;
   CNvmStorage      *pclNvm;
   CImageViewer     *pclView;
};

#endif // TABSNAPSHOT_H
