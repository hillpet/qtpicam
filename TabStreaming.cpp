/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabStreaming.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Streaming tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *             CR008:   Take streamer tool from JSON file
 *                      Handle the duration correctly
 *
 *
 *
 *
 *
 *
**/
#include <QTimer>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
extern const char *pcJsonParseError;
extern const char *pcConnecting;
extern const char *pcConnectionError;
//
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsValue;
//
extern const char *pcKeyIsRecording;
extern const char *pcKeyIsStreamPort;
extern const char *pcKeyIsLastFile;
extern const char *pcKeyIsStreamTool;
extern const char *pcKeyIsLastFileSize;

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabStreaming::CTabStreaming(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;
   QByteArray  baCamParms;

   pclUi      = pclParent;
   pclLib     = new CPatrn; 
   pclTimer   = new QTimer(this);
   pclParJson = new CPiJson;
   pclValJson = new CPiJson;
   pclRpiNet  = new CPinet();
   pclNvm     = new CNvmStorage;
   pclView    = new CImageViewer(pclUi->Str_Image);
   pclProcess = NULL;
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //
   //       Signals                                                                    Slots
   connect( pclUi->Str_ButtonPreview,     SIGNAL(clicked()),                           this,             SLOT(on_Str_ButtonPreview_clicked())      );
   connect( pclUi->Str_ButtonStream,      SIGNAL(clicked()),                           this,             SLOT(on_Str_ButtonStream_clicked())       );
   connect( pclUi->Str_ButtonShowVlc,     SIGNAL(clicked()),                           this,             SLOT(on_Str_ButtonShowVlc_clicked())      );
   //
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Str_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Str_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Str_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Str_HttpDisconnected(bool))       );
   //
   //  Dialog window done
   //
   iStreaming =  0;
   iDuration  =  0;
   pclLib->MSG_AddText(pclUi->Str_Messages, "TAB:Streaming init OK", clStr);
}

//
//  Function:  CTabStreaming destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabStreaming::~CTabStreaming()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
   //
   str_StopStreamer();
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Str_ButtonPreview_clicked
//  Purpose:   Take snapshot image
//
//  Parms:     
//  Returns:
//
void CTabStreaming::on_Str_ButtonPreview_clicked()
{
   QString clStr;

   //
   // Make snapshot, include all selected parameters
   //
   clStr = pclLib->LIB_BuildOptionsList(pclParJson);
   str_ConnectToServer(HCNX_SNAPSHOT, clStr);
}

//
//  Function:  on_Str_ButtonStream_clicked
//  Purpose:   Start/Stop streaming
//
//  Parms:     
//  Returns:
//
void CTabStreaming::on_Str_ButtonStream_clicked()
{
   //
   // Start/Stop streaming, include all selected parameters
   //
   if(iStreaming > 0)
   {
      //
      // Streaming is ongoing: stop it
      //
      pclLib->MSG_AddText(pclUi->Str_Messages, "BTN:Stop");
      str_ConnectToServer(HCNX_STOP);
   }
   else if(iStreaming == 0)
   {
      //
      // No streaming is ongoing: start one
      // Include all selected (and valid) options
      //
      QString clOpt = pclLib->LIB_BuildOptionsList(pclParJson);
      //
      // Set up streaming duration
      // Set up streaming port
      //
      clOpt += "&Duration=";
      clOpt += str_GetDuration();
      //
      clOpt += "&StreamPort=";
      clOpt += str_GetPort();
      //
      pclLib->MSG_AddText(pclUi->Str_Messages, "BTN:Start", clOpt);
      str_ConnectToServer(HCNX_STREAM, clOpt);
   }
   else
   {
      //
      // Streaming has errors: retry to connect
      //
      pclLib->MSG_AddText(pclUi->Str_Messages, "BTN:Error");
      str_ConnectToServer(HCNX_STR_PARMS);
   }
}

//
//  Function:  on_Str_ButtonShowVlc_clicked
//  Purpose:   (Re)start VLC to show streaming video
//
//  Parms:     
//  Returns:
//
void CTabStreaming::on_Str_ButtonShowVlc_clicked()
{
   if(iStreaming)
   {
      str_StartStreamer();
   }
}

/* ======   Private Functions separator ===========================================
void _____Signals_Slots_____(){}
==============================================================================*/

//
//  Function:  on_Str_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabStreaming::on_Str_HttpProgress(qint64 llReceived)
{
   // QString clStr;
   // 
   // clStr.sprintf("on_Str_HttpProgress(): %lld bytes received", llReceived);
   // pclLib->MSG_AddText(pclUi->Str_Messages, clStr);
   //
   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   str_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Str_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabStreaming::on_Str_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Str_Messages, clStr);
}

//
//  Function:  on_Str_HttpDisconnected
//  Purpose:   ***** HTTP reply from the Raspberry Pi has been received ****
//             Parse the JSON document received from the server
//
//  Parms:     
//  Returns:
//
void CTabStreaming::on_Str_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   if(fBinary)
   {
      str_HandleHttpCallback(pclValJson);
   }
   else
   {
      //
      // These commands return all settings, so we need to reparse
      // the new JSON vars
      //
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         str_TerminateConnection(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data, result is 
         //    pclValJson->clStrList : all root key names
         //    pclValJson->clRootObj : the root object
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)   
         {
            //
            // All is Okee: we have a JSON object as reply
            //
            switch( pclRpiNet->GetHttpCnx() )
            {
               case HCNX_STR_PARMS:
               case HCNX_STREAM:
               case HCNX_STOP:
                  str_UpdateButtons();
                  break;

               default:
               case HCNX_IDLE:
               case HCNX_DLD_FILE:
               case HCNX_SNAPSHOT:
                  break;
            }
            str_HandleHttpCallback(pclValJson);
         }
         else
         {
            //
            // All is not Okee: we have a BAD JSON object as reply
            //
            iStreaming = -1;
            str_UpdateButtons();
            str_TerminateConnection(STCNX_ERROR);
         }
      }
   }
}

//
//  Function:  on_Str_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//  Note:      Timer runs at:
//             HTTP_GEN_TIMEOUT  on connecting
//             1000 mSecs        on streaming
//
void CTabStreaming::on_Str_Timeout()
{
   if(iStreaming >= 0)
   {
      //
      // The RPi seems to be doing OKee
      //
      iStreaming++;
      str_UpdateStreamingStatus();
      //
      // Start VLC streamer after a short delay
      //
      if(iStreaming == 3)
      {
         str_StartStreamer();
      }
   }
   else
   {
      //
      // Some kind of comm error
      //
      pclTimer->stop();
      str_TerminateConnection(STCNX_DISCONNECTED);
   }
}

//
//  Function:  on_Str_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//
void CTabStreaming::on_Str_TabChanged(int iIdx)
{
   QString  clStr;

   clStr = pclUi->TabWidget->tabText(iIdx);
   pclLib->MSG_AddText(pclUi->Str_Messages, clStr);
   str_ConnectToServer(HCNX_STR_PARMS);
}

//
//  Function:  on_Str_Vlc_Started
//  Purpose:   VLC Streaming video has started
//
//  Parms:     
//  Returns:
//
void CTabStreaming::on_Str_Vlc_Started()
{
   //
   // VLC notification: running
   //
   pclUi->Str_ButtonShowVlc->setEnabled(false);
   pclLib->MSG_AddText(pclUi->Str_Messages, "on_Str_Vlc_Started()");
}

//
//  Function:  on_Str_Vlc_Finished
//  Purpose:   VLC Streaming video has finished
//
//  Parms:     Exitcode, Exitstatus
//  Returns:
//
void CTabStreaming::on_Str_Vlc_Finished(int iExit, QProcess::ExitStatus tStatus)
{
   QString  clStr;

   //
   // VLC notification: terminated
   //
   pclUi->Str_ButtonShowVlc->setEnabled(true);
   str_StopStreamer();
   //
   clStr.sprintf("on_Str_Vlc_Finished(): Exitcode=%d, Status=%d", iExit, tStatus);
   pclLib->MSG_AddText(pclUi->Str_Messages, clStr);
}

//
//  Function:  on_Str_Vlc_Error
//  Purpose:   VLC Streaming video: ERROR
//
//  Parms:     Error code
//  Returns:
//
void CTabStreaming::on_Str_Vlc_Error(QProcess::ProcessError tError)
{
   QString  clStr;

   str_StopStreamer();
   //
   clStr.sprintf("on_Str_Vlc_Error(): Errorcode=%d", tError);
   pclLib->MSG_AddText(pclUi->Str_Messages, clStr);
}


/* ======   Private Functions separator ===========================================
void _____Private_Methods_____(){}
==============================================================================*/

//
//  Function:  str_CheckStreamingStatus
//  Purpose:   Check if we have an ongoing streaming
//
//  Parms:     
//  Returns:   iStreaming (1...?) or 0
//
//  Note:      Updates local iStreaming:
//             iStreaming  =
//              -1      : Error retrieving "Recording" JSON value
//               0      : RPi is not recording
//               > 0    : Rpi is recording, iStreaming contains secs since last update
//
//
//
int CTabStreaming::str_CheckStreamingStatus()
{
   int     iTime;
   QString clStr = pclValJson->GetObjectString(pcKeyIsRecording);

   if( !str_ConvertHms(&clStr, &iTime) )
   {
      // We are NOT streaming
      iStreaming = 0;
      pclUi->Str_ButtonShowVlc->setEnabled(false);
   }
   else
   {
      // We ARE streaming
      iStreaming = iTime;
   }
   return(iStreaming);
}

//
//  Function:  str_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabStreaming::str_ConnectToServer(HCNX tReq)
{
   str_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Str_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Str_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  str_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabStreaming::str_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   str_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Str_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Str_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  str_ConvertHms
//  Purpose:   Convert time format to int
//
//  Parms:     Time ptr, result ptr
//  Returns:   true if we are streaming
//
//  Note:      Time format hh:mm:ss
//             result =
//               -1 Illegal time
//                0 Time = --:--:--
//             else Time in Secs
//
bool CTabStreaming::str_ConvertHms(QString *pclStr, int *piTime)
{
   bool  fCc=true;
   int   iTime=0;

   if(pclStr->contains("--:--:--"))
   {
      fCc = false;
   }
   else
   {
      QVariant clVar(*pclStr);
      if( clVar.canConvert(QVariant::Time) )
      {
         QTime clTime = clVar.toTime();
         if(clTime.isValid())
         {
            iTime = (clTime.hour() * 3600) + (clTime.minute() * 60) + clTime.second() + 1;
         }
         else
         {
            iTime = pclStr->toInt(&fCc) + 1;
            if(!fCc) iTime = -1;
         }
      }
      else
      {
         iTime = -1;
      }
   }
   *piTime = iTime;
   return(fCc);
}

//
//  Function:  str_GetDuration
//  Purpose:   Get the specified streaming duration (or 0 if not specified)
//
//  Parms:     
//  Returns:   Duration in mSecs
//
QString CTabStreaming::str_GetDuration()
{
   int      iTime=0;
   QString  clStr=pclUi->Str_Time->text();

   if( str_ConvertHms(&clStr, &iTime) )
   {
      clStr.sprintf("%d", iTime * 1000);
   }
   else clStr = "0";
   //
   iDuration = iTime;
   return(clStr);
}

//
//  Function:  str_GetPort
//  Purpose:   Get the port of the stream
//
//  Parms:     
//  Returns:   Port number
//
QString CTabStreaming::str_GetPort()
{
   bool     fOkee;
   int      i;
   QString  clPort = pclUi->Str_Port->text();

   if( clPort.length() == 0)
   {
      //
      // No port specified on this tab: revert to global parameter
      //
      clPort = pclParJson->GetObjectString(pcKeyIsStreamPort, pcKeyIsValue);
   }
   //
   // Make sure the port is valid! Hardcode if not.
   //
   i = clPort.toInt(&fOkee);
   if( !fOkee || (i <= 1024) || (i >= 65536) ) clPort = "5001";
   //
   return(clPort);
}

//
//  Function:  str_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabStreaming::str_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Str_ProgBar->setMaximum(1000);
      pclUi->Str_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Str_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Str_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  str_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabStreaming::str_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   str_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Str_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  str_StartStreamer
//  Purpose:   Start the foreground video streamer (vlc)
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_StartStreamer()
{
   int         iIdx;
   QString     clExec, clUrl, clIp, clPort;
   QStringList clArgs;
   //
   //pwjh clArgs << "F:\\Content\\Video Patrn\\buitenhofdeleistert.mp4";
   //pwjh clArgs << "http://192.168.178.12:5001" << "http://192.168.178.12:5001" << "--rtp-timeout=15";
   //
   if(pclProcess == NULL) 
   {
      pclProcess = new QProcess(this);
      //
      // Connect signals for VLC control
      //
      connect( pclProcess, SIGNAL(started()),                           this, SLOT(on_Str_Vlc_Started())                            );
      connect( pclProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(on_Str_Vlc_Finished(int, QProcess::ExitStatus))  );
      connect( pclProcess, SIGNAL(error(QProcess::ProcessError)),       this, SLOT(on_Str_Vlc_Error(QProcess::ProcessError))        );
      //
      // Setup correct streaming VLC IP address
      // Set up streaming port
      //
      iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
      clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
      //
      clUrl   = "http://";
      clUrl  += clIp;
      clUrl  += ":";
      clUrl  += str_GetPort();
      //
      clExec = pclParJson->GetObjectString(pcKeyIsStreamTool, pcKeyIsValue, 0);
      clArgs << clUrl << "--rtp-timeout=15";
      pclProcess->start(clExec, clArgs);
   }
}

//
//  Function:  str_StopStreamer
//  Purpose:   Stop the foreground video streamer (vlc)
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_StopStreamer()
{
   if(pclProcess) 
   {
      pclProcess->terminate();
      pclProcess->disconnect();
      pclProcess->close();
      delete pclProcess;
      pclProcess = NULL;
   }
}

//
//  Function:  str_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabStreaming::str_TerminateConnection(STCNX tStatus)
{
   str_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  str_UpdateButtons
//  Purpose:   Update the button text/color
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_UpdateButtons()
{
   switch( str_CheckStreamingStatus() )
   {
      case 0:
         // Not streaming
         pclUi->Str_ButtonStream->setText("START");
         pclUi->Str_ButtonStream->setStyleSheet("color:white; background-color: green");
         break;

      case -1:
         // Error streaming
         pclUi->Str_ButtonStream->setText("ERROR");
         pclUi->Str_ButtonStream->setStyleSheet("color:red; background-color: yellow");
         break;

      default:
         // Streaming
         pclUi->Str_ButtonStream->setText("STOP");
         pclUi->Str_ButtonStream->setStyleSheet("color:white; background-color: red");
         break;
   }
}

//
//  Function:  str_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabStreaming::str_UpdateCnxStatus(STCNX tStatus)
{
   //
   // Show connection status
   //
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Str_Status->setText(pcConnecting);
         pclUi->Str_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Str_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Str_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Str_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Str_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Str_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Str_Status->setText(pcConnectionError);
         break;
   }
}

//
//  Function:  str_UpdateStreamingStatus
//  Purpose:   Update the streaming status: --:--:-- or the actual time if streaming
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_UpdateStreamingStatus()
{
   //
   // Show streaming time
   //
   if(iStreaming < 0)
   {
      //
      // Error in retrieving streaming time: erase
      //
      pclUi->Str_Duration->setText(" Error !");
   }
   else if (iStreaming == 0)
   {
      //
      // "Streaming" JSON element shows RPi is NOT streaming
      //
      pclUi->Str_Duration->setText("--:--:--");
      pclUi->Str_Time->clear();
   }
   else
   {
      //
      // "Streaming" JSON element shows RPi streaming 
      //
      int     iTime=0;
      QTime   clRecTime(0, 0, 0);
      QTime   clDurTime(0, 0, 0);
      //
      clRecTime = clRecTime.addSecs(iStreaming);
      pclUi->Str_Duration->setText( clRecTime.toString(Qt::TextDate) );
      //
      // Update duration, if any
      //
      if(iDuration)
      {
         if(--iDuration > 0) 
         {
            // Still running 
            iTime = iDuration;
         }
         else if(iDuration == 0)
         {
            // Streaming has stopped at the server side by now: stop streaming client as well
            iDuration--;
            str_StopStreamer();
         }
         else if(iDuration < -3)
         {
            //
            // Server should be completed now: retrieve settings
            //
            iDuration = 0;
            str_ConnectToServer(HCNX_STR_PARMS);
         }
         //
         // Display streaming duration
         //
         clDurTime = clDurTime.addSecs(iTime);
         pclUi->Str_Time->setText( clDurTime.toString(Qt::TextDate) );
      }
      else
      {
         pclUi->Str_Time->clear();
      }
   }
}
/* ======   Private Functions separator ===========================================
void _____Callback_Methods_____(){}
==============================================================================*/

//
//  Function:  str_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_HandleHttpCallback(CPiJson *pclJson)
{
   pclJson = pclJson;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      case HCNX_STR_PARMS:
         pclUi->Str_Port->setText( pclJson->GetObjectString(pcKeyIsStreamPort) );
         // Fall through
      default:
         if(iStreaming > 0) pclTimer->start(1000);
         else               pclTimer->stop();
         str_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_STREAM:
         //
         // Streaming started: show seconds increase
         //
         pclTimer->start(1000);
         str_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_STOP:
         //
         // Streaming stopped
         //
         pclTimer->stop();
         str_TerminateConnection(STCNX_CONNECTED);
         //
         str_StopStreamer();
         break;

      case HCNX_DLD_FILE:
         //
         // Snapshot has been taken and downloaded
         //
         pclTimer->stop();
         str_TerminateConnection(STCNX_CONNECTED);
         str_CallbackDownloadPixture();
         break;

      case HCNX_SNAPSHOT:
         //
         // Snapshot was taken
         //
         pclTimer->stop();
         str_TerminateConnection(STCNX_CONNECTED);
         str_CallbackSnapshotPixture();
         break;
   }
   str_UpdateStreamingStatus();
}

//
//  Function:  str_CallbackSnapshotPixture
//  Purpose:   Http server has responded: download the snapshot image
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_CallbackSnapshotPixture()
{
   int      iSize=0;
   QString  clStr;

   clStr = pclValJson->GetObjectString(pcKeyIsLastFile);
   iSize = pclValJson->GetObjectInteger(pcKeyIsLastFileSize);

   if(iSize)
   {
      str_HandleProgressbar(true, (qint64)iSize); 
      str_ConnectToServer(HCNX_DLD_FILE, clStr);
   }
}

//
//  Function:  str_CallbackDownloadPixture
//  Purpose:   Http server has responded: display the returned image
//
//  Parms:     
//  Returns:
//
void CTabStreaming::str_CallbackDownloadPixture()
{
   QString clStr;

   pclView->DisplayImageFromData(pclRpiNet->baHttpResp);
   pclView->GetImageInfo(&clStr);
   pclLib->MSG_AddText(pclUi->Str_Messages, "Snapshot image: ", clStr);
}
