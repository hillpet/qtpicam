/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabStreaming.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Streaming tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABSTREAMING_H
#define TABSTREAMING_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"
#include "ImageViewer.h"


class CTabStreaming : public QDialog
{
    Q_OBJECT

public:
   CTabStreaming(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabStreaming();

signals:

private slots:
   void              on_Str_ButtonPreview_clicked  ();
   void              on_Str_ButtonStream_clicked   ();
   void              on_Str_ButtonShowVlc_clicked  ();
   void              on_Str_HttpReady              (qint64);
   void              on_Str_HttpProgress           (qint64);
   void              on_Str_HttpDisconnected       (bool);
   void              on_Str_Timeout                ();
   void              on_Str_TabChanged             (int);
   //
   void              on_Str_Vlc_Started            ();
   void              on_Str_Vlc_Finished           (int, QProcess::ExitStatus);
   void              on_Str_Vlc_Error              (QProcess::ProcessError);
   //
public:
   CPiJson          *pclParJson;

private:
   int               str_CheckStreamingStatus      ();
   void              str_ConnectToServer           (HCNX);
   void              str_ConnectToServer           (HCNX, QString);
   bool              str_ConvertHms                (QString *, int *);
   QString           str_GetDuration               ();
   QString           str_GetPort                   ();
   qint64            str_HandleProgressbar         (bool, qint64);
   void              str_SetupConnection           (HCNX);
   void              str_StartStreamer             ();
   void              str_StopStreamer              ();
   void              str_TerminateConnection       (STCNX);
   void              str_UpdateButtons             ();
   void              str_UpdateCnxStatus           (STCNX);
   void              str_UpdateStreamingStatus     ();
   //
   void              str_HandleHttpCallback        (CPiJson *);
   void              str_CallbackDownloadPixture   ();
   void              str_CallbackSnapshotPixture   ();
   //
   Ui::Dialog       *pclUi;
   //
   int               iStreaming;
   int               iDuration;
   qint64            llToDownload;
   qint64            llDownloaded;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclValJson;
   //
   CNvmStorage      *pclNvm;
   CImageViewer     *pclView;
   QProcess         *pclProcess;
};

#endif // TABSTREAMING_H
