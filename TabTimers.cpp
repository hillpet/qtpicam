/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabTimers.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Timers tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QTimer>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
static const char *pcKeyIsCommands        = "TimerCommands";
static const char *pcKeyIsAlarms          = "Alarms";
static const char *pcKeyIsNumber          = "Number";
//
static const char *pcFieldIsCommand       = "Command";
static const char *pcFieldIsUsed          = "Used";
static const char *pcFieldIsArmed         = "Armed";
static const char *pcFieldIsTimestamp     = "Timestamp";
static const char *pcFieldIsRepeatTime    = "RepeatTime";
static const char *pcFieldIsRepeatNumber  = "RepeatNumber";
static const char *pcFieldIsRepeatCount   = "RepeatCount";
static const char *pcFieldIsRepeatNext    = "RepeatNext";
//
static const char *pcFieldIsDuration      = "Duration";
static const char *pcFieldIsLapseTime     = "LapseTime";
//
// compound http statements:
//
static const char *pcAlarmCommand         = "&AlarmCmd=";
static const char *pcAlarmTime            = "&AlarmTime="; 
static const char *pcAlarmRepeat          = "&AlarmRep="; 
static const char *pcAlarmNumber          = "&AlarmNum=";
//
extern const char *pcJsonParseError;
extern const char *pcConnecting;
extern const char *pcConnectionError;
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsValue;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsSelected;

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabTimers::CTabTimers(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;

   iCurAlarm   = 0;
   iCurCommand = 0;
   iNumAlarms  = 0;
   //
   pclUi      = pclParent;
   pclLib     = new CPatrn; 
   pclTimer   = new QTimer(this);
   pclParJson = new CPiJson;
   pclValJson = new CPiJson;
   pclRpiNet  = new CPinet();
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //
   //       Signals                                                                    Slots
   connect( pclUi->Tim_ComboCmds,         SIGNAL(activated(int)),                      this,             SLOT(on_Tim_ComboCmds_activated(int))     );
   connect( pclUi->Tim_ButtonDelete,      SIGNAL(clicked()),                           this,             SLOT(on_Tim_ButtonDelete_clicked())       );
   connect( pclUi->Tim_ButtonTmrUpdate,   SIGNAL(clicked()),                           this,             SLOT(on_Tim_ButtonTmrUpdate_clicked())    );
   connect( pclUi->Tim_ButtonLapUpdate,   SIGNAL(clicked()),                           this,             SLOT(on_Tim_ButtonLapUpdate_clicked())    );
   //
   connect( pclUi->Tim_TimerList,         SIGNAL(itemPressed(QListWidgetItem *)),      this,             SLOT(on_Tim_TimerList_checked(QListWidgetItem *)) );
   connect( pclUi->Tim_TimerList,         SIGNAL(currentRowChanged(int)),              this,             SLOT(on_Tim_TimerList_clicked(int)) );
   connect( pclUi->Tim_TimerList,         SIGNAL(itemChanged(QListWidgetItem *)),      this,             SLOT(on_Tim_TimerList_changed(QListWidgetItem *)) );
   //
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Tim_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Tim_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Tim_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Tim_HttpDisconnected(bool))       );
   //
   tim_BuildComboList(pclParJson);
   tim_HandleProgressbar(true, 0);
   //
   //  Dialog window done
   //
   pclLib->MSG_AddText(pclUi->Tim_Messages, "TAB:Timers init OK", clStr);
}

//
//  Function:  CTabTimers destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabTimers::~CTabTimers()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
}

/* ======   Local   Functions separator ===========================================
void __Signal_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Tim_ComboCmds_activated
//  Purpose:   An different item in the ComboBox drop-down list has been selected
//
//  Parms:     New TimerCommand combobox drop-down index
//  Returns:
//  Note:      There is really nothing we should be doing here, the actual selected
//             TimerCommand will be secure in the ComboBox until it is needed when
//             the actual TimerCommand will be updated on the Raspberry.
//
void CTabTimers::on_Tim_ComboCmds_activated(int iComboIdx)
{
   iComboIdx = iComboIdx;
}

//
//  Function:  on_Tim_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabTimers::on_Tim_HttpProgress(qint64 llReceived)
{
   //test QString clStr;
   //test // 
   //test clStr.sprintf("on_Tim_HttpProgress(): %lld bytes received", llReceived);
   //test pclLib->MSG_AddText(pclUi->Tim_Messages, clStr);
   //test

   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   tim_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Tim_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabTimers::on_Tim_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Tim_Messages, clStr);
}

//
//  Function:  on_Tim_HttpDisconnected
//  Purpose:   ***** HTTP disconnected from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Data is Binary
//  Returns:
//
void CTabTimers::on_Tim_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   pclTimer->stop();
   //
   if(fBinary)
   {
      tim_HandleHttpCallback(pclValJson);
   }
   else
   {
      switch( pclRpiNet->GetHttpCnx() )
      {
         case HCNX_DELALARM:
            //
            // This command does not return all timer alarms, so we need to keep our 
            // existing JSON vars. After the last alarm has been deleted, we will 
            // retrieve the alarm settings.
            //
            tim_HandleHttpCallback(pclValJson);
            break;

         case HCNX_SETALARM:
         case HCNX_TIMELAPSE:
         case HCNX_TIM_PARMS:
         case HCNX_IDLE:
         default:
            //
            // These commands return all timer alarms, so we need to reparse
            // the new JSON vars
            //
            pclValJson->ClearAll();
            if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
            {
               tim_UpdateCnxStatus(STCNX_ERROR);
            }
            else
            {
               //
               // HTTP Server replied: parse JSON data, result is 
               //    pclValJson->clStrList : all root key names
               //    pclValJson->clRootObj : the root object
               //
               iNum = pclValJson->ParseLocalParameters();
               if(iNum > 0)   tim_HandleHttpCallback(pclValJson);
               else           tim_UpdateCnxStatus(STCNX_ERROR);
            }
            break;
      }
   }
}

//
//  Function:  on_Tim_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabTimers::on_Tim_Timeout()
{
   pclTimer->stop();
   tim_TerminateConnection(STCNX_DISCONNECTED);
}

//
//  Function:  on_Tim_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//  Note:      To get the tab info:
//             clStr = pclUi->TabWidget->tabText(iIdx);
//
void CTabTimers::on_Tim_TabChanged(int iIdx)
{
   iIdx = iIdx;
   tim_ConnectToServer(HCNX_TIM_PARMS);
}

//  Function:  on_Tim_TimerList_changed
//  Purpose:   One of the checkboxes in the timerlist has been changed by clicking
//             on the checkbox with the mouse. The checkbox toggles state.
//
//  Parms:     Checklist Item *
//  Returns:
//
void CTabTimers::on_Tim_TimerList_changed(QListWidgetItem *pclLwi)
{
   //QString  clStr;

   pclLwi = pclLwi;
   
   //clStr.sprintf("on_Tim_TimerList_changed(): Alarm=%d", iCurAlarm);
   //pclLib->MSG_AddText(pclUi->Tim_Messages, clStr);
}

//
//  Function:  on_Tim_TimerList_checked
//  Purpose:   One of the items in the timerlist has been selected (by mouse-click)
//
//  Parms:     Checklist Item *
//  Returns:
//
void CTabTimers::on_Tim_TimerList_checked(QListWidgetItem *pclLwi)
{
   //QString  clStr;

   if(pclLwi)
   {
      iCurAlarm = pclUi->Tim_TimerList->row(pclLwi);
      tim_ShowTimer(iCurAlarm, pclValJson);
   }
   //
   //clStr.sprintf("on_Tim_TimerList_checked(): Alarm=%d", iCurAlarm);
   //pclLib->MSG_AddText(pclUi->Tim_Messages, clStr);
}

//
//  Function:  on_Tim_TimerList_clicked
//  Purpose:   One of the items in the timerlist has been selected (cursor-keys)
//             Only called if a different item in the timer list has been selected
//
//  Parms:     Index
//  Returns:
//
void CTabTimers::on_Tim_TimerList_clicked(int iIdx)
{
   //QString clStr;

   if(iIdx >= 0)
   {
      //
      // Get the current row index of the listbox
      //
      iCurAlarm = iIdx;
   }
   else
   {
      iCurAlarm = 0;
   }
   tim_ShowTimer(iCurAlarm, pclValJson);
   //clStr.sprintf("on_Tim_TimerList_clicked(): Alarm=%d", iCurAlarm);
   //pclLib->MSG_AddText(pclUi->Tim_Messages, clStr);
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Tim_ButtonDelete_clicked
//  Purpose:   Delete timer
//
//  Parms:     
//  Returns:
//
void CTabTimers::on_Tim_ButtonDelete_clicked()
{
   int      iIdx;
   QString  clStr, clCmd;

   //
   // Read all timers and find a checked one
   //
   pclLib->MSG_AddText(pclUi->Tim_Messages, "on_Tim_ButtonDelete_clicked()");
   iIdx = tim_GetNextCheckedTimer();
   if(iIdx != -1)
   {
      //
      // We have a valid checked timer: delete it
      //
      clCmd = tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsCommand);
      if( clCmd.length() > 0)
      {
         //
         // Update this timer
         //
         clStr  = pcAlarmCommand;
         clStr += clCmd;
         clStr += pcAlarmTime;
         clStr += tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsTimestamp);
         //
         pclLib->MSG_AddText(pclUi->Tim_Messages, "Delete:", clStr);
         tim_ConnectToServer(HCNX_DELALARM, clStr);
      }
   }
   else
   {
      //
      // Refreah the timer list from the server
      //
      tim_ConnectToServer(HCNX_TIM_PARMS);
   }
}

//
//  Function:  on_Tim_ButtonTmrUpdate_clicked
//  Purpose:   Update timer list
//
//  Parms:     
//  Returns:
//
void CTabTimers::on_Tim_ButtonTmrUpdate_clicked()
{
   QString  clTmp, clStr;

   //pclLib->MSG_AddText(pclUi->Tim_Messages, "on_Tim_ButtonTmrUpdate_clicked()");
   //
   // Update this timer
   //
   clTmp = pclUi->Tim_ComboCmds->currentText();
   if( clTmp.length() > 0)
   {
      //
      // Update this timer
      //
      clStr  = pcAlarmCommand;
      clStr += clTmp;
      clStr += pcAlarmTime;
      clTmp  = pclUi->Tim_Timestamp->text();
      if( clTmp.length() > 0)
      {
         // Handle only non-empty timestamps
         clStr += clTmp;
         clStr += pcAlarmRepeat;
         clStr += pclUi->Tim_SnoozeSecs->text();
         clStr += pcAlarmNumber;
         clStr += pclUi->Tim_NumberRuns->text();
         //
         pclLib->MSG_AddText(pclUi->Tim_Messages, "Update/Add:", clStr);
         tim_ConnectToServer(HCNX_SETALARM, clStr);
      }
      else
      {
         pclLib->MSG_AddText(pclUi->Tim_Messages, "Update Timers: Ignoring emtpy time stamps!");
         tim_ConnectToServer(HCNX_TIM_PARMS);
      }
   }
}

//
//  Function:  on_Tim_ButtonLapUpdate_clicked
//  Purpose:   Update timelapse settings
//
//  Parms:     
//  Returns:
//
void CTabTimers::on_Tim_ButtonLapUpdate_clicked()
{
}

/* ======   Private Functions separator ===========================================
void __Private_Methods________(){}
==============================================================================*/

//
//  Function:  tim_BuildComboList
//  Purpose:   Build the ComboBox containing all possible timer commands
//
//  Parms:     Json ptr
//  Returns:   
//
void CTabTimers::tim_BuildComboList(CPiJson *pclJson)
{
   int         iIdx, iNumElements;
   JTYPE       tType;
   QString     clVal;

   tType        = pclJson->GetObjectType(iCurCommand, pcKeyIsEnum, (const char *)NULL);
   iNumElements = pclJson->GetArrayNumElements();
   //
   // Retrieve the object.members
   //
   pclUi->Tim_ComboCmds->clear();
   //
   for(iIdx=0; iIdx<iNumElements; iIdx++)
   {
      tType = pclJson->GetObjectType(pcKeyIsCommands, pcKeyIsEnum, (const char *)NULL, iIdx);
      switch(tType)
      {
         default:
            break;

         case JTYPE_STRING:
            clVal = pclJson->GetValueString();
            pclUi->Tim_ComboCmds->addItem(clVal);
            break;
      }
   }
   pclUi->Tim_ComboCmds->setCurrentIndex(0);
}

//
//  Function:  tim_BuildTimerList
//  Purpose:   Retrieve all timer settings and fill the listbox with it
//
//  Parms:     Json ptr
//  Returns:   
//
void CTabTimers::tim_BuildTimerList(CPiJson *pclJson)
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   iNumAlarms = pclJson->GetObjectInteger(pcKeyIsNumber);
   clStr.sprintf("tim_BuildTimerList(): RPi has %d Alarms", iNumAlarms);
   pclLib->MSG_AddText(pclUi->Tim_Messages, clStr);
   //
   if(iNumAlarms > 0)
   {
      //
      // Delete List, if any
      //`Disable sorting
      //
      if(pclUi->Tim_TimerList) 
      {
         pclUi->Tim_TimerList->clear();
         pclUi->Tim_TimerList->setSortingEnabled(false);
         //
         for(int i=0; i<iNumAlarms; i++)
         {
            clStr = tim_RetrieveAlarmField(i, pclJson, pcFieldIsCommand);
            if( clStr.length() == 0)
            {
               clStr = "--------";
            }
            //
            // Store this parameter into the QListWidget
            //
            pclLwi = new QListWidgetItem(clStr, pclUi->Tim_TimerList);
            pclLwi->setCheckState(Qt::Unchecked);
         }
         tim_ShowTimer(iCurAlarm, pclJson);
      }
      else
      {
         pclLib->MSG_AddText(pclUi->Tim_Messages, "tim_BuildTimerList():No Timerlist!");
      }
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Tim_Messages, "tim_BuildTimerList():Rpi has No Alarms!");
   }
}

//
//  Function:  tim_BuildTimelapse
//  Purpose:   Retrieve all timelapse settings
//
//  Parms:     Json ptr
//  Returns:   
//
void CTabTimers::tim_BuildTimelapse(CPiJson *pclJson)
{
   //
   // Retrieve all settings for the current timer
   //
   pclUi->Tim_Duration->setText(pclJson->GetObjectString(pcFieldIsDuration));
   pclUi->Tim_LapseTime->setText(pclJson->GetObjectString(pcFieldIsLapseTime));
   pclUi->Tim_NumberOfPix->setText("?");
}

//
//  Function:  tim_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns: 
//
void CTabTimers::tim_ConnectToServer(HCNX tReq)
{
   tim_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Tim_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Tim_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  tim_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabTimers::tim_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   tim_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Tim_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Tim_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  tim_GetNextCheckedTimer
//  Purpose:   Get the next checked timer, if any
//
//  Parms:     
//  Returns:   Timer index or -1
//
int CTabTimers::tim_GetNextCheckedTimer()
{
   int               iNr;
   QListWidgetItem  *pclLwi;

   iNr = pclUi->Tim_TimerList->count();
   //
   // Get the current row index of the listbox
   //
   for(int i=0; i<iNr; i++)
   {
      pclLwi = pclUi->Tim_TimerList->item(i);
      if(pclLwi)
      {
         if(pclLwi->checkState() == Qt::Checked)
         {
            //
            // Got one
            //
            pclLwi->setCheckState(Qt::Unchecked);
            return(i);
         }
      }
   }
   return(-1);
}

//
//  Function:  tim_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabTimers::tim_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Tim_ProgBar->setMaximum(1000);
      pclUi->Tim_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Tim_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Tim_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  tim_RetrieveAlarmField
//  Purpose:   Retrieve an Alarm field from a JSON object
//
//  Parms:     Index, Json ptr, Field
//  Returns:   String value
//
QString CTabTimers::tim_RetrieveAlarmField(int iIdx, CPiJson *pclJson, QString clField)
{
   int         iVal;
   JTYPE       tType;
   QByteArray  baKey;
   QString     clVal;
   char       *pcKey;

   //
   // Translate to JSON key
   //
   baKey = clField.toLocal8Bit();
   pcKey = baKey.data();
   //
   tType = pclJson->GetObjectType(pcKeyIsAlarms, pcKey, (const char *)NULL, iIdx);
   switch(tType)
   {
      default:
         break;

      case JTYPE_INTEGER:
         iVal = pclJson->GetValueInteger();
         clVal.sprintf("%d", iVal);
         break;

      case JTYPE_BOOL:
         if(pclJson->GetValueBool() == true) clVal = "true";
         else                                clVal = "false";
         break;

      case JTYPE_STRING:
         clVal = pclJson->GetValueString();
         break;
   }
   return(clVal);
}

//
//  Function:  tim_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabTimers::tim_TerminateConnection(STCNX tStatus)
{
   tim_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  tim_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabTimers::tim_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Tim_Status->setText(pcConnecting);
         pclUi->Tim_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Tim_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Tim_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Tim_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Tim_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Tim_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Tim_Status->setText(pcConnectionError);
         break;
   }
}

//
//  Function:  tim_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabTimers::tim_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   tim_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Tim_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  tim_ShowTimer
//  Purpose:   Retrieve all timer settings for this timer and show them
//
//  Parms:     Timer idx, Json ptr
//  Returns:   
//
void CTabTimers::tim_ShowTimer(int iIdx, CPiJson *pclJson)
{
   int      iCurIdx=0;
   QString  clStr;

   if(iIdx >= 0)
   {
      //
      // Retrieve all settings for the current timer
      //
      pclUi->Tim_Used->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsUsed));
      pclUi->Tim_Armed->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsArmed));
      pclUi->Tim_Timestamp->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsTimestamp));
      pclUi->Tim_SnoozeSecs->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsRepeatTime));
      pclUi->Tim_NumberRuns->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsRepeatNumber));
      pclUi->Tim_RunCount->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsRepeatCount));
      pclUi->Tim_NextRun->setText(tim_RetrieveAlarmField(iIdx, pclValJson, pcFieldIsRepeatNext));
      //
      // Have combo box show current command
      //
      clStr = tim_RetrieveAlarmField(iIdx, pclJson, pcFieldIsCommand);
      if( clStr.length() > 0)
      {
         iCurIdx = pclUi->Tim_ComboCmds->findText(clStr, Qt::MatchExactly);
         if(iCurIdx == -1) 
         {
            //
            // Not found !
            //
            iCurIdx = 0;
         }
      }
   }
   pclUi->Tim_ComboCmds->setCurrentIndex(iCurIdx);
   pclUi->Tim_TimerList->setCurrentRow(iCurAlarm);
}

/* ======   Private Functions separator ===========================================
void __Callback_Methods________(){}
==============================================================================*/

//
//  Function:  tim_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabTimers::tim_HandleHttpCallback(CPiJson *pclJson)
{
   QString clStr;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      default:
         tim_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_TIM_PARMS:
         tim_TerminateConnection(STCNX_CONNECTED);
         clStr = "Get Alarms";
         //
         // Timelapse and Alarm settings have been returned to us: handle it
         //
         iCurAlarm = 0;
         tim_BuildTimerList(pclJson);
         tim_BuildTimelapse(pclJson);
         break;

      case HCNX_DELALARM:
         tim_TerminateConnection(STCNX_CONNECTED);
         clStr = "Delete Alarms";
         //
         // Redo until all checked timers have been deleted !
         //
         on_Tim_ButtonDelete_clicked();
         break;

      case HCNX_SETALARM:
         tim_TerminateConnection(STCNX_CONNECTED);
         clStr = "Set Alarms";
         //
         // Refresh the timer list from the server
         //
         tim_ConnectToServer(HCNX_TIM_PARMS);
         break;

      case HCNX_TIMELAPSE:
         tim_TerminateConnection(STCNX_CONNECTED);
         clStr = "Timelapse";
         break;
   }
   pclLib->MSG_AddText(pclUi->Tim_Messages, "tim_HandleHttpCallback():", clStr);
}

