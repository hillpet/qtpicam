/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabTimers.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Timers tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABTIMERS_H
#define TABTIMERS_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"


class CTabTimers : public QDialog
{
    Q_OBJECT

public:
   CTabTimers(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabTimers();

signals:

private slots:
   void              on_Tim_ButtonDelete_clicked   ();
   void              on_Tim_ButtonLapUpdate_clicked();
   void              on_Tim_ButtonTmrUpdate_clicked();
   void              on_Tim_ComboCmds_activated    (int);
   void              on_Tim_HttpDisconnected       (bool);
   void              on_Tim_HttpProgress           (qint64);
   void              on_Tim_HttpReady              (qint64);
   void              on_Tim_TabChanged             (int);
   void              on_Tim_Timeout                ();
   void              on_Tim_TimerList_changed      (QListWidgetItem *);
   void              on_Tim_TimerList_checked      (QListWidgetItem *);
   void              on_Tim_TimerList_clicked      (int);
   //
private:
   void              tim_BuildComboList            (CPiJson *);
   void              tim_BuildTimelapse            (CPiJson *);
   void              tim_BuildTimerList            (CPiJson *);
   void              tim_BuildValueFields          (CPiJson *);
   void              tim_CommandBuildList          (CPiJson *);
   void              tim_ConnectToServer           (HCNX);
   void              tim_ConnectToServer           (HCNX, QString);
   int               tim_GetNextCheckedTimer       ();
   void              tim_HandleHttpCallback        (CPiJson *);
   qint64            tim_HandleProgressbar         (bool, qint64);
   QString           tim_RetrieveAlarmField        (int, CPiJson *, QString);
   void              tim_SetupConnection           (HCNX);
   void              tim_ShowTimer                 (int, CPiJson *);
   void              tim_TerminateConnection       (STCNX);
   void              tim_UpdateCnxStatus           (STCNX);
   //
   Ui::Dialog       *pclUi;
   //
   // Private variables
   //
   int               iCurAlarm;
   int               iCurCommand;
   int               iNumAlarms;
   qint64            llToDownload;
   qint64            llDownloaded;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclParJson;
   CPiJson          *pclValJson;
   CNvmStorage      *pclNvm;
};

#endif // TABTIMERS_H
