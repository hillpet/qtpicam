/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabVideos.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Videos tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *             CR003:   Copied from Fotos
 *
 *
 *
 *
 *
 *
 *
**/

#include <QMessageBox>
#include <QFileDialog>
#include <QTimer>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

//
// Parameter root-keys
//
extern const char *pcJsonParseError;
extern const char *pcConnecting;
extern const char *pcConnectionError;
//
extern const char *pcKeyIsAddress;
extern const char *pcKeyIsStatus;
extern const char *pcKeyIsEnum;
extern const char *pcKeyIsUrl;
extern const char *pcKeyIsPort;
extern const char *pcKeyIsPathname;
extern const char *pcKeyIsFilter;
extern const char *pcKeyIsNumFiles;
extern const char *pcKeyIsVideos;
extern const char *pcKeyIsImageName;
extern const char *pcKeyIsImageSize;
extern const char *pcKeyIsValue;

/* ======   Local   Functions separator ===========================================
void _____Public_Methods_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialog box
//
//  Parms:     Parent window
//  Returns:
//
CTabVideos::CTabVideos(Ui::Dialog *pclParent, QJsonDocument *pclDoc)
{
   QString     clStr;
   QByteArray  baCamParms;

   pclUi          = pclParent;
   //
   iVidTotal      = 0;
   iVidIndex      = 0;
   pclLib         = new CPatrn; 
   pclTimer       = new QTimer(this);
   pclParJson     = new CPiJson;
   pclValJson     = new CPiJson;
   pclRpiNet      = new CPinet();
   pclNvm         = new CNvmStorage;
   pclView        = new CImageViewer(pclUi->Vid_Image);
   //
   if( pclParJson->SwitchToGlobalParameters(pclDoc) )
   {
      int iNum = pclParJson->ParseGlobalParameters();
      clStr.sprintf("-JSON doc has %d parameters", iNum);
   }
   else
   {
      clStr += "-No JSON parameters!";
   }
   //
   //    Define signals and slots
   //                                                                                                                                      
   //       Signals                                                                    Slots
   connect( pclUi->Vid_CheckListFiles,    SIGNAL(currentRowChanged(int)),              this,             SLOT(on_Vid_CheckList_clicked(int))       );
   connect( pclUi->Vid_CheckListFiles,    SIGNAL(itemDoubleClicked(QListWidgetItem *)),this,             SLOT(on_Vid_ButtonShow_clicked())         );
   connect( pclUi->Vid_Filter,            SIGNAL(editingFinished()),                   this,             SLOT(on_Vid_FilterChanged())              );
   connect( pclUi->Vid_Wildcard,          SIGNAL(editingFinished()),                   this,             SLOT(on_Vid_WildcardChanged())            );
   connect( pclUi->Vid_ButtonShow,        SIGNAL(clicked()),                           this,             SLOT(on_Vid_ButtonShow_clicked())         );
   connect( pclUi->Vid_ButtonDelete,      SIGNAL(clicked()),                           this,             SLOT(on_Vid_ButtonDelete_clicked())       );
   connect( pclUi->Vid_ButtonDownload,    SIGNAL(clicked()),                           this,             SLOT(on_Vid_ButtonDownload_clicked())     );
   connect( pclUi->Vid_ButtonBrowse,      SIGNAL(clicked()),                           this,             SLOT(on_Vid_ButtonBrowse_clicked())       );
   connect( pclUi->Vid_ButtonAll,         SIGNAL(clicked()),                           this,             SLOT(on_Vid_ButtonAll_clicked())          );
   connect( pclUi->Vid_ButtonNone,        SIGNAL(clicked()),                           this,             SLOT(on_Vid_ButtonNone_clicked())         );
   connect( pclTimer,                     SIGNAL(timeout()),                           this,             SLOT(on_Vid_Timeout())                    );
   connect( pclRpiNet,                    SIGNAL(ReadHttpProgress(qint64)),            this,             SLOT(on_Vid_HttpProgress(qint64))         );
   connect( pclRpiNet,                    SIGNAL(ReadHttpReady(qint64)),               this,             SLOT(on_Vid_HttpReady(qint64))            );
   connect( pclRpiNet,                    SIGNAL(ReadHttpDisconnected(bool)),          this,             SLOT(on_Vid_HttpDisconnected(bool))       );
   //
   vid_HandleProgressbar(true, 0);
   pclUi->Vid_Filter->setText(pclParJson->GetObjectString(pcKeyIsFilter, pcKeyIsValue));
   pclUi->Vid_Wildcard->setText("*");
   //
   //  Dialog window done
   //
   pclLib->MSG_AddText(pclUi->Vid_Messages, "TAB:Video init OK", clStr);
}

//
//  Function:  CTabVideos destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CTabVideos::~CTabVideos()
{
   delete pclLib;
   delete pclParJson;
   delete pclValJson;
   delete pclRpiNet;
   delete pclTimer;
   delete pclNvm;
   delete pclView;
}

/* ======   Local   Functions separator ===========================================
void __Signal_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Vid_HttpProgress
//  Purpose:   ***** HTTP progress update from the Raspberry Pi has been received ****
//             Update the progressbar
//
//  Parms:     Number of bytes received
//  Returns:
//
void CTabVideos::on_Vid_HttpProgress(qint64 llReceived)
{
   // QString clStr;
   // 
   // clStr.sprintf("on_Vid_HttpProgress(): %lld bytes received", llReceived);
   // pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
   //
   pclTimer->start(HTTP_GEN_TIMEOUT); //Restart time-out
   vid_HandleProgressbar(false, llReceived);
}

//
//  Function:  on_Vid_HttpReady
//  Purpose:   ***** HTTP Ready from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Number of bytes received in total on this connection
//  Returns:
//
void CTabVideos::on_Vid_HttpReady(qint64 llTotal)
{
   QString  clStr;

   clStr.sprintf("Connected: %lld bytes received", llTotal);
   pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
}

//
//  Function:  on_Vid_HttpDisconnected
//  Purpose:   ***** HTTP disconnected from the Raspberry Pi has been received ****
//             Handle the completion callback
//
//  Parms:     Data is Binary
//  Returns:
//
void CTabVideos::on_Vid_HttpDisconnected(bool fBinary)
{
   QString  clStr;
   int      iNum;

   pclTimer->stop();
   //
   if(fBinary)
   {
      vid_HandleHttpCallback(pclValJson);
   }
   else
   {
      pclValJson->ClearAll();
      if(!pclValJson->ReadDocumentData(pclRpiNet->baHttpResp, &clStr) )
      {
         vid_UpdateCnxStatus(STCNX_ERROR);
      }
      else
      {
         //
         // HTTP Server replied: parse JSON data, result is 
         //    pclValJson->clStrList : all root key names
         //    pclValJson->clRootObj : the root object
         //
         iNum = pclValJson->ParseLocalParameters();
         if(iNum > 0)
         {
            vid_HandleHttpCallback(pclValJson);
         }
         else
         {
            vid_UpdateCnxStatus(STCNX_ERROR);
         }
      }
   }
}

//
//  Function:  on_Vid_Timeout
//  Purpose:   Connection timeout
//
//  Parms:     
//  Returns:
//
void CTabVideos::on_Vid_Timeout()
{
   pclTimer->stop();
   vid_TerminateConnection(STCNX_DISCONNECTED);
}

//
//  Function:  on_Vid_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//  Note:      To get the tab info:
//             clStr = pclUi->TabWidget->tabText(iIdx);
//
void CTabVideos::on_Vid_TabChanged(int iIdx)
{
   iIdx = iIdx;

   QString clStr = pclUi->Vid_Filter->text();
   vid_ConnectToServer(HCNX_GET_VIDEOS, clStr);
}

//
//  Function:  on_Vid_FilterChanged
//  Purpose:   Filter field was edited, and maybe changed
//
//  Parms:     
//  Returns:
//  Note:      
//
void CTabVideos::on_Vid_FilterChanged()
{
   QString clStr = pclUi->Vid_Filter->text();
   vid_ConnectToServer(HCNX_GET_VIDEOS, clStr);
}

//
//  Function:  on_Vid_WildcardChanged
//  Purpose:   Wildcard field was edited, and maybe changed
//
//  Parms:     
//  Returns:
//  Note:      
//
void CTabVideos::on_Vid_WildcardChanged()
{
   vid_ImagesHide();
}

/* ======   Local   Functions separator ===========================================
void _____Button_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Vid_ButtonAll_clicked
//  Purpose:   Select ALL picture
//
//  Parms:     
//  Returns:
//
void CTabVideos::on_Vid_ButtonAll_clicked()
{
   QListWidgetItem  *pclLwi;

   //
   // Set all checklistboxes of the list
   //
   for(int iIdx=0; iIdx<iVidTotal; iIdx++)
   {
      pclLwi = pclUi->Vid_CheckListFiles->item(iIdx);
      if(pclLwi->isHidden() == false)
      {
         pclLwi->setCheckState(Qt::Checked);
      }
   }
}

//
//  Function:  on_Vid_ButtonBrowse_clicked
//  Purpose:   Browse for download pathname
//
//  Parms:     
//  Returns:
//
void CTabVideos::on_Vid_ButtonBrowse_clicked()
{
   QString clDir = QFileDialog::getExistingDirectory(this, tr("Select download directory"), "/", 
                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

   pclUi->Vid_Pathname->setText(clDir);
}

//
//  Function:  on_Vid_ButtonDelete_clicked
//  Purpose:   Delete picture
//
//  Parms:     
//  Returns:
//  Note:      To display images:
//             pclView->DisplayImageFromFile("E:\\Users\\Peter\\ProjQt\\Picam\\test.jpg");
//             pclView->DisplayImageFromData(baTest);
//
void CTabVideos::on_Vid_ButtonDelete_clicked()
{
   int               iNumChecked=0;
   qint64            llTotal=0;
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   //
   // Poll all checklistboxes of the list to find out what files to delete
   //
   for(int iIdx=0; iIdx<iVidTotal; iIdx++)
   {
      pclLwi = pclUi->Vid_CheckListFiles->item(iIdx);
      switch( pclLwi->checkState() )
      {
         case Qt::Checked:
            if(pclLwi->isHidden() == false)
            {
               iNumChecked++;
               clVar    = pclLwi->data(Qt::UserRole);
               llTotal += clVar.toInt();
            }
            break;

         default:
            break;
      }
   }
   if(llTotal)
   {
      //
      // Setup the progress bar
      //
      vid_HandleProgressbar(true, llTotal); 
      //
      // Start deleting
      //
      vid_CallbackDeleteNextImage();
   }
   clStr.sprintf("Delete %d Images: %lld bytes", iNumChecked, llTotal);
   pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
}

//
//  Function:  on_Vid_ButtonDownload_clicked
//  Purpose:   Delete picture
//
//  Parms:     
//  Returns:
//  Note:      To display images:
//             pclView->DisplayImageFromFile("E:\\Users\\Peter\\ProjQt\\Picam\\test.jpg");
//             pclView->DisplayImageFromData(baTest);
//
void CTabVideos::on_Vid_ButtonDownload_clicked()
{
   int               iNumChecked=0;
   qint64            llTotal=0;
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   //
   // Poll all checklistboxes of the list to find out what files to download
   //
   for(int iIdx=0; iIdx<iVidTotal; iIdx++)
   {
      pclLwi = pclUi->Vid_CheckListFiles->item(iIdx);
      switch( pclLwi->checkState() )
      {
         case Qt::Checked:
            if(pclLwi->isHidden() == false)
            {
               iNumChecked++;
               clVar    = pclLwi->data(Qt::UserRole);
               llTotal += clVar.toInt();
            }
            break;

         default:
            break;
      }
   }
   if(llTotal)
   {
      //
      // Setup the progress bar
      //
      vid_HandleProgressbar(true, llTotal); 
      //
      // Start downloading
      //
      vid_CallbackShowNextImage();
   }
   clStr.sprintf("Download %d Images: %lld bytes", iNumChecked, llTotal);
   pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
}

//
//  Function:  on_Vid_ButtonNone_clicked
//  Purpose:   Select NO picture
//
//  Parms:     
//  Returns:
//
void CTabVideos::on_Vid_ButtonNone_clicked()
{
   QListWidgetItem  *pclLwi;

   //
   // Set all checklistboxes of the list
   //
   for(int iIdx=0; iIdx<iVidTotal; iIdx++)
   {
      pclLwi = pclUi->Vid_CheckListFiles->item(iIdx);
      if(pclLwi->isHidden() == false)
      {
         pclLwi->setCheckState(Qt::Unchecked);
      }
   }
}

//
//  Function:  on_Vid_ButtonShow_clicked
//  Purpose:   Show picture
//
//  Parms:     
//  Returns:
//  Note:      To display images:
//             pclView->DisplayImageFromFile("E:\\Users\\Peter\\ProjQt\\Picam\\test.jpg");
//             pclView->DisplayImageFromData(baTest);
//
void CTabVideos::on_Vid_ButtonShow_clicked()
{
   QString clStr;

   clStr = QFileDialog::getOpenFileName(this, tr("Open Video"), "/", tr("Video Files (*.mp4 *.h264 *.avi)"));

   vid_VideoPlayFile(clStr);
}

//
//  Function:  on_Vid_CheckList_clicked
//  Purpose:   One of the items in the pixture list has been selected (mouse or keys)
//             Only called if a different item in the pixture list has been selected
//
//  Parms:     Index
//  Returns:
//  Note:      Can be called on tab changes with iIdx = -1 !
//
void CTabVideos::on_Vid_CheckList_clicked(int iIdx)
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   if(iIdx >= 0)
   {
      //
      // Get the current row index of the listbox
      //
      iVidIndex = iIdx;
      pclLwi    = pclUi->Vid_CheckListFiles->currentItem();
      clStr     = clVidPath;
      clStr    += pclLwi->text();
   }
}

/* ======   Private Functions separator ===========================================
void __Private_Methods________(){}
==============================================================================*/

//
//  Function:  vid_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabVideos::vid_ConnectToServer(HCNX tReq)
{
   vid_SetupConnection(tReq);
   if( pclRpiNet->SendHttpRequest(tReq) )
   {
      pclLib->MSG_AddText(pclUi->Vid_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Vid_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  vid_ConnectToServer
//  Purpose:   Connect to Raspberry Pi HTTP server
//
//  Parms:     HTTP request, options
//  Returns:
//
void CTabVideos::vid_ConnectToServer(HCNX tReq, QString clOpt)
{
   QByteArray  baOpt;
   const char *pcOpt;

   vid_SetupConnection(tReq);
   baOpt = clOpt.toLocal8Bit();
   pcOpt = baOpt.constData();
   //
   if( pclRpiNet->SendHttpRequest(tReq, pcOpt) )
   {
      pclLib->MSG_AddText(pclUi->Vid_Messages, "Connect to Server....");
   }
   else
   {
      pclLib->MSG_AddText(pclUi->Vid_Messages, "Connect to Server: connection is still ongoing");
   }
}

//
//  Function:  vid_GetNextImage
//  Purpose:   Retrieve the next checked file from the list widget
//
//  Parms:     
//  Returns:   ListWidgetItem *
//
QListWidgetItem *CTabVideos::vid_GetNextImage()
{
   QListWidgetItem  *pclLwi;

   //
   // Check all checklistboxes of the list to find the next image
   //
   for(int iIdx=0; iIdx<iVidTotal; iIdx++)
   {
      pclLwi = pclUi->Vid_CheckListFiles->item(iIdx);
      if( pclLwi && (pclLwi->isHidden() == false) )
      {
         if(pclLwi->checkState() == Qt::Checked)
         {
            return(pclLwi);
         }
      }
   }
   return(NULL);
}

//
//  Function:  vid_HandleProgressbar
//  Purpose:   Handle the update of the progressbar. Scale max value to 1000.
//
//  Parms:     FlagMax, Increment in bytes
//  Returns:   Total number 
//
qint64 CTabVideos::vid_HandleProgressbar(bool fSetMax, qint64 llValue)
{
   int      iValue;
   qint64   llTotal;

   if(fSetMax)
   {
      llTotal      = llDownloaded;
      llDownloaded = 0;
      llToDownload = llValue;
      pclUi->Vid_ProgBar->setMaximum(1000);
      pclUi->Vid_ProgBar->setValue(0);
   }
   else
   {
      if(llToDownload)
      {
         llDownloaded += llValue;
         llTotal       = llDownloaded;
         iValue = (int)((llDownloaded * 1000)/ llToDownload);
         pclUi->Vid_ProgBar->setValue(iValue);
      }
      else
      {
         pclUi->Vid_ProgBar->setValue(0);
         llTotal = 0;
      }
   }
   return(llTotal);
}

//
//  Function:  vid_ImagesBuildList
//  Purpose:   Fill the checklist widget with all the pixtures
//
//  Parms:     
//  Returns:
//
void CTabVideos::vid_ImagesBuildList(CPiJson *pclJson)
{
   int               iVidSize;
   QString           clStr;
   QVariant          clVar;
   QListWidgetItem  *pclLwi;

   clVidPath   = pclJson->GetObjectString(pcKeyIsPathname);
   iVidTotal   = pclJson->GetObjectInteger(pcKeyIsNumFiles);
   //
   // Delete List, if any
   // Disable sorting
   //
   if(pclUi->Vid_CheckListFiles) 
   {
      pclUi->Vid_CheckListFiles->clear();
      pclUi->Vid_CheckListFiles->setSortingEnabled(false);
      //
      for(int i=0; i<iVidTotal; i++)
      {
         clStr    = pclJson->GetObjectString(pcKeyIsVideos, pcKeyIsImageName, i);
         iVidSize = pclJson->GetObjectInteger(pcKeyIsVideos, pcKeyIsImageSize, i);
         //
         // Store this pixture filename into the QListWidget
         //
         pclLwi = new QListWidgetItem(clStr, pclUi->Vid_CheckListFiles);
         //
         // Reset checkbox value
         //
         pclLwi->setCheckState(Qt::Unchecked);
         pclLwi->setSelected(false);
         //
         clVar = iVidSize;
         pclLwi->setData(Qt::UserRole, clVar);
      }
      vid_ImagesHide();
      iVidIndex = 0;
      pclUi->Vid_CheckListFiles->setCurrentRow(iVidIndex);
   }
}

//
//  Function:  vid_ImagesHide
//  Purpose:   Hide all images in the list that do not match the current wildcard
//
//  Parms:     
//  Returns:   Not hidden number in the list
//
int CTabVideos::vid_ImagesHide()
{
   QString           clStr, clWld;
   QListWidgetItem  *pclLwi;

   clWld = pclUi->Vid_Wildcard->text();
   //
   for(int iIdx=0; iIdx<iVidTotal; iIdx++)
   {
      pclLwi = pclUi->Vid_CheckListFiles->item(iIdx);
      clStr = pclLwi->text();
      pclLwi->setHidden((vid_WildcardMatch(clWld, clStr, true)==false));
   }
   return(pclUi->Vid_CheckListFiles->count());
}

//
//  Function:  vid_TerminateConnection
//  Purpose:   Terminate connection to Raspberry Pi HTTP server
//
//  Parms:     Status
//  Returns:
//
void CTabVideos::vid_TerminateConnection(STCNX tStatus)
{
   vid_UpdateCnxStatus(tStatus);
   pclRpiNet->SendHttpRequest(HCNX_IDLE);
}

//
//  Function:  vid_UpdateCnxStatus
//  Purpose:   Update the connection status
//
//  Parms:     STCNX
//  Returns:
//
void CTabVideos::vid_UpdateCnxStatus(STCNX tStatus)
{
   switch(tStatus)
   {
      default:
      case STCNX_CONNECTING:
         pclUi->Vid_Status->setText(pcConnecting);
         pclUi->Vid_Status->setStyleSheet("color:gray; background-color: white");
         break;

      case STCNX_CONNECTED:
         pclUi->Vid_Status->setStyleSheet("color:white; background-color: green");
         pclUi->Vid_Status->setText( pclValJson->GetObjectString(pcKeyIsStatus) );
         break;

      case STCNX_ERROR:
         pclUi->Vid_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Vid_Status->setText(pcJsonParseError);
         break;

      case STCNX_DISCONNECTED:
         pclUi->Vid_Status->setStyleSheet("color:white; background-color: red");
         pclUi->Vid_Status->setText(pcConnectionError);
         break;
   }
}

//
//  Function:  vid_SaveImage
//  Purpose:   Save the image we just received from the Raspberry
//
//  Parms:     Filename
//  Returns:   true if OKee
//
bool CTabVideos::vid_SaveImage(QString clName)
{
   bool    fCc=false;
   QString clPath=pclUi->Vid_Pathname->text();

   if( clPath.length() > 0)
   {
      clPath += "\\";
      clPath += clName;
      QFile clFile(clPath);

      if( clFile.open(QIODevice::WriteOnly) )
      {
         clFile.write(pclRpiNet->baHttpResp);
         clFile.close();
         fCc = true;
      }
   }
   //
   // Verify if all went OKee
   //
   if(fCc == false)
   {
      QMessageBox::information(this, QGuiApplication::applicationDisplayName(), tr("Error saving image: check download path !"));
   }
   return(fCc);
}

//
//  Function:  vid_SetupConnection
//  Purpose:   Setup connection to Raspberry Pi HTTP server
//
//  Parms:     HTTP request
//  Returns:
//
void CTabVideos::vid_SetupConnection(HCNX tReq)
{
   int      iIdx;
   QString  clIp, clPort;

   tReq = tReq;

   pclTimer->start(HTTP_GEN_TIMEOUT);
   vid_UpdateCnxStatus(STCNX_CONNECTING);
   //
   iIdx   = pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsValue, 0);
   clIp   = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsUrl, iIdx);
   clPort = pclParJson->GetObjectString(pcKeyIsAddress, pcKeyIsEnum, pcKeyIsPort, iIdx);
   //
   pclLib->MSG_AddText(pclUi->Vid_Messages, "Connect ", clIp);
   pclRpiNet->SetIpAddress(clIp, clPort.toInt());
}

//
//  Function:  vid_VideoPlayFile
//  Purpose:   Play a video file
//
//  Parms:     Filepath
//  Returns:
//
void CTabVideos::vid_VideoPlayFile(QString clVideo)
{
   QMediaPlayer     *pclPlayer;
   QVideoWidget     *pclVideo;

   pclPlayer = new QMediaPlayer;
   //
   //pclVideo = new QVideoWidget;
   pclVideo = new QVideoWidget(pclUi->Vid_Image);
   pclPlayer->setVideoOutput(pclVideo);
   pclPlayer->setMedia(QUrl::fromLocalFile(clVideo));
   //
   pclVideo->show();
   pclPlayer->play();
}

//
//  Function:  vid_WildcardMatch
//  Purpose:   Check if the string matches the wildcard search
//
//  Parms:     Wildcard string, Destination string, NoCase YESNO
//  Returns:   
//
bool CTabVideos::vid_WildcardMatch(QString sWld, QString sSrc, bool fIgnoreCase)
{
   QString  clWld, clSrc;
   int      iS=0, iW=0, iWcopy=-1;

   clWld = sWld;
   clSrc  = sSrc;
   //
   if(fIgnoreCase)
   {
      // Ignore case
      clWld.toUpper();
      clSrc.toUpper();
   }
   while( (clWld[iW] != 0) && (clSrc[iS] != 0) )
   {
      if(clWld[iW] == '*')
      {
         iWcopy = iW;
         if(clSrc[iS++] == clWld[iW+1])
         {
            iW += 2;
         }
      }
      else if(clWld[iW] == '?') 
      {
         iS++;
         iW++;
      }
      else if(clWld[iW++] != clSrc[iS++])
      {
         if(iWcopy != -1)
         {
            // We have an ongoing * match
            iW = iWcopy;
         }
         else
         {
            return(false);
         }
      }
   }
   //
   while(clWld[iW] == '*') 
   {
      iW++;
   }
   return(clWld[iW] == 0);
}

/* ======   Private Functions separator ===========================================
void __Callback_Methods________(){}
==============================================================================*/

//
//  Function:  vid_HandleHttpCallback
//  Purpose:   Http server has responded: callback services
//
//  Parms:     
//  Returns:
//
void CTabVideos::vid_HandleHttpCallback(CPiJson *pclJson)
{
   QListWidgetItem  *pclLwi;

   switch( pclRpiNet->GetHttpCnx() )
   {
      case HCNX_IDLE:
         break;

      default:
         vid_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_DLD_FILE:
         //
         // Request went out to download (an) image(s)
         //    o Store this image
         //    o download more
         //
         pclLwi = vid_GetNextImage();
         if(pclLwi)
         {
            vid_SaveImage(pclLwi->text());
            //
            // Image has been handled: forget it
            //
            pclLwi->setCheckState(Qt::Unchecked);
            vid_CallbackShowNextImage();
         }
         else vid_TerminateConnection(STCNX_CONNECTED);
         break;

      case HCNX_SHW_VIDEO:
         //
         // Request went out to display an image
         //
         vid_TerminateConnection(STCNX_CONNECTED);
         vid_CallbackShowPixture();
         break;

      case HCNX_GET_VIDEOS:
         //
         // Request went out to download the relevant data of all fotos matching the
         // filtersettings;
         //
         vid_TerminateConnection(STCNX_CONNECTED);
         vid_ImagesBuildList(pclJson);
         break;

      case HCNX_DEL_VIDEOS:
         //
         // Request went out to delete (an) image(s)
         //    o update deleted bytes
         //    o delete more
         //
         pclLwi = vid_GetNextImage();
         if(pclLwi)
         {
            QVariant clVar = pclLwi->data(Qt::UserRole);
            vid_HandleProgressbar(false, clVar.toInt());
            //
            // Image has been handled: forget it
            //
            pclLwi->setCheckState(Qt::Unchecked);
            vid_CallbackDeleteNextImage();
         }
         else vid_TerminateConnection(STCNX_CONNECTED);
         break;
   }
}

//
//  Function:  vid_CallbackDeleteNextImage
//  Purpose:   Handle the deletion of the next checked item
//
//  Parms:     
//  Returns:
//
void CTabVideos::vid_CallbackDeleteNextImage()
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   //
   // Check all checklistboxes of the list to find the next image
   //
   pclLwi = vid_GetNextImage();
   if(pclLwi)
   {
      //
      // Delete this image
      //
      clStr = pclLwi->text();
      pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
      vid_ConnectToServer(HCNX_DEL_VIDEOS, clStr);
   }
   else
   {
      //
      // All images deleted: rebuild file list
      //
      clStr.sprintf("Deleted all Images: %lld bytes", vid_HandleProgressbar(true, 0));
      pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
      on_Vid_FilterChanged();
   }
}
//
//  Function:  vid_CallbackShowPixture
//  Purpose:   Http server has responded: display the returned image
//
//  Parms:     
//  Returns:
//
void CTabVideos::vid_CallbackShowPixture()
{
   QString clStr;

   pclView->DisplayImageFromData(pclRpiNet->baHttpResp);
   pclView->GetImageInfo(&clStr);
   pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
}

//
//  Function:  vid_CallbackShowNextImage
//  Purpose:   Handle the download of the next checked item
//
//  Parms:     
//  Returns:
//
void CTabVideos::vid_CallbackShowNextImage()
{
   QString           clStr;
   QListWidgetItem  *pclLwi;

   //
   // Check all checklistboxes of the list to find the next image
   //
   pclLwi = vid_GetNextImage();
   if(pclLwi)
   {
      //
      // Download this image
      //
      clStr  = clVidPath;
      clStr += pclLwi->text();
      pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
      vid_ConnectToServer(HCNX_DLD_FILE, clStr);
   }
   else
   {
      //
      // All images downloaded
      //
      vid_TerminateConnection(STCNX_CONNECTED);
      clStr.sprintf("Downloaded all Images: %lld bytes", vid_HandleProgressbar(true, 0));
      pclLib->MSG_AddText(pclUi->Vid_Messages, clStr);
   }
}

