/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          TabVideos.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera Videos tab
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       13 Aug 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef TABVIDEOS_H
#define TABVIDEOS_H

#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "RpiNet.h"
#include "ImageViewer.h"


class CTabVideos : public QDialog
{
    Q_OBJECT

public:
   CTabVideos(Ui::Dialog *pclParent = 0, QJsonDocument *pclDoc = NULL);
  ~CTabVideos();
  //

signals:

private slots:
   void              on_Vid_ButtonAll_clicked      ();
   void              on_Vid_ButtonDelete_clicked   ();
   void              on_Vid_ButtonDownload_clicked ();
   void              on_Vid_ButtonNone_clicked     ();
   void              on_Vid_ButtonShow_clicked     ();
   void              on_Vid_ButtonBrowse_clicked   ();
   void              on_Vid_CheckList_clicked      (int);
   void              on_Vid_HttpProgress           (qint64);
   void              on_Vid_HttpDisconnected       (bool);
   void              on_Vid_HttpReady              (qint64);
   void              on_Vid_TabChanged             (int);
   void              on_Vid_Timeout                ();
   void              on_Vid_FilterChanged          ();
   void              on_Vid_WildcardChanged        ();
   //
public:
   CPiJson          *pclParJson;

private:
   void              vid_ConnectToServer           (HCNX);
   void              vid_ConnectToServer           (HCNX, QString);
   QListWidgetItem  *vid_GetNextImage              ();
   qint64            vid_HandleProgressbar         (bool, qint64);
   void              vid_ImagesBuildList           (CPiJson *);
   int               vid_ImagesHide                ();
   bool              vid_SaveImage                 (QString);
   void              vid_SetupConnection           (HCNX);
   void              vid_TerminateConnection       (STCNX);
   void              vid_UpdateCnxStatus           (STCNX);
   void              vid_VideoPlayFile             (QString);
   bool              vid_WildcardMatch             (QString, QString, bool);
   //
   void              vid_HandleHttpCallback        (CPiJson *);
   void              vid_CallbackShowPixture       ();
   void              vid_CallbackShowNextImage     ();
   void              vid_CallbackDeleteNextImage   ();
   //
   Ui::Dialog       *pclUi;
   //
   int               iVidIndex;
   int               iVidTotal;
   qint64            llToDownload;
   qint64            llDownloaded;
   QString           clVidPath;
   //
   CPatrn           *pclLib;
   QTimer           *pclTimer;
   CPinet           *pclRpiNet;
   CPiJson          *pclValJson;
   CNvmStorage      *pclNvm;
   CImageViewer     *pclView;
};


#endif // TABVIDEOS_H
