/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          dialog.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera dialogs
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       02 Jun 2014
**
 *  Revisions:
 *             CR008:   Do not start all tabs twice
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QMessageBox>
#include <libpatrn.h>
#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

extern const char *pcKeyIsAddress;
extern const char *pcKeyIsAutoconnect;

/* ======   Local   Functions separator ===========================================
void ______Global_Functions_____(){}
==============================================================================*/

//
//  Function:  Dialog constructor
//  Purpose:   Setup TabWidget dialob box
//
//  Parms:     Parent window
//  Returns:
//
Dialog::Dialog(QWidget *pclParent) : QDialog(pclParent), pclUi(new Ui::Dialog)
{
   bool        fTabsOKee;
   QByteArray  baCamParms;
   QString     clStr;

   pclUi->setupUi(this);
   //
   pclNvm     = new CNvmStorage;
   pclRpiNet  = new CPinet();
   pclParJson = new CPiJson;
   //
   // Read Cam parameters into global storage
   //
   pclNvm->NvmGet(NVM_JSON_CONFIG_FILE, &clStr);
   baCamParms = pclParJson->ReadDocumentFile(clStr);
   fTabsOKee  = pclParJson->ReadDocumentData(baCamParms, &clParmDoc, &clStr);
   //
   pclParJson->SwitchToGlobalParameters(&clParmDoc);
   if( pclParJson->ParseGlobalParameters() == -1)
   {
      fTabsOKee = false;
   }
   //
   // List all TABs here
   //
   pclTabGeneric   = new CTabGeneric   (pclUi, &clParmDoc);
   pclTabMotion    = new CTabMotion    (pclUi, &clParmDoc);
   pclTabSnapshot  = new CTabSnapshot  (pclUi, &clParmDoc);
   pclTabTimers    = new CTabTimers    (pclUi, &clParmDoc);
   pclTabRecord    = new CTabRecord    (pclUi, &clParmDoc);
   pclTabStreaming = new CTabStreaming (pclUi, &clParmDoc);
   pclTabFotos     = new CTabFotos     (pclUi, &clParmDoc);
   pclTabVideos    = new CTabVideos    (pclUi, &clParmDoc);
   //
   if(fTabsOKee)
   {
      // OKee JSON data
      //    int            ParseLocalParameters    ();
      //    QString        GetObjectName           (int);
      //    JTYPE          SetObjectRoot           (int);
      //
   }
   else
   {
      // Error JSON data
      QMessageBox::information(this, QGuiApplication::applicationDisplayName(), tr("Error reading JSON parameter file !"));
   }
   //
   //    Define signals and slots
   //
   //       Signals                                                  Slots
   connect( this,                SIGNAL(ServerAutoConnect()),        pclTabGeneric,       SLOT(on_Gen_ServerAutoConnect())          );
   connect( pclUi->TabWidget,    SIGNAL(currentChanged(int)),        this,                SLOT(on_Dlg_TabChanged(int))              );
   //
   // TAB change notifications
   //
   connect( this,                SIGNAL(NfyTabGeneric(int)),         pclTabGeneric,       SLOT(on_Gen_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabMotion(int)),          pclTabMotion,        SLOT(on_Mot_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabSnapshot(int)),        pclTabSnapshot,      SLOT(on_Snp_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabTimers(int)),          pclTabTimers,        SLOT(on_Tim_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabRecord(int)),          pclTabRecord,        SLOT(on_Rec_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabStreaming(int)),       pclTabStreaming,     SLOT(on_Str_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabFotos(int)),           pclTabFotos,         SLOT(on_Fot_TabChanged(int))              );
   connect( this,                SIGNAL(NfyTabVideos(int)),          pclTabVideos,        SLOT(on_Vid_TabChanged(int))              );
   //
   // Start with Generic TAB always
   //
   pclUi->TabWidget->setCurrentIndex(0);
   //
   // If enabled, try to connect to the Raspberry Pi HTTP server with the default settings
   //
   if( pclParJson->GetObjectInteger(pcKeyIsAddress, pcKeyIsAutoconnect) )
   {
      emit ServerAutoConnect();
   }
}

//
//  Function:  Dialog destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
Dialog::~Dialog()
{
   delete pclNvm;
   delete pclParJson;
   delete pclUi;
   delete pclTabGeneric;
   delete pclTabMotion;
   delete pclTabSnapshot;
   delete pclTabTimers;
   delete pclTabRecord;
   delete pclTabStreaming;
   delete pclTabFotos;
   delete pclTabVideos;
}


/* ======   Local   Functions separator ===========================================
void __Signal_Handlers_____(){}
==============================================================================*/

//
//  Function:  on_Dlg_TabChanged
//  Purpose:   Handle the change of a tab widget
//
//  Parms:     Index
//  Returns:
//
void Dialog::on_Dlg_TabChanged(int iIdx)
{
   //
   // Until we have connection: stay on the generic tab to connect to a Raspberry Server
   //
   switch( pclRpiNet->IsConnected() )
   {
      case STCNX_CONNECTED:
         switch(iIdx)
         {
            default:                            break;
            case 0: emit NfyTabGeneric(iIdx);   break;
            case 1: emit NfyTabMotion(iIdx);    break;
            case 2: emit NfyTabSnapshot(iIdx);  break;
            case 3: emit NfyTabTimers(iIdx);    break;
            case 4: emit NfyTabRecord(iIdx);    break;
            case 5: emit NfyTabStreaming(iIdx); break;
            case 6: emit NfyTabFotos(iIdx);     break;
            case 7: emit NfyTabVideos(iIdx);    break;
         }
         break;

      case STCNX_DISCONNECTED:
      case STCNX_ERROR:
      case STCNX_CONNECTING:
      default:
         //
         // Connecting is still ongoing: ignore tab changes untill connected
         //
         pclUi->TabWidget->setCurrentIndex(0);
         break;
   }
}

