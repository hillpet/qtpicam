/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          dialog.h
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera dialogs
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       02 Jun 2014
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QJsonDocument>
#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "ui_dialog.h"
#include "TabGeneric.h"
#include "TabMotion.h"
#include "TabTimers.h"
#include "TabSnapshot.h"
#include "TabRecord.h"
#include "TabStreaming.h"
#include "TabFotos.h"
#include "TabVideos.h"

class Dialog : public QDialog
{
    Q_OBJECT

public:
   explicit       Dialog               (QWidget *parent = 0);
                 ~Dialog               ();

signals:
   void           ServerAutoConnect    ();
   void           NfyTabGeneric        (int);
   void           NfyTabMotion         (int);
   void           NfyTabTimers         (int);
   void           NfyTabSnapshot       (int);
   void           NfyTabRecord         (int);
   void           NfyTabStreaming      (int);
   void           NfyTabFotos          (int);
   void           NfyTabVideos         (int);

private slots:
   void           on_Dlg_TabChanged    (int);

private:
   QJsonDocument  clParmDoc;              // Parameter JSON Document
   //
   CNvmStorage   *pclNvm;
   CPinet        *pclRpiNet;
   CPiJson       *pclParJson;
   CTabGeneric   *pclTabGeneric;
   CTabMotion    *pclTabMotion;
   CTabSnapshot  *pclTabSnapshot;
   CTabTimers    *pclTabTimers;
   CTabRecord    *pclTabRecord;
   CTabStreaming *pclTabStreaming;
   CTabFotos     *pclTabFotos;
   CTabVideos    *pclTabVideos;
   //
   Ui::Dialog    *pclUi;
};

#endif // DIALOG_H
