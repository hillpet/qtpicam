/*  (c) Copyright:  2014 Patrn ESS, Confidential Data
**
**  $Workfile:          http_connections.h
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            HTTP connection strings
**
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Aug 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// http GET defines
//
//             Command           reply bin   http request     
EXTRACT_CNX(   HCNX_IDLE,        false,       NULL                               )
EXTRACT_CNX(   HCNX_CAM_PARMS,   false,      "cam.json?parms"                    )
EXTRACT_CNX(   HCNX_TIM_PARMS,   false,      "timelapse.json?parms"              )
EXTRACT_CNX(   HCNX_REC_PARMS,   false,      "record.json?parms"                 )
EXTRACT_CNX(   HCNX_STR_PARMS,   false,      "stream.json?parms"                 )
EXTRACT_CNX(   HCNX_SHW_VIDEO,   true,       "%s"                                )
EXTRACT_CNX(   HCNX_SHW_FOTO,    true,       "%s"                                )
EXTRACT_CNX(   HCNX_DLD_FILE,    true,       "%s"                                )
EXTRACT_CNX(   HCNX_GET_FOTOS,   false,      "cam.json?fotos&filter=%s"          )
EXTRACT_CNX(   HCNX_DEL_FOTOS,   false,      "cam.json?rm-fotos&%s"              )
EXTRACT_CNX(   HCNX_GET_VIDEOS,  false,      "cam.json?videos&filter=%s"         )
EXTRACT_CNX(   HCNX_DEL_VIDEOS,  false,      "cam.json?rm-videos&%s"             )
EXTRACT_CNX(   HCNX_RECORD,      false,      "cam.json?record%s"                 )
EXTRACT_CNX(   HCNX_STOP,        false,      "cam.json?stop"                     )
EXTRACT_CNX(   HCNX_STREAM,      false,      "cam.json?stream%s"                 )
EXTRACT_CNX(   HCNX_SNAPSHOT,    false,      "cam.json?snapshot%s"               )
EXTRACT_CNX(   HCNX_TIMELAPSE,   false,      "cam.json?timelapse"                )
EXTRACT_CNX(   HCNX_SETALARM,    false,      "cam.json?alarm%s"                  )
EXTRACT_CNX(   HCNX_DELALARM,    false,      "cam.json?alarm%s&Delete=yes"       )
EXTRACT_CNX(   HCNX_SHOW_MOTION, true,       "@%s"                               )
EXTRACT_CNX(   HCNX_PARM_MOTION, false,      "cam.json?mparms"                   )
EXTRACT_CNX(   HCNX_SNAP_MOTION, false,      "cam.json?msnap%s"                  )
EXTRACT_CNX(   HCNX_NORM_MOTION, false,      "cam.json?mnorm%s"                  )
EXTRACT_CNX(   HCNX_ZOOM_MOTION, false,      "cam.json?mzoom%s"                  )
EXTRACT_CNX(   HCNX_MARK_MOTION, false,      "cam.json?mdelta%s"                 )
