/*  (c) Copyright: 2014 Patrn ESS, Confidential Data
**
**  $Workfile:          Picam.cpp
**  $Revision:          
**  $Modtime:           
**
**  Purpose:            Raspberry Pi Camera apps entry point (TheApp)
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       02 Jun 2014
**
 *  Revisions:
 *             CR008:   Support for CR008 changes
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <QApplication>
#include <QTranslator>
//
#include <libpatrn.h>
#include <NvmStorage.h>
#include <PatrnFiles.h>
#include <RpiJson.h>
#include "dialog.h"
#include "NvmDefs.h"
#include "RpiNet.h"

bool NewNvm(CNvmStorage *pclNvm);

#define  PICAM_VERSION        "1.01"
#define  PICAM_DB_VERSION     "1.10"

//
// Standard parameter key names:
//
const char *pcPatrn              = "(C)2013..2015 - www.patrn.nl";
const char *pcConnecting         = "**  Connecting  **";
const char *pcConnectionError    = "** Disconnected **";
const char *pcJsonParseError     = "**  Json Error  **";
const char *pcMsgParseError      = "Json parse error: ";
//
const char *pcKeyIsAddress       = "Address";
const char *pcKeyIsStatus        = "Status";
const char *pcKeyIsSwVersion     = "Version";
const char *pcKeyIsLastFile      = "LastFile";
const char *pcKeyIsLastFileSize  = "LastFileSize";
//
const char *pcKeyIsDescription   = "description";
const char *pcKeyIsSelected      = "selected";
const char *pcKeyIsAutoconnect   = "autoconnect";
const char *pcKeyIsChanged       = "changed";
const char *pcKeyIsDefault       = "default";
const char *pcKeyIsType          = "type";
const char *pcKeyIsName          = "name";
const char *pcKeyIsEnum          = "enum";
const char *pcKeyIsObject        = "object";
const char *pcKeyIsUrl           = "url";
const char *pcKeyIsPort          = "port";
const char *pcKeyIsNumparms      = "numparms";
const char *pcKeyIsValue         = "value";
//
const char *pcKeyIsPathname      = "Pathname";
const char *pcKeyIsFilter        = "Filter";
const char *pcKeyIsNumFiles      = "NumFiles";
const char *pcKeyIsFotos         = "fotos";
const char *pcKeyIsVideos        = "videos";
const char *pcKeyIsImageName     = "Name";
const char *pcKeyIsImageSize     = "Size";
const char *pcKeyIsRecording     = "Recording";
const char *pcKeyIsStreamAddress = "StreamAddr";
const char *pcKeyIsStreamPort    = "StreamPort";
const char *pcKeyIsStreamTool    = "StreamTool";
//
// Set the global dirty marker 
//
int     CPiJson::iGlobalDirty    = 1;

//
//  Function:  main
//  Purpose:   PiCam apps entry point
//
//  Parms:     Cmd line arguments
//  Returns:
//
int main(int argc, char *argv[])
{
   bool           fNvmOKee;
   QString        clStr;
   QString        clWr, clRd;
   CNvmStorage   *pclNvm;
   char          *pcConfig=(char *)"picam.cfg";
   QApplication   clApps(argc, argv);

   
   pclNvm = new CNvmStorage(NUM_NVMS);
   //
   // Read Archive
   //
   if(pclNvm->NvmRead(pcConfig) == false)
   {
      // Archive is bad
      fNvmOKee = false;
   }
   else
   {
      // NVM loaded correctly: check if we rolled the rev.
      pclNvm->NvmGet(NVM_DB_VERSION, &clStr);
      fNvmOKee = (clStr.compare(PICAM_DB_VERSION ) == 0);
   }
   //
   // Check if we have a correct NV storage
   //
   if(!fNvmOKee)
   {
      NewNvm(pclNvm);
      pclNvm->NvmWrite(pcConfig);
   }

   Dialog clDiag;
   clDiag.show();

   return clApps.exec();
}

//
//  Function:  NewNvm
//  Purpose:   Init a new NVM
//
//  Parms:     
//  Returns:
//
bool NewNvm(CNvmStorage *pclNvm)
{
   //
   // Take the Apps folder for default folder location
   // 
   pclNvm->NvmPut(NVM_VERSION,               (char *)PICAM_VERSION);                         // Version,
   pclNvm->NvmPut(NVM_INITIALIZED,           (char *)"1");                                   // Initialized,
   //
   pclNvm->NvmPut(NVM_GEN_VIEWER_DIR,        (char *)"");                                    // JSON Viewer directory
   pclNvm->NvmPut(NVM_PICAM_EXECMODE,        (char *)"0");                                   // Picam execution mode
   //
   pclNvm->NvmPut(NVM_PIX_FILTER,            (char *)"201");                                 // Picture filter
   pclNvm->NvmPut(NVM_PIX_WILDCARD,          (char *)"*");                                   // Picture wildcard
   pclNvm->NvmPut(NVM_PIX_SERVER_DIR,        (char *)"");                                    // Picture directory server side
   //
   pclNvm->NvmPut(NVM_VID_FILTER,            (char *)"201");                                 // Video   filter
   pclNvm->NvmPut(NVM_VID_WILDCARD,          (char *)"*");                                   // Video   wildcard
   pclNvm->NvmPut(NVM_VID_SERVER_DIR,        (char *)"");                                    // Video   directory server side
   //
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_APPEND,   (char *)"1");                                   // LOG: Append checkbox
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_ERRORS,   (char *)"1");                                   //      Errors checkbox
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_PROGRESS, (char *)"0");                                   //      Progress checkbox
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_DEBUG,    (char *)"1");                                   //      Debug checkbox
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_JSON,     (char *)"1");                                   //      json  checkbox
   //
   pclNvm->NvmPut(NVM_PATH_LOG_ERRORS,       (char *)"PicamErrors.txt");                     //      all errors
   pclNvm->NvmPut(NVM_PATH_LOG_PROGRESS,     (char *)"PicamProgress.txt");                   //      all progress
   pclNvm->NvmPut(NVM_PATH_LOG_DEBUG,        (char *)"PicamTraces.txt");                     //      all debug
   pclNvm->NvmPut(NVM_PATH_LOG_JSON,         (char *)"PicamJson.txt");                       //      all json
   //
   #ifdef _WIN32                                                                             // Win 7
   pclNvm->NvmPut(NVM_JSON_CONFIG_FILE,      (char *)"E:/Users/Peter/ProjQt/Picam/picam.json");
   pclNvm->NvmPut(NVM_PIX_CLIENT_DIR,        (char *)"F:/Content/Foto Rpi");                 // Picture directory client side
   pclNvm->NvmPut(NVM_VID_CLIENT_DIR,        (char *)"F:/Content/Video Rpi");                // Video   directory client side
   #else
   pclNvm->NvmPut(NVM_JSON_CONFIG_FILE,      (char *)"/Users/peter/Proj/Picam/picam.json");  //iMac
   pclNvm->NvmPut(NVM_PIX_CLIENT_DIR,        (char *)"/Users/peter/Pictures");               // Picture directory client side
   pclNvm->NvmPut(NVM_VID_CLIENT_DIR,        (char *)"/Users/peter/Movies/");                // Video   directory client side
   #endif
   //
   pclNvm->NvmPut(NVM_DB_VERSION,            (char *)PICAM_DB_VERSION);                      // Db_version,
   //
   return(true);
}


