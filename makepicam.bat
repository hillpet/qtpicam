@echo off
rem
rem QT5 Picam builder helper: use correct libpatrn.a prior to build/link
rem

echo QT5 Picam builder helper: select libpatrn.a (%2)
echo %date%--%time% %1,%2 >>makepicam.log

if .%2.==.STATICRELEASE. goto staticrelease
if .%2.==.DYNAMICRELEASE. goto dynamicrelease
if .%2.==.DYNAMICDEBUG. goto dynamicdebug

echo Incorrect action %2 >>makepicam.log
echo ERROR: Incorrect action %2 for Picam build !
exit 255

:staticrelease
echo Picam selecting libpatrn.static.release.a
echo copy %1\..\Libs\libpatrn.static.release.a %1\..\Libs\libpatrn.a >>makepicam.log
copy %1\..\Libs\libpatrn.static.release.a %1\..\Libs\libpatrn.a
goto end

:dynamicrelease
echo Picam selecting libpatrn.dynamic.release.a
echo copy %1\..\Libs\libpatrn.dynamic.release.a %1\..\Libs\libpatrn.a >>makepicam.log
copy %1\..\Libs\libpatrn.dynamic.release.a %1\..\Libs\libpatrn.a
goto end

:dynamicdebug
echo Picam selecting libpatrn.dynamic.debug.a
echo copy %1\..\Libs\libpatrn.dynamic.debug.a %1\..\Libs\libpatrn.a >>makepicam.log
copy %1\..\Libs\libpatrn.dynamic.debug.a %1\..\Libs\libpatrn.a
goto end


:end

